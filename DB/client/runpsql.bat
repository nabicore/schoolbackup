@echo off

REM PostgreSQL server psql runner script for Windows
REM Dave Page, EnterpriseDB

rem SET server=localhost
SET server=10.40.80.99
SET /P server="Server [%server%]: "

rem SET database=postgres
SET database=sample
SET /P database="Database [%database%]: "

SET port=5432
SET /P port="Port [%port%]: "

rem SET username=postgres
SET username=ci9999
SET /P username="Username [%username%]: "

for /f "delims=" %%a in ('chcp ^|find /c "932"') do @ SET CLIENTENCODING_JP=%%a
if "%CLIENTENCODING_JP%"=="1" SET PGCLIENTENCODING=SJIS
if "%CLIENTENCODING_JP%"=="1" SET /P PGCLIENTENCODING="Client Encoding [%PGCLIENTENCODING%]: "

REM Run psql
".\psql.exe" -h %server% -U %username% -d %database% -p %port%

pause

