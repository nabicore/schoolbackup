﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace dbtest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String host = "10.40.80.35";
            String port = "5432";
            String dbname = "db0000";
            String user = "ci0000";
            String passwd = "";

            String constr = "Server=" + host + ";Port=" + port +
                ";User Id=" + user + ";Database=" + dbname +
                ";Password=" + passwd;

            using (NpgsqlConnection conn = new NpgsqlConnection(constr))
            {
                NpgsqlTransaction tran = null;

                try
                {
                    NpgsqlCommand command;

                    conn.Open();

                    tran = conn.BeginTransaction();

                    using (command = new NpgsqlCommand("SELECT * FROM fruits3;", conn))
                    {
                        using (NpgsqlDataReader dr = command.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                label1.Text += "\n";
                                label1.Text += dr[0].ToString() + " ";
                                label1.Text += dr[1].ToString();
                                label1.Text += dr[2].ToString();
                            }
                        }
                    }
                }
                catch (NpgsqlException ex)
                {
                    label1.Text = "Err Code = " + ex.Code.ToString();
                }
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
    
}
