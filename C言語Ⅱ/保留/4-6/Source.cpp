#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



void InputArray(int(*p)[5]) {
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 5; ++j)
			scanf("%d", p[i] + j);
}

void OutputArray(const int* const p) {

	for (int i = 0; i < 15; ++i) {
		if (i % 5 == 0)
			puts("");
		printf("%d ", *(p + i));
	}
	puts("");
	
}

int main() {

	
	int v[3][5];
	InputArray(v);
	OutputArray(&v[0][0]);


	system("pause");
	return 0;
}