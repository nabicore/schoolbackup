#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>
#include <Windows.h>


struct Monster {
	char name[20];
	int level;
	Monster* next;
};


Monster* Remove(Monster* top) {
	Monster* ret = top;

	Monster* prev = top;
	Monster* now = top;

	while (now) {
		if (now->level <= 15) {
			auto rmv = now;
			prev->next = now->next;
			now = now->next;

			if (rmv == ret)
				ret = now;

			free(rmv);
			continue;
		}
		prev = now;
		now = now->next;
	}

	return ret;
}


//入力
void Input(Monster* top) {
	int input = 1;
	Monster* current = top;
	Monster* prev = nullptr;
	while (true) {
		std::cin >> current->name >> current->level;
		current->next = (Monster*)malloc(sizeof(Monster));
		prev = current;
		current = current->next;

		std::cout << "入力を終了するなら０を入力　";
		std::cin >> input;

		// 入力終了
		if (input == 0) {
			free(current);
			prev->next = nullptr;
			break;
		}
	}
}


//出力
void Output(Monster* top) {
	for (Monster* now = top; now != nullptr; now = now->next)
		printf("%s %d\n", now->name, now->level);
}


//解放
void Free(Monster* top) {
	for (Monster* now = top; now != nullptr;) {
		Monster* tmp = now->next;
		free(now);
		now = tmp;
	}
}





Monster* InputInDescending(Monster* top) {
	int input = 1;

	Monster inputData;
	ZeroMemory(&inputData, sizeof(Monster));

	while (true) {
		std::cin >> inputData.name >> inputData.level;

		Monster* insertPos = nullptr;
		for (Monster* node = top; node != nullptr; node = node->next) {
			if (node->level < inputData.level) {
				if (!insertPos) {
					inputData.next = top;
					insertPos = (Monster*)malloc(sizeof(Monster));
					ZeroMemory(insertPos, sizeof(Monster));
					top = insertPos;
				}
				else {
					inputData.next = node;
					insertPos->next = (Monster*)malloc(sizeof(Monster));
					ZeroMemory(insertPos->next, sizeof(Monster));
					insertPos = insertPos->next;
				}
				break;
			}
			else {
				if (!node->next) {
					node->next = (Monster*)malloc(sizeof(Monster));
					ZeroMemory(node->next, sizeof(Monster));
					insertPos = node->next;
					break;
				}
				insertPos = node;
			}
		}

		memcpy(insertPos, &inputData, sizeof(Monster));

		std::cout << "入力を終了するなら０を入力　";
		std::cin >> input;

		if (input == 0)
			break;
	}

	return top;
}

int main() {

	Monster* top = (Monster*)malloc(sizeof(Monster));
	ZeroMemory(top, sizeof(Monster));
	top = InputInDescending(top);
	Output(top);
	Free(top);

	system("pause");
	return 0;
}