#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>




void InputArray(int* p) {
	std::cin >> *p;
	for (int i = 1; i < 5; ++i)
		*(p + i) = *p;
}

void OutputArray(const int* const p) {
	for (int i = 0; i < 5; ++i)
		std::cout << *(p + i) << " ";
	std::cout << std::endl;
}

int main() {

	int v[5];
	InputArray(v);
	OutputArray(v);


	system("pause");
	return 0;
}