#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



void Output(char** name) {

	for (int i = 0; i < 4; ++i) 
		printf("%s\n", *(name + i));
}

int main() {

	char* name[] = { "gargoyle", "wraith", "troll", "dragon" };
	Output(name);

	system("pause");
	return 0;
}