#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



bool MatchingString(const char str1[], const char str2[]) {
	int i = 0;
	while (true) {
		if (str1[i] == '\0' || str2[i] == '\0') {
			if (str1[i] == str2[i])
				break;
			return false;
		}
		if (str1[i] != str2[i])
			return false;

		i += 1;
	}
	return true;
}


int main() {
	const int n = 10;
	char str1[n], str2[n];

	scanf("%9s", str1);
	scanf("%9s", str2);

	if (MatchingString(str1, str2))
		puts("OK");
	else
		puts("NG");

	system("pause");
	return 0;
}