#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>




void InputValue(int* i) {
	std::cin >> *i;
}

void OutputValue(int v) {
	std::cout << v << std::endl;
}

int main() {

	int a;
	InputValue(&a);
	OutputValue(a);


	system("pause");
	return 0;
}