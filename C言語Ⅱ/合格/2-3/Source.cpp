#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int main() {
	int data[10];
	srand((unsigned int)time(NULL));
	for (int i = 0; i < 10; ++i)
		data[i] = rand();

	puts("== Result ==");
	for (int i = 0; i < 10; ++i)
		printf("data[%2d] = %d\n", i, data[i]);

	system("pause");
	return 0;
}