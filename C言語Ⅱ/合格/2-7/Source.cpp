#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int main() {
	srand((unsigned int)time(NULL));
	const int n = 64;
	const int maxCut = 8;
	const int cut = 4096;
	int amount[maxCut];
	for (int i = 0; i < maxCut; ++i)
		amount[i] = 0;

	int data[n];
	for (int i = 0; i < n; ++i)
		data[i] = rand();

	for (int i = 0; i < n; ++i)
		for (int j = 0; j < maxCut; ++j)
			if (data[i] < cut * (j + 1)) {
				amount[j] += 1;
				break;
			}

	for (int i = 0; i < maxCut; ++i) {
		printf("%5d ~ %5d : ", cut * i, cut * (i + 1));
		for (int j = 0; j < amount[i]; ++j)
			printf("*");
		puts("");
	}

	system("pause");
	return 0;
}