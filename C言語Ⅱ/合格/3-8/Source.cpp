#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



bool FindString(const char str1[], const char str2[]) {
	int i = 0;

	while (true) {
		if (str1[i] == str2[0]) {
			for (int j = 0; str2[j] != '\0'; ++j) {
				if (str1[i] == str2[j])
					i += 1;
				else
					return false;
			}
			return true;
		}
		i += 1;
	}
}


int main() {
	const int n = 16;
	char str1[n], str2[n];

	scanf("%15s", str1);
	scanf("%15s", str2);

	if (FindString(str1, str2))
		puts("OK");
	else
		puts("NG");

	system("pause");
	return 0;
}