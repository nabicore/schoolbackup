#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>



char* MakeMsg(const char(*msg)[20], int h) {
	int strSize = 0;
	for (int i = 0; i < h; ++i)
		for (int j = 0; msg[i][j] != '\0'; ++j)
			strSize += 1;

	char* ret = (char*)malloc(strSize + 1);
	int strCount = 0, line = 0, cursor = 0;
	while (strCount < strSize) {
		if (msg[line][cursor] == '\0') {
			line += 1;
			cursor = 0;
		}
		ret[strCount] = msg[line][cursor];

		cursor += 1;
		strCount += 1;
	}
	ret[strCount] = '\0';

	return ret;
}


int main() {

	char str[3][20];
	std::cin >> str[0] >> str[1] >> str[2];
	char* msg = MakeMsg(str, 3);
	std::cout << msg << std::endl;
	free(msg);

	system("pause");
	return 0;
}