#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>


struct Array {
	int w, h;
	int** data;
};

void Alloc(Array* arr, int w, int h) {
	arr->w = w;
	arr->h = h;
	arr->data = (int**)malloc(sizeof(int*) * h);
	for (int y = 0; y < h; ++y)
		*(arr->data + y) = (int*)malloc(sizeof(int) * w);
}

void Free(Array* arr) {
	for (int y = 0; y < arr->h; ++y)
		free(*(arr->data + y));
	free(arr->data);
	arr->data = nullptr;
}

void Input(Array* arr) {
	for (int y = 0; y < arr->h; ++y) {
		int* tmp = *(arr->data + y);
		for (int x = 0; x < arr->w; ++x, ++tmp)
			*tmp = x + y * arr->w;
	}
}

void Output(const Array* arr) {
	for (int y = 0; y < arr->h; ++y) {
		for (int x = 0; x < arr->w; ++x)
			printf("%3d", arr->data[y][x]);
		puts("");
	}
}

void RotateLeft(Array* arr) {
	Array result;
	Alloc(&result, arr->h, arr->w);

	for (int y = 0; y < result.h; ++y)
		for (int x = 0; x < result.w; ++x)
			result.data[y][x] = arr->data[x][result.h - y - 1];
	
	Free(arr);
	memcpy(arr, &result, sizeof(Array));
}


int main() {

	Array arr;
	Alloc(&arr, 4, 3);
	Input(&arr);
	Output(&arr);

	//�l���]
	for (int i = 0; i < 4; ++i) {
		std::cout << "== �E��] =====" << std::endl;
		RotateLeft(&arr);
		Output(&arr);
	}
	Free(&arr);

	system("pause");
	return 0;
}