#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>



int* MakeArray(const int* arr, int w, int h) {
	int* ret = (int*)malloc(sizeof(int) * w * h);
	for (int j = 0; j < h; ++j) 
		for (int i = 0; i < w; ++i) {
			ret[w * j + i] = *(arr + (w * j) + i);
		}

	return ret;
}

int main() {

	int arr[2][5] = {
		{ 1, 2, 3, 4, 5 },
		{ 6, 7, 8, 9, 0 }
	};
	int* data = MakeArray(&arr[0][0], 5, 2);
	for (int i = 0; i < 10; ++i)
		std::cout << *(data + i) << "  ";
	free(data);


	system("pause");
	return 0;
}