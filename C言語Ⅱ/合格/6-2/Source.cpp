#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>


void output(int* arr, bool(*judge)(int)) {
	for (int i = 0; i < 5; ++i)
		if (judge(arr[i]))
			printf("%d ", arr[i]);
	puts("");
}

bool judge2(int v) {
	if (v % 4 == 0)
		return true;
	return false;
}

bool judge1(int v) {
	if (v >= 3 && v < 9)
		return true;
	return false;
}


int main() {

	int arr[2][5] = {
		{ 1, 3, 5, 7, 9 },
		{ 2, 4, 6, 8, 10 }
	};
	output(arr[0], judge1);
	output(arr[1], judge2);

	system("pause");
	return 0;
}