#include <stdio.h>
#include <math.h>
#include <stdlib.h>



int main() {
	int start = 0, end = 0;

	printf("開始は何段？");
	scanf("%d", &start);
	printf("終了は何段？");
	scanf("%d", &end);

	for (int j = start; j <= end; ++j) {
		for (int i = 1; i < 10; ++i) {
			printf("%3d", i * j);
		}
		printf("\n");
	}

	system("pause");
	return 0;
}