#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>


int main() {

	int* p = (int*)malloc(sizeof(int) * 5);

	for (int i = 0; i < 5; ++i)
		p[i] = 4 * i;

	for (int i = 0; i < 5; ++i)
		std::cout << p[i] << " " ;
	std::cout << std::endl;
	
	free(p);

	system("pause");
	return 0;
}