#include <stdio.h>
#include <stdlib.h>



int main() {
	int data[10];
	for (int i = 0; i < 10; ++i)
		scanf("%d", &data[i]);

	puts("== Result ==");
	for (int i = 0; i < 10; ++i)
		printf("data[%d] = %d\n", i, data[i]);

	system("pause");
	return 0;
}