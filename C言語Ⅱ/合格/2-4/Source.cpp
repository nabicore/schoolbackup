#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int main() {
	int data[10];
	srand((unsigned int)time(NULL));
	for (int i = 0; i < 10; ++i) {
		data[i] = rand() % 8;
		printf("%3d", data[i]);
	}
	puts("");

	int search = 0, searchCount = 0;
	printf("探したい数値を入力：");
	scanf("%d", &search);

	for (int i = 0; i < 10; ++i) {
		if (search == data[i]) {
			printf("%d番目にあります\n", i);
			searchCount += 1;
		}
	}
	if (searchCount <= 0)
		printf("見つかりませんでした\n");

	system("pause");
	return 0;
}