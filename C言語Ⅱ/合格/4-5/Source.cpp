#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>




void CopyString(char* cpy, const char* src) {
	int i = 0;
	do {
		*(cpy + i) = *(src + i);
		i += 1;
	} while (*(src + i) != '\0');
	*(cpy + i) = '\0';
}

int main() {

	char msg[10];
	CopyString(msg, "TanoshiiC");
	std::cout << msg << std::endl;


	system("pause");
	return 0;
}