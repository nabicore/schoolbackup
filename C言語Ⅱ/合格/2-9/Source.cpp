#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int main() {
	srand((unsigned int)time(NULL));
	const int n = 20;
	int data[n];
	for (int i = 0; i < n; ++i) {
		data[i] = i;
		printf("%d ", data[i]);
	}
	printf("\n== Shuffle ==\n");


	for (int i = 0; i < n; ++i) {
		int randI = rand() % 20;
		int tmp = data[i];
		data[i] = data[randI];
		data[randI] = tmp;
	}


	for (int i = 0; i < n; ++i) {
		printf("%d ", data[i]);
	}
	puts("");

	system("pause");
	return 0;
}