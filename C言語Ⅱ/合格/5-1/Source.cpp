#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



struct Monster {
	char*						name;
	int							level;
};

void OutputMonster(const Monster* ch) {
	std::cout << "名前：" << ch->name << " " <<
		"レベル：" << ch->level << std::endl;
}

int main() {

	Monster monster = { "goblin", 4 };
	OutputMonster(&monster);

	system("pause");
	return 0;
}