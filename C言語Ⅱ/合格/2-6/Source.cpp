#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int main() {
	srand((unsigned int)time(NULL));
	const int n = 20;
	int data[n];

	for (int i = 0; i < n; ++i) {
		data[i] = rand() % 16;
		printf("%3d", data[i]);
	}
	puts("");

	
	for (int i = 0; i < n - 1; ++i)
		for (int j = 0; j < n - 1 - i; ++j) 
			if (data[j] > data[j + 1]) {
				int tmp = data[j];
				data[j] = data[j + 1];
				data[j + 1] = tmp;
			}


	puts("== Sort ==");
	for (int i = 0; i < n; ++i) {
		printf("%3d", data[i]);
	}
	puts("");

	system("pause");
	return 0;
}