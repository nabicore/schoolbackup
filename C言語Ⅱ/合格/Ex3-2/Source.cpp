#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>


struct Monster {
	char name[20];
	int level;
	Monster* next;
};


Monster* Remove(Monster* top) {
	Monster* ret = top;

	Monster* prev = top;
	Monster* now = top;

	while (now) {
		if (now->level <= 15) {
			auto rmv = now;
			prev->next = now->next;
			now = now->next;

			if (rmv == ret)
				ret = now;

			free(rmv);
			continue;
		}
		prev = now;
		now = now->next;
	}

	return ret;
}


//入力
void Input(Monster* top) {
	int input = 1;
	Monster* current = top;
	Monster* prev = nullptr;
	while (true) {
		std::cin >> current->name >> current->level;
		current->next = (Monster*)malloc(sizeof(Monster));
		prev = current;
		current = current->next;

		std::cout << "入力を終了するなら０を入力　";
		std::cin >> input;

		// 入力終了
		if (input == 0) {
			free(current);
			prev->next = nullptr;
			break;
		}
	}
}


//出力
void Output(Monster* top) {
	for (Monster* now = top; now != nullptr; now = now->next)
		printf("%s %d\n", now->name, now->level);
}


//解放
void Free(Monster* top) {
	for (Monster* now = top; now != nullptr;) {
		Monster* tmp = now->next;
		free(now);
		now = tmp;
	}
}

int main() {

	Monster* top = (Monster*)malloc(sizeof(Monster));
	Input(top);
	top = Remove(top);
	Output(top);
	Free(top);

	system("pause");
	return 0;
}