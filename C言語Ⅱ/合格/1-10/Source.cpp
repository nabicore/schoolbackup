#include <stdio.h>
#include <math.h>
#include <stdlib.h>



int main() {

	for (int i = 1; i <= 12; ++i)
		printf("%3d|", i);
	printf("\n");
	for (int i = 1; i <= 12; ++i)
		printf("---+");
	printf("\n");

	for (int j = 1; j <= 12; ++j) {
		for (int i = 1; i <= 12; ++i) {
			printf("%3d|", i * j);
		}
		printf("\n");
	}

	system("pause");
	return 0;
}