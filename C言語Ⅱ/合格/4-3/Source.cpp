#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>




void InputValue(int* i) {
	scanf("%d", i);
}

void OutputValue(int v) {
	printf("%d\n", v);
}

int main() {

	int a;
	InputValue(&a);
	OutputValue(a);


	system("pause");
	return 0;
}