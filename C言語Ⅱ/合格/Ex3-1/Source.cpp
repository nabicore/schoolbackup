#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>


struct Monster {
	char name[20];
	int level;
	Monster* next;
};


int main() {

	Monster* top = (Monster*)malloc(sizeof(Monster));

	//入力
	int input = 1;
	Monster* current = top;
	Monster* prev = nullptr;
	while (true) {
		std::cin >> current->name >> current->level;
		current->next = (Monster*)malloc(sizeof(Monster));
		prev = current;
		current = current->next;

		std::cout << "入力を終了するなら０を入力　";
		std::cin >> input;

		// 入力終了
		if (input == 0) {
			free(current);
			prev->next = nullptr;
			break;
		}
	}

	//出力
	for (Monster* now = top; now != nullptr; now = now->next) 
		printf("%s %d\n", now->name, now->level);

	//解放
	for (Monster* now = top; now != nullptr;) {
		Monster* tmp = now->next;
		free(now);
		now = tmp;
	}

	system("pause");
	return 0;
}