#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>


int Add(int v1, int v2) {
	return v1 + v2;
}

bool IsOdd(int v) {
	return v % 2 == 1;
}




int main() {

	int v1 = 100, v2 = 111;
	
	int(*f1)(int v1, int v2) = Add;
	bool(*f2)(int v) = IsOdd;

	std::cout << f1(v1, v2) << " " << f2(v1) << " " << f2(v2) << std::endl;

	system("pause");
	return 0;
}