#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int Input(const char* msg) {
	int lang;
	std::cout << msg;
	std::cin >> lang;
	return lang;
}

int main() {

	char* msg[2][3]{
		{ "おはよう", "こんにちは", "こんばんは" },
		{ "GoodMorning", "GoodAfternoon", "GoodEvening" }
	};

	int lang = Input("言葉を選んでください　０日本語　1英語");
	int no = Input("挨拶選んでください　０朝　1昼　2夜 ");
	std::cout << msg[lang][no] << std::endl;

	system("pause");
	return 0;
}