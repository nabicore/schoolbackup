#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



void Output(char** name) {
	for (int i = 3; i < 8; ++i)
		printf("%c", *(*name + i));
	puts("");
	for (int i = 0; i < 1; ++i)
		printf("%c", *(*(name + 1) + i));
	puts("");
	printf("%s\n", *(name + 2));
	for (int i = 0; i < 3; ++i)
		printf("%c", *(*(name + 3) + i));
	puts("");
}

int main() {

	char* name[] = { "gargoyle", "wraith", "troll", "dragon" };
	Output(name);

	system("pause");
	return 0;
}