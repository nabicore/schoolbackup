#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>




int main() {

	int n;
	std::cout << "何バイト確保しますか？";
	std::cin >> n;

	char* p = (char*)malloc(n);
	std::cin >> std::setw(n) >> p;
	std::cout << p << std::endl;

	free(p);


	system("pause");
	return 0;
}