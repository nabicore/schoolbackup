#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>



int main() {

	const int n = 5;

	char* items[2][5]{
		{ "剣", "鎌", "斧", "槍", "扇" },
		{ "鉄の", "すごい", "やばい", "祝福された", "呪われた" }
	};


	int input = 0;
	srand((unsigned int)time(NULL));
	std::cout << "自動生成する数を決めてください：";
	std::cin >> input;

	for (int i = 0; i < input; ++i)
		std::cout << items[1][rand() % 5] << items[0][rand() % 5] << std::endl;

	system("pause");
	return 0;
}