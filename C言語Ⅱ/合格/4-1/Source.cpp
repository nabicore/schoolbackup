#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>






int main() {

	int i, *ip;
	char c, *cp;
	float f, *fp;
	double d, *dp;
	bool b, *bp;
	short int si, *psi;
	long long ll, *pll;

	std::cout << sizeof(ip)	 << " " << sizeof(i) << std::endl;
	std::cout << sizeof(cp)	 << " " << sizeof(c) << std::endl;
	std::cout << sizeof(fp)	 << " " << sizeof(f) << std::endl;
	std::cout << sizeof(dp)	 << " " << sizeof(d) << std::endl;
	std::cout << sizeof(bp)	 << " " << sizeof(b) << std::endl;
	std::cout << sizeof(psi) << " " << sizeof(si) << std::endl;
	std::cout << sizeof(pll) << " " << sizeof(ll) << std::endl;

	system("pause");
	return 0;
}