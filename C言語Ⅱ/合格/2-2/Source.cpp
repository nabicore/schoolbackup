#include <stdio.h>
#include <math.h>
#include <stdlib.h>



int main() {
	int data[20] = { 1 };
	for (int i = 1; i < 20; ++i)
		data[i] = pow(2, i);

	puts("== Result ==");
	for (int i = 0; i < 20; ++i)
		printf("data[%2d] = %d\n", i, data[i]);

	system("pause");
	return 0;
}