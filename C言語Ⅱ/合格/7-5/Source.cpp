#include <stdio.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>


void DeleteChar(char** str, int n, char del) {
	int charCnt = 0;
	for (int i = 0; i < n; ++i) 
		if ((*str)[i] != del) 
			charCnt += 1;

	char* oldStr = *str;
	char* newStr = (char*)malloc(charCnt + 1);
	int cursor = 0;

	for (int i = 0; i < n; ++i)
		if (oldStr[i] != del) {
			newStr[cursor] = oldStr[i];
			cursor += 1;
		}
	newStr[cursor] = '\0';
	free(oldStr);
	*str = newStr;
}


int main() {

	char* str = (char*)malloc(11);
	memcpy(str, "abacadaeaf", 11);
	std::cout << "消去前　" << str << std::endl;

	DeleteChar(&str, 11, 'a');
	std::cout << "消去後　" << str << std::endl;
	free(str);

	system("pause");
	return 0;
}