//**************************************************************************************//
//
//　GP2　防衛シューティング
//
//**************************************************************************************//
#include	"lib/GpLib.h"

#include <string>
using namespace std;
//**************************************************************************************//
//ライブラリ側で必要な変数　
//必要に応じて変更すること
//**************************************************************************************//
const char USERNAME[256] =	"製作者:*****　ゲームプログラミング�U";
const int	WINW	=	1024;
const int	WINH	=	576;
//const int	WINW	=	640;
//const int	WINH	=	480;
bool		WindowMode = true;
int			KeyboardMode = 0;	//pad disable:1 pad enable:0
//**************************************************************************************//
//ゲーム内で必要な定義、変数宣言　
//**************************************************************************************//
int		nowStageNo;
POINT	mouse;

IMGOBJ    fontIMG;
IMGOBJ backIMG;
IMGOBJ yashiroIMG;
IMGOBJ kumaIMG;
IMGOBJ kumaMaskIMG;
IMGOBJ shotIMG;
IMGOBJ shotMaskIMG;
IMGOBJ effectsIMG;
IMGOBJ explosionIMG;
IMGOBJ cloud1IMG;
IMGOBJ cloud2IMG;

struct CharBase
{
	float	px,py;
	float	pz;
	float	addPx,addPy;			
	int		width,height;		//画像幅高さ
	float	scaleX,scaleY;		//拡大率
	float	angle;				//角度
	bool	show;				//利用フラグ
	IMGOBJ	img;				//使用イメージ名
	float	anim;				//アニメ番号
	int		dir;				//段数
};

struct yashiro
{
	CharBase pos;
	int life;
	RECT rt;
	float pow;
	int score;
	bool damageEffect;
}yashiro;

const int YASHIRO_POSX = 150;
const int YASHIRO_POSY = WINH/2;
const int YASHIROMAXLIFE = 5120;
const int MAXPOW = 500;

const int ENEMYMAX = 10;
struct Enemy
{
	CharBase pos;
	int life;
	int lifemax;
	RECT rt;
	float animCnt;
	int animTableNo;
	bool damageEffect;
	int deadCnt;
	bool attackEffect;
	int attackCnt;
}enemies[ENEMYMAX];

const int ENEMYSZX = 225;
const int ENEMYSZY = 225;
const int ENEMYSTART_X = WINW + ENEMYSZX;
const int ENEMYSTART_Y = 200;
const int ENEMYMAXLIFE = 4;
const int ENEMYSPEEDMAX = -3;


const int SHOTMAX = 10;
struct Shot
{
	CharBase pos;
	RECT rt;
	int pow;
}shots[SHOTMAX];
const int SHOTSPEED = 10;

const int EXPLOSIONMAX = 100;
struct Explosion
{
	CharBase pos;
	int cnt;
}explosions[EXPLOSIONMAX];

struct GameoverInfo
{
	float px, py;
}gameoverInfo;

const int EFFECTMAX = 100;
struct Effect
{
	CharBase pos;

}effects[EFFECTMAX];

const int CLOUDMAX = 6;
struct Back
{
	CharBase pos;
	CharBase cloud[CLOUDMAX];
	int scrollX;
}back;

//**************************************************************************************//
//関数記述
//**************************************************************************************//
//======================================================================================//
//プロトタイプ宣言
//======================================================================================//
void 		Title();
void		TitleDisp();
void		TitleStep();
void		TitleInit();
void		TitleKey();

void		Stage1();
void 		Stage1Init();
void 		Stage1Step();
void 		Stage1Disp();
void		Stage1Key();

void 		End();
void 		EndInit();
void 		EndStep();
void 		EndDisp();
void 		EndKey();
enum{TITLESCREEN,STAGE1SCREEN,ENDSCREEN};

//======================================================================================//
//関数定義
//======================================================================================//
//------------------------------------------------------
//GP2汎用処理
//
//----------------------
//CharBase利用時汎用処理
//----------------------
//利用チェック
bool CheckUseChar(const CharBase& base)
{
	return (base.show == true);
}

//----------------------
//汎用移動
void MoveChar(CharBase& pos)
{
	pos.px += pos.addPx;
	pos.py += pos.addPy;
}

//----------------------
//汎用消去処理
void NoUseChar(CharBase& pos)
{
	pos.show = false; 
}

//----------------------
//汎用出現処理
void UseChar(CharBase& pos)
{
	pos.show = true; 
}
//----------------------
//判定用矩形作成 左上座標基準
void MakeRectLeftTop(RECT &rt,CharBase &cb)
{
	SetRect(&rt,(int)cb.px,(int)cb.py,
				(int)cb.px+cb.width,(int)cb.py+cb.height);
}

//----------------------
//判定用矩形作成 中央座標基準
void MakeRectCenter(RECT &rt,CharBase &cb)
{
	SetRect(
		&rt,
		int(cb.px - cb.width  * cb.scaleX/2),
		int(cb.py - cb.height * cb.scaleY/2),
		int(cb.px + cb.width  * cb.scaleX/2),
		int(cb.py + cb.height * cb.scaleY/2));
}

//----------------------
//汎用描画処理 中央基準
void Draw_Char(const CharBase& pos)
{
	Draw_GraphicsCenter(
				pos.px,pos.py,pos.pz,
				pos.img,
				pos.width	* (int)pos.anim,
				pos.height*(pos.dir),
				pos.width,pos.height,
				(int)pos.angle,nullptr,
				pos.scaleX,pos.scaleY,
				255,255,255,255);
}

//----------------------
//汎用描画処理 左上基準
void Draw_Char2(const CharBase& pos)
{
	Draw_Graphics(pos.px,pos.py,pos.pz,
				pos.img,
				pos.width	* (int)(pos.anim),
				pos.height*(pos.dir),
				pos.width,pos.height,
				(int)pos.angle,nullptr,
				pos.scaleX,pos.scaleY,
				255,255,255,255);
}

//CharBaseここまで
//----------------------------

//----------------------
//文字表示
const int STRAIGHT = 0;		//まっすぐ描画
const int WORDBREAK = 1;	//\nで改行する。
void Draw_TextToBmp(int x,int y,const string& msg,int size,int type = STRAIGHT )
{
	int i;
	//フォントデータの横並び数
	const int GRAPHIC_CELL_LENGTH = 16;
	//フォントデータの元サイズ
	const int GRAPHIC_SIZEXY = 8;
	int draw_x,draw_y;
	int font_x,font_y ;
	//初期描画位置作成
	draw_x = x; draw_y = y;
	if(size <= 0)	size = 1;
	for(i = 0 ; i < (int)msg.length(); i++){
		//描画文字決定
		font_x = (int)msg[i] % GRAPHIC_CELL_LENGTH * GRAPHIC_SIZEXY;
		font_y = (int)msg[i] / GRAPHIC_CELL_LENGTH * GRAPHIC_SIZEXY;
		//文字描画
		Draw_Graphics(draw_x,draw_y,0.5f,fontIMG,
					font_x,font_y,GRAPHIC_SIZEXY,GRAPHIC_SIZEXY,
					0,0,(float)size,(float)size); 
		//文字描画位置更新
		if(type == WORDBREAK && msg[i] == '\n'){
			draw_y += GRAPHIC_SIZEXY * size;
			draw_x = x;
		}else{
			draw_x += GRAPHIC_SIZEXY * size;
		}
	}
}

//終了チェック
float overCount;
bool overFlag;
const int OVERCHANGETIME = 60;		//10分の１ sec指定
//終了処理開始
void StartGameOver()
{
	if(!overFlag)
	{
		overFlag = true;
		overCount = 0;
	}
}
//終了処理初期化
void InitGameOver()
{
	overFlag = false;
	overCount = 0;
	gameoverInfo.px = WINW;
	gameoverInfo.py = 0;
}

//終了処理判別
bool isGameOver()
{
	return overFlag;
}

//終了処理
void CheckGameOverToChangeScreen()
{
	if(overFlag){
		overCount += ONEFRAME_TIME*10;//10分の１sec
		if(overCount >= OVERCHANGETIME){
			ChangeStage(ENDSCREEN);
		}
	}
}

//----------------------
//あたりチェック
bool HitCheck(CharBase& v1,CharBase& v2)
{
	RECT rt1;
	RECT rt2;
	MakeRectCenter(rt1,v1);
	MakeRectCenter(rt2,v2);
//デバッグ----------------
	Draw_CkRect(rt1);
	Draw_CkRect(rt2);
//ここまで----------------
	if(RectCheck(&rt1,&rt2) ){
			return true;
	}else{
		return false;
	}
}

//汎用処理ここまで
//------------------------------------------------------
//======================================================
//新規の関数はここから追加する

bool IsDeadEnemy(const Enemy& enemy)
{
	return enemy.deadCnt != 0;
}

void InitEffect()
{
	for(Effect& effect: effects)
	{
		Effect temp = {};
		effect = temp;
	}
}

void CreateEffect(float x, float y, float z)
{
	int i = 0;
	int angle = SetRandomCount(360);
	for(Effect& effect: effects)
	{
		if(!CheckUseChar(effect.pos))
		{
			effect.pos.px = x;
			effect.pos.py = y;
			effect.pos.pz = z;
			effect.pos.addPx = 0;
			effect.pos.addPy = 0;
			effect.pos.width  = 32;
			effect.pos.height = 32;
			effect.pos.angle = angle + 120.0f * i;
			effect.pos.anim = 0;
			effect.pos.img = effectsIMG;
			effect.pos.scaleX = 0.5f;
			effect.pos.scaleY = 0.25f;
			UseChar(effect.pos);
			if(++i > 3) break;
		}
	}
}

void StepEffect()
{
	for(Effect& effect: effects)
	{
		if(CheckUseChar(effect.pos))
		{
			effect.pos.angle += 9;

			effect.pos.scaleX += effect.pos.scaleX / 8.0f;
			effect.pos.scaleY = effect.pos.scaleX / 5.5f;
			if(effect.pos.scaleX >= 16.0f)
				NoUseChar(effect.pos);
		}
	}
}

void DrawEffect()
{
	for(Effect& effect: effects)
	{
		if(CheckUseChar(effect.pos))
		{
			Draw_SetRenderMode(ADD);
			const CharBase& pos = effect.pos;
			Draw_GraphicsCenter(
				pos.px, pos.py, pos.pz,
				pos.img,
				48+32,	0,
				pos.width, pos.height,
				(int)pos.angle, nullptr,
				pos.scaleX, pos.scaleY,
				100, 32, 64, 255);
			Draw_GraphicsCenter(
				pos.px, pos.py, pos.pz,
				pos.img,
				48+32,	0,
				pos.width, pos.height,
				(int)pos.angle, nullptr,
				pos.scaleX, pos.scaleY,
				100, 32, 64, 255);
			Draw_GraphicsCenter(
				pos.px, pos.py, pos.pz,
				pos.img,
				48+32,	0,
				pos.width, pos.height,
				(int)pos.angle, nullptr,
				pos.scaleX, pos.scaleY,
				100, 32, 64, 255);
			Draw_EndRenderMode();
		}
	}
}

void InitExplosion()
{
	for(Explosion& explosion: explosions)
	{
		Explosion temp = {};
		explosion = temp;
	}
}

void CreateExplosion(float x, float y, float z)
{
	for(Explosion& explosion: explosions)
	{
		if(!CheckUseChar(explosion.pos))
		{
			explosion.pos.px = x;
			explosion.pos.py = y;
			explosion.pos.pz = z;
			explosion.pos.addPx = float(12 - SetRandomCount(24));
			explosion.pos.addPy = float(12 - SetRandomCount(24));
			explosion.pos.width  = 96;
			explosion.pos.height = 96;
			explosion.pos.angle = 0;
			explosion.pos.anim = 0;
			explosion.pos.img = explosionIMG;
			explosion.pos.scaleX = 2.0f;
			explosion.pos.scaleY = 2.0f;
			explosion.cnt = 0;
			UseChar(explosion.pos);
			break;
		}
	}
}

void StepExplosion()
{
	for(Explosion& explosion: explosions)
	{
		if(CheckUseChar(explosion.pos))
		{
			MoveChar(explosion.pos);
			explosion.pos.anim+=0.5;
			if(explosion.pos.anim >= 16.0f)
				NoUseChar(explosion.pos);
		}
	}
}


void DrawExplosion()
{
	for(Explosion& explosion: explosions)
	{
		if(CheckUseChar(explosion.pos))
		{
			Draw_Char(explosion.pos);
		}
	}
}

void DamageYashiro(int dam)
{
	yashiro.life -= dam;
	yashiro.damageEffect = true;
	if(yashiro.life <= 0)
	{
		yashiro.life = 0;
		StartGameOver();
	}
}

void StepYashiro()
{
}

void DrawScore(int x, int y, DWORD color = 0xFF000000)
{
	stringstream msg;
	msg << "クマーっ： " << setfill('0') << setw(3) << yashiro.score;
	Draw_FontText(x, y, 0.5f, msg.str(), color, 1);
}
void DrawYashiro()
{
	float px=0, py=0;
	if(yashiro.damageEffect)
	{
		yashiro.damageEffect = false;
		//iで指定した値だけ震える
		int i = 8;
		int j = i * 2 + 1;
		px = float(i - rand() % j);
		py = float(i - rand() % j);
	}

	CharBase pos = yashiro.pos;
	pos.px += px;
	pos.py += py;
	Draw_Char(pos);

	if(isGameOver())
	{
		if((int)gameoverInfo.px % 20 == 0)
		{
			float x = pos.px + ((pos.width / 2) - (rand() % pos.width));
			float y = pos.py + ((pos.height / 2) - (rand() % pos.height));
			for(int i = 0; i < 8; ++i)
				CreateExplosion(x, y, pos.pz);
		}
	}
	const int LIFEBAR_LENGTH = 200;
	const int LIFEBAR_HEIGHT = 30;

	//耐久表示
	int now = yashiro.life * LIFEBAR_LENGTH / YASHIROMAXLIFE;
	Draw_Box(	
		int(yashiro.pos.px - LIFEBAR_LENGTH / 2 + px),
		int(yashiro.pos.py + 30 + py), 
		int(yashiro.pos.px + LIFEBAR_LENGTH / 2 + px),
		int(yashiro.pos.py + 30 + LIFEBAR_HEIGHT + py),
		0.0f,
		ARGB(255,255,0,0), ARGB(255,0,0,0),
		3, 1);
	Draw_Box(
		int(yashiro.pos.px - LIFEBAR_LENGTH / 2 + px),
		int(yashiro.pos.py + 30 + py), 
		int(yashiro.pos.px - LIFEBAR_LENGTH / 2 + now + px),
		int(yashiro.pos.py + 30 + LIFEBAR_HEIGHT + py),
		0.0f,
		ARGB(255,0,255,0), ARGB(255,0,0,0),
		3, 1);
	Draw_FontText(
		int(yashiro.pos.px - LIFEBAR_LENGTH / 2),
		int(yashiro.pos.py + 30),
		0.0f,
		to_string(yashiro.life),
		0xff000000, 1);

	//ためた攻撃力
	const int POWBAR_LENGTH = 100;
	const int POWBAR_HEIGHT = 10;
	
	now = (int)(yashiro.pow * POWBAR_LENGTH / MAXPOW);
	Draw_Box(
		int(yashiro.pos.px - POWBAR_LENGTH / 2 + px),
		int(yashiro.pos.py + py), 
		int(yashiro.pos.px + POWBAR_LENGTH / 2 + px),
		int(yashiro.pos.py + POWBAR_HEIGHT + py),
		0.0f,
		ARGB(255,255,0,0), ARGB(255,0,0,0),
		3, 1);
	Draw_Box(
		int(yashiro.pos.px - POWBAR_LENGTH / 2 + px),
		int(yashiro.pos.py + py), 
		int(yashiro.pos.px - POWBAR_LENGTH / 2 + now + px),
		int(yashiro.pos.py + POWBAR_HEIGHT + py),
		0.0f,
		ARGB(255,0,255,0), ARGB(255,0,0,0),
		3, 1);

	//スコア表示
	DrawScore(0, 0);
}

void InitYashiro()
{
	CharBase& pos = yashiro.pos;
	pos.px = YASHIRO_POSX;
	pos.py = YASHIRO_POSY;
	pos.img = yashiroIMG;
	pos.angle = 0;
	pos.width = 300;
	pos.height =420;
	pos.pz = 0.5f;
	pos.scaleX = 1.0f;
	pos.scaleY = 1.0f;
	pos.anim = 0;
	pos.dir = 0;
	yashiro.life = YASHIROMAXLIFE;
	yashiro.score = 0;
	yashiro.damageEffect = false;
	UseChar(pos);
	MakeRectCenter(yashiro.rt, pos);
}

void CreateEnemy()
{
	for(Enemy& enemy: enemies)
	{
		CharBase& pos = enemy.pos;
		if(!CheckUseChar(pos))
		{
			pos.width = ENEMYSZX;
			pos.height = ENEMYSZY;
			pos.scaleY = pos.scaleX = SetRandomCount(9, 1) * 0.25f;
			pos.px = (float)ENEMYSTART_X;
			pos.py = (float)SetRandomCount((WINH - int(pos.height*pos.scaleY/2.0f)) - WINH/2, WINH/2);
			pos.pz = 1.15f - ((pos.py + (pos.height * pos.scaleY) / 2.f) / WINH);
			pos.addPx = -6.0f + pos.scaleX*2.0f;
			pos.addPy = 0;
			pos.img = kumaIMG;
			pos.dir = 0;
			pos.anim = 0;
			enemy.lifemax = enemy.life = int(10.0f * pos.scaleX);
			enemy.animCnt = 0;
			enemy.animTableNo = 0;
			enemy.deadCnt = 0;
			enemy.attackEffect = false;
			enemy.damageEffect = false;
			enemy.attackCnt = 0;
			UseChar(pos);
			MakeRectCenter(enemy.rt, pos);
			break;
		}
	}
}

bool StepEnemy_DeadEffect(Enemy& enemy)
{
	if(!IsDeadEnemy(enemy)) return false;

	if(++enemy.deadCnt >= 120)
	{
		NoUseChar(enemy.pos);
		enemy.deadCnt = 0;
	}
	return true;
}

void AnimEnemy(Enemy& enemy)
{
	float animTable[][5] = 
	{
		{3, 1, 3, 2, -4},
		{0, 1, 2, -3},
		{0, 1, -2},
		{2, -1},
	};
	enemy.animCnt += ONEFRAME_TIME * 10;
	if(enemy.animCnt > 1.0f)
	{
		enemy.animCnt = 0;
		++enemy.animTableNo;
		if(animTable[enemy.pos.dir][enemy.animTableNo] < 0)
			enemy.animTableNo += (int)animTable[enemy.pos.dir][enemy.animTableNo];
		enemy.pos.anim = animTable[enemy.pos.dir][enemy.animTableNo];
	}
}

void MotionChange_EnemyAttack(Enemy& enemy)
{
	enemy.attackEffect = true;
	enemy.pos.addPx = 0;
	enemy.pos.dir = 1;
	enemy.pos.anim = 0;
	enemy.animCnt = 0;
	enemy.animTableNo = 0;
}

void AttackEnemy(Enemy& enemy)
{
//	if(++enemy.attackCnt % 10 == 0)
	{
		DamageYashiro(1);
	}
}

RECT GetYashiroCollison(CharBase& yashiroPos)
{
	RECT rt = 
	{
		int(yashiroPos.px - yashiroPos.width  * yashiroPos.scaleX/2),
		int(yashiroPos.py - yashiroPos.height * yashiroPos.scaleY/2 + 256),
		int(yashiroPos.px + yashiroPos.width  * yashiroPos.scaleX/2),
		int(yashiroPos.py + yashiroPos.height * yashiroPos.scaleY/2)
	};
	return rt;

}

void StepEnemy()
{
	for(Enemy& enemy: enemies)
	{
		CharBase& pos = enemy.pos;
		if(CheckUseChar(pos))
		{

			//やられ
			if(!StepEnemy_DeadEffect(enemy))
			{

				//移動
				MoveChar(pos);
				if(pos.px <= -pos.width)
					NoUseChar(pos);

				//アニメ
				AnimEnemy(enemy);

				//社と判定
				MakeRectCenter(enemy.rt, pos);
				yashiro.rt = GetYashiroCollison(yashiro.pos);
//				MakeRectCenter(yashiro.rt, yashiro.pos);
				if(RectCheck(yashiro.rt, enemy.rt))
				{
					if(!enemy.attackEffect)
						MotionChange_EnemyAttack(enemy);
					//NoUseChar(pos);
					AttackEnemy(enemy);
				}
			}
		}
	}

	if(SetRandomCount(100) <= 0)
		CreateEnemy();
}

int DamageEnemy(Enemy& enemy, int pow)
{
	enemy.life -= pow;
	enemy.damageEffect = true;
	if(enemy.life <= 0)
	{
		for(int i = 0; i < 8; ++i)
		{
			CreateExplosion(enemy.pos.px, enemy.pos.py, enemy.pos.pz);
		}
		enemy.pos.addPx = 0;
		enemy.deadCnt = 1;
		enemy.pos.dir = 3;
		enemy.pos.anim = 2;
		++yashiro.score;
	}
	return enemy.life;
}

void DrawEnemy()
{
	for(Enemy& enemy: enemies)
	{
		CharBase& pos = enemy.pos;
		if(CheckUseChar(pos))
		{
			if(enemy.deadCnt % 3 == 0)
			{

				//影
				Draw_Graphics(
					pos.px - pos.scaleX*pos.width/2,
					pos.py + pos.scaleY*pos.height/2,
					0.9f,
					kumaIMG,
					int(pos.anim * pos.width),
					pos.height,
					pos.width, -pos.height,
					0, nullptr,
					pos.scaleX, pos.scaleY/2.0f,
					255, 0, 0, 0);

				//本体
				Draw_Char(pos);
				if(enemy.damageEffect)
				{
					Draw_GraphicsCenter(
						pos.px, pos.py, pos.pz,
						kumaMaskIMG,
						int(pos.width  * pos.anim),
						pos.height * pos.dir,
						pos.width, pos.height,
						(int)pos.angle, nullptr,
						pos.scaleX, pos.scaleY,
						128, 255, 0, 0);
					enemy.damageEffect = false;
				}
			}

			if(!IsDeadEnemy(enemy))
			{
				//体力ゲージ
				const int ENEMYBAR_LENGTH = 100;
				const int ENEMYBAR_HEIGHT = 10;
				int now = enemy.life * ENEMYBAR_LENGTH/ enemy.lifemax;
				int x = (int)pos.px;
				int y = (int)pos.py;
				Draw_Box(
					x, y,
					x + ENEMYBAR_LENGTH,
					y + ENEMYBAR_HEIGHT, 0.0f,
					ARGB(255, 255, 0, 0), ARGB(255, 0, 0, 0), 3, 1);
				Draw_Box(
					x, y,
					x + now,
					y + ENEMYBAR_HEIGHT, 0.0f,
					ARGB(255, 0, 255, 0), ARGB(255, 0, 0, 0), 3, 1);
			}
		}
	}
}

void InitEnemy()
{
	for(Enemy& enemy: enemies)
	{
		NoUseChar(enemy.pos);
		enemy.life = 0;
		enemy.damageEffect = false;
	}
}

void InitShot()
{
	for(Shot& shot: shots)
		NoUseChar(shot.pos);
}

void DrawShot()
{
	for(Shot& shot: shots)
	{
		if(CheckUseChar(shot.pos))
		{
			Draw_Char(shot.pos);

			//エフェクト
			{
				CharBase pos = shot.pos;
				pos.scaleX += pos.scaleX / 2.0f;
				pos.scaleY += pos.scaleY / 2.0f;
				Draw_SetRenderMode(ADD);
				Draw_GraphicsCenter(
					pos.px, pos.py, pos.pz - 0.005f,
					pos.img,
					pos.width  * (int)pos.anim,
					pos.height * pos.dir,
					pos.width, pos.height,
					(int)pos.angle, nullptr,
					pos.scaleX, pos.scaleY,
					255, 255, 0, 0);
				Draw_EndRenderMode();
			}
		}
	}
}
void DamageShot(Shot& shot, int pow)
{
	{
		//8フレーム先の位置で爆発させる（見た目上の意味で…）
		float x = shot.pos.px + shot.pos.addPx*8;
		float y = shot.pos.py + shot.pos.addPy*8;
		CreateEffect(x, y, 0.1f);
	}

	shot.pow -= pow;
	if(shot.pow <= 0)
		NoUseChar(shot.pos);
}
void StepShot()
{
	for(Shot& shot: shots)
	{
		CharBase& pos = shot.pos;
		if(CheckUseChar(pos))
		{
			MoveChar(pos);
			pos.angle += 15.0f;
			if(pos.angle >= 360.0f) pos.angle -= 360.0f;


			if(!InScreenCameraCenter(pos.px, pos.py, (float)pos.width, (float)pos.height))
			{
				NoUseChar(pos);
			}
			MakeRectCenter(shot.rt, pos);

			for(Enemy& enemy: enemies)
			{
				if(CheckUseChar(pos) && CheckUseChar(enemy.pos) && !IsDeadEnemy(enemy))
				{
					MakeRectCenter(enemy.rt, enemy.pos);
					if(RectCheck(enemy.rt, shot.rt))
					{
						int damage = shot.pow;
						DamageShot(shot, enemy.life);
						DamageEnemy(enemy, damage);
					}
				}
			}
		}
	}
}
void CreateShot(int angle, int pow)
{
	for(Shot& shot: shots)
	{
		CharBase& pos = shot.pos;
		if(!CheckUseChar(pos))
		{
			pos.px = (float)YASHIRO_POSX;
			pos.py = (float)YASHIRO_POSY;
			pos.img = shotIMG;
			pos.angle = (float)angle;
			pos.width = 56;
			pos.height = 56;
			pos.pz = 0.5f;
			pos.scaleX = 1.0f + pow / 5.0f;
			pos.scaleY = 1.0f + pow / 5.0f;
			pos.anim = 0;
			pos.dir = 0;
			pos.addPx = ROUND_X(angle, SHOTSPEED, 0);
			pos.addPy = ROUND_Y(angle, SHOTSPEED, 0);
			shot.pow = pow + 1;
			MakeRectCenter(shot.rt, pos);
			UseChar(pos);
			break;
		}
	}
}

void CreateShot_3Way(int angle, int pow)
{
	int cnt = -1;
	for(Shot& shot: shots)
	{
		CharBase& pos = shot.pos;
		if(!CheckUseChar(pos))
		{
			int degree = angle + cnt * 10;
			pos.px = (float)YASHIRO_POSX;
			pos.py = (float)YASHIRO_POSY;
			pos.img = shotIMG;
			pos.angle = (float)degree;
			pos.width = 56;
			pos.height = 56;
			pos.pz = 0.0f;
			pos.scaleX = 1.0f;
			pos.scaleY = 1.0f;
			pos.anim = 0;
			pos.dir = 0;
			pos.addPx = ROUND_X(degree, SHOTSPEED, 0);
			pos.addPy = ROUND_Y(degree, SHOTSPEED, 0);
			shot.pow = pow + 1;
			MakeRectCenter(shot.rt, pos);
			UseChar(pos);

			++cnt;
			if(cnt > 1) break;
		}
	}
}

void DrawGameOver()
{
	if(isGameOver())
	{
		int backColor = ARGB(255, 48, 48, 255);
		Draw_SetRenderMode(SUBTRACT);
		Draw_Box(0, 100, WINW, 100 + 120,
			0.1f, backColor, backColor, 1, 1);
		Draw_EndRenderMode();
		Draw_FontText(
			(int)gameoverInfo.px, 100, 0.0f,
			"あなたは社を守れませんでした",
			ARGB(255, 255, 255, 255), 2);
		gameoverInfo.px -= 8.0f;
		if(gameoverInfo.px <= -1950)
			gameoverInfo.px = WINW;
	}
}

void InitBack()
{
	back.scrollX = 0;
	back.pos.px = 0;
	back.pos.py = 0;
	back.pos.pz = 0.9f;
	back.pos.addPx = -0.5f;
	back.pos.addPy = 0;
	back.pos.width  = 800;
	back.pos.height = 600;
	back.pos.angle = 0;
	back.pos.anim = 0;
	back.pos.img = backIMG;
	back.pos.scaleX = 1.0f;
	back.pos.scaleY = 1.0f;

	for(int i = 0; i < CLOUDMAX; ++i)
	{
		CharBase& cloud = back.cloud[i];
		cloud = CharBase();
		cloud.width  = 1024;
		cloud.height = 1024;
		cloud.scaleX = 0.5f;
		cloud.scaleY = 0.5f;
		cloud.px = float(256 * i);
		cloud.py = -64;
		cloud.pz = 0.95f;
		cloud.addPx = -0.5f;
		cloud.addPy = 0;
		cloud.angle = 0;
		cloud.anim = 0;
		cloud.img = cloud2IMG;
		UseChar(cloud);
	}
}

void StepBack()
{
//	MoveChar(back.pos);
//	if(back.pos.px < -back.pos.width)
//		back.pos.px += back.pos.width;

	for(CharBase& cloud: back.cloud)
	{
		if(CheckUseChar(cloud))
		{
			MoveChar(cloud);
			//画面端まできたら戻す
			if(cloud.px < -cloud.width * cloud.scaleX)
				cloud.px = WINW;
		}
	}
}

void DrawBack()
{
	//空の色
	{
		float f = (float)yashiro.life / (float)YASHIROMAXLIFE;
		int color = ARGB(
			255,
			int(255.0f * (1.0f - f)),
			0,
			int(255.0f * f));
		Draw_Box(0, 0, WINW, WINH, 0.97f,
			color, color, 1, 1);
	}

	//雲
	for(CharBase& cloud: back.cloud)
	{
		if(CheckUseChar(cloud))
		{
			Draw_Char2(cloud);
		}
	}

	//背景
	CharBase pos = back.pos;
	for(int i = 0; i < 3; ++i)
	{
		Draw_Char2(pos);
		pos.px += pos.width;
	}
}
//新規の関数はここまでに追加する
//======================================================

//---------------------------------
// STAGE1
//---------------------------------
//初期化処理		
void Stage1Init()
{
	InitBack();
	InitYashiro();
	InitEnemy();
	InitShot();
	InitExplosion();
	InitGameOver();
	InitEffect();
}

//フレーム処理
void Stage1Step()
{
	StepBack();
	StepYashiro();
	StepEnemy();
	StepShot();
	StepExplosion();
	CheckGameOverToChangeScreen();
	StepEffect();
}

//描画処理
void Stage1Disp()
{
	DrawBack();
	DrawYashiro();
	DrawEnemy();
	DrawShot();
	DrawExplosion();
	DrawGameOver();
	DrawEffect();
}

//入力処理
void Stage1Key()
{
	if(CheckPush(KEY_BTN0)){
		ChangeStage(ENDSCREEN);
	}
	if(CheckPush(KEY_BTN1)){
		POINT mouse = GetMousePosition();
		CreateEffect((float)mouse.x, (float)mouse.y, 0.0f);
	}
	if(!isGameOver())
	{
		if(CheckPress(KEY_MOUSE_LBTN))
		{
			yashiro.pow += ONEFRAME_TIME * 100;
			if(yashiro.pow >= MAXPOW)
				yashiro.pow = MAXPOW;
		}
		if(CheckPull(KEY_MOUSE_LBTN))
		{
			POINT mouse = GetMousePosition();
			float angle = DegreeOfPoints2(yashiro.pos.px, yashiro.pos.py, (float)mouse.x, (float)mouse.y);
			CreateShot((int)angle, (int)yashiro.pow/10);
			yashiro.pow = 0;
		}
	}
}

void Stage1()
{
	if(InitStage()){Stage1Init();}
	Stage1Key();
	Stage1Step();
	Stage1Disp();
}

//---------------------------------
// Title
//---------------------------------

//初期化処理		
void TitleInit()
{
}

//フレーム処理
void TitleStep()
{

}

//描画処理
void TitleDisp()
{
	Draw_TextToBmp(100, 100, "GP2\n DefenceShooting", 4, WORDBREAK);
	Draw_TextToBmp(200, 200, "Push A Start", 3);
}

//入力処理
void TitleKey()
{
	if(CheckPush(KEY_BTN0)){
		ChangeStage(STAGE1SCREEN);
	}
}

void Title()
{
	if(InitStage()){TitleInit();}
	TitleKey();
	TitleStep();
	TitleDisp();

}

//---------------------------------
// End
//---------------------------------

//初期化処理		
void EndInit()
{
}

//フレーム処理
void EndStep()
{

}

//描画処理
void EndDisp()
{
	DrawScore(100, 132, -1);
}

//入力処理
void EndKey()
{
	if(CheckPush(KEY_BTN0)){
		ChangeStage(TITLESCREEN);
	}
}

void End()
{
	if(InitStage()){EndInit();}
	EndKey();
	EndStep();
	EndDisp();
}


//---------------------------------------------------------------------------------------
//ゲームループ 
//---------------------------------------------------------------------------------------
void GameLoop()
{
	switch(CheckStage())
	{
	case 0:
		Title();
		break;

	case 1:
		Stage1();
		break;

	case 2:
		End();
		break;

	}
}

//---------------------------------------------------------------------------------------
//プログラム全体の初期化
//フォントの作成、ローディング画面で使用する画像の読み込みなど
//起動時に一回だけ行う処理はここで行う
//---------------------------------------------------------------------------------------
void OnCreate()
{
	Draw_CreateFont(0,30,"MS　ゴシック");
	
	fontIMG = Draw_LoadObject("res/gra/font.png");
	backIMG = Draw_LoadObject("res/gra/ground.png");
	yashiroIMG = Draw_LoadObject("res/gra/oyasiro.png");
	kumaIMG = Draw_LoadObject("res/gra/kuma.png");
	kumaMaskIMG = Draw_LoadObject("res/gra/kumaMask.png");
	shotIMG = Draw_LoadObject("res/gra/shot.png");
	shotMaskIMG = Draw_LoadObject("res/gra/shotMask.png");
	explosionIMG = Draw_LoadObject("res/gra/explode.png");
	effectsIMG =  Draw_LoadObject("res/gra/effects.png");
	cloud1IMG = Draw_LoadObject("res/gra/cloud1.png");
	cloud2IMG = Draw_LoadObject("res/gra/cloud2.png");

	AddFontResourceEx("res/font/yutapon_k_bold_000_ot_sample.otf", FR_PRIVATE, NULL);
	Draw_CreateFont(1,30,"ゆたぽん（COD）K");
	Draw_CreateFont(2,120,"ゆたぽん（COD）K");
	
	InitCamera(WINW/2,WINH/2,0.0f,0.0f);

}
//---------------------------------------------------------------------------------------
//プログラム全体の後始末
//終了時に一回だけ行う処理はここで行う
//---------------------------------------------------------------------------------------
void OnDestroy()
{
	RemoveFontResourceEx("res/font/yutapon_k_bold_000_ot_sample.otf", FR_PRIVATE, NULL);
}
//======================================================================================//
//WINDOW　メイン
//======================================================================================//
int WINAPI WinMain (HINSTANCE hInstance,HINSTANCE hPreInst,LPSTR lpszCmdLine,int nCmdShow)
{
	return DoWindow(hInstance,hPreInst,lpszCmdLine,nCmdShow);
}
