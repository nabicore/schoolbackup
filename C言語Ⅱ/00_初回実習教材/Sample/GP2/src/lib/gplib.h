//**************************************************************************************//
//　ソフトウェア設計　ライブラリの制作
//		gplib.h
//**************************************************************************************//
//**************************************************************************************//
//インクルード
//**************************************************************************************//
#pragma once
#define _CRT_SECURE_NO_DEPRECATE 1
#pragma warning(disable : 4995)	

//DirectSound DorectShowを使わない場合はコメントアウトすること
#define USE_DIRECTSOUND
#define USE_DIRECTSHOW

#pragma	comment(lib,"dxguid.lib")
#pragma	comment(lib,"d3dxof.lib")
#pragma	comment(lib,"dxguid.lib")
#pragma	comment(lib,"d3d9.lib")
#pragma	comment(lib,"d3dx9.lib")
#pragma	comment(lib,"winmm")
#pragma	comment(lib,"dinput8.lib")

#ifdef USE_DIRECTSHOW
#	pragma comment(lib,"strmiids")
#endif

#ifdef USE_DIRECTSOUND
#	pragma	comment(lib,"dsound.lib")
#endif

#include <d3dx9.h>
#include <dxerr.h>

#ifdef USE_DIRECTSOUND
#	include <dsound.h>
#endif

#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>

#ifdef USE_DIRECTSHOW
#	include <dshow.h>
#endif

#include <crtdbg.h>
#include <time.h>
#include <assert.h> 

#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <tuple>


//**************************************************************************************//
//マクロ　変数　宣言
//**************************************************************************************//
 
//VisualStudo2012以前には nullptr が無いので定義
#if (_MSC_VER<1700)
  const class {
  public:
      template<class T>
      operator T*() const { return 0; }
      // あらゆる型のメンバポインタへの変換
      template<class C, class T>
      operator T C::*() const  { return 0; }
  private:
      void operator&() const;
  } nullptr;
#endif

#define ARGB D3DCOLOR_ARGB
#define REFRESHRATE 60
#define ONEFRAME_TIME (1.0f / static_cast<float>(REFRESHRATE))
#define ONEFLAME_TIME (1.0f / static_cast<float>(REFRESHRATE))

//IMG
#define	IMGMAX 100
//extern	LPDIRECT3DTEXTURE9 ImgTable[IMGMAX];
typedef	int IMGOBJ;

//FONT
#define	DDFONTMAX	10
extern LPD3DXFONT pFontData[DDFONTMAX];

//KEY
#define		FREE_KEY	0
#define		PUSH_KEY	1
#define		HOLD_KEY	2
#define		PULL_KEY	3

enum
{
	JOY1,
	JOY2
};
enum
{
	JOY_DOWN = 0x1000,
	JOY_LEFT = 0x1001,
	JOY_UP = 0x1002,
	JOY_RIGHT = 0x1004
};

//キーチェック
//キーに名前をつける
enum CODE_KEY
{
	KEY_NEUTRAL = -1,	//ニュートラル状態
	KEY_DOWN,			//下キー
	KEY_LEFT,			//左キー
	KEY_UP,				//上キー
	KEY_RIGHT,			//右キー
	KEY_BTN0,			//ボタン0
	KEY_BTN1,			//ボタン1
	KEY_BTN2,			//ボタン2
	KEY_SPACE,			//スペースキー
	KEY_F1,				//F1キー
	KEY_F2,				//F2キー
	KEY_BTNMAX,			//キーボードの最大数　キー追加の際はこの上に記述
	KEY_MOUSE_LBTN = KEY_BTNMAX,//マウスの左ボタン
	KEY_MOUSE_RBTN,		//マウスの右ボタン
	KEY_MAX				//最大数
};

//デバッグ関連
//#define RELEASE
//と記述することでデバック用の関数を無効化する。
//各R付きの関数はシステム用のもの
#ifdef _DEBUG
#define	TToM Dbg_TilteToMessage
#define	FToM Dbg_FileOut
#define	BToM Dbg_BoxToMessage
#define	SToM Dbg_DisplayToMessage
#define	SToMNC Dbg_DisplayToMessageNC
#define	DFPS Draw_ShowFps
#else
#define	TToM __noop
#define	FToM __noop
#define	BToM __noop
#define	SToM __noop
#define	DFPS __noop
#endif

#define	TToMR	Dbg_TilteToMessage
#define	FToMR	Dbg_FileOut
#define	BToMR	Dbg_BoxToMessage
#define	SToMR	Dbg_DisplayToMessage
#define	SToMNCR Dbg_DisplayToMessageNC

enum
{
	DEC,	//半透明描画
	ADD,	//加算合成
	HALFADD,	//半加算合成
	MULTIPL,	//乗算合成
	SUBTRACT,	//減算
	BY,
	BF_MAX
};

//ダイアログ用
enum DLG_ERROR
{
	DLG_ERROR_INVALIDBUFFER,	//getのポインタがNULLになっている
	DLG_ERROR_CANCEL,			//キャンセルが押された
	DLG_OK
};

//三角関数関係
#define PAI  3.14159265358979323846f	//for Koyanagi!
#define PI   3.14159265358979323846f
#define PI_D 3.14159265358979323846

#define ROUND_X(Angle, Length, Center) static_cast<float>(( cos(Angle * PI / 180.0f) * Length + Center))
#define ROUND_Y(Angle, Length, Center) static_cast<float>((-sin(Angle * PI / 180.0f) * Length + Center))

//ホイール関連
enum WHEEL
{
	MWHEEL_UP = -1,
	MWHEEL_NEUTRAL,
	MWHEEL_DOWN,
};

//**************************************************************************************//
//外部変数
//**************************************************************************************//
extern	HWND		hWnd;				//ウインドウハンドル
extern	const char	USERNAME[256];		//タイトル名
extern	const int 	WINW;				//WINDOW幅
extern	const int	WINH;				//WINDOW高さ
extern	bool		WindowMode;			//ウインドウモード
extern	float		FrameTime;			//フレームタイム
//extern const int PLAYER_NUM;			//　プレイヤーの数
const int	PLAYER_NUM = 2;

extern	int			KeyboardMode;			//強制キーボードフラグ

//**************************************************************************************//
//extern 宣言
//**************************************************************************************//
void GameLoop();
int  DoWindow(HINSTANCE hInstance,HINSTANCE hPreInst,
							LPSTR lpszCmdLine,int nCmdShow);
void OnCreate();
void OnDestroy();

void InitDx();
void DelDx();

//カメラ制御
void SetLookAt(float x, float y);
void StepCamera();
void InitCamera(float x, float y, float addpx, float addpy);
//void SetPos(float x, float y);
void AddCameraForce(float x, float y);
void SetCameraTarget(float x,float y,int speed);
#define SetLookAtTarget SetCameraTarget
void SetScale(float scale);
float GetScale();

void AddScale(float add);
void AddScale(float add,int step);
POINT GetCameraPos();
POINT GetLookAt();
bool InScreenCamera(int x,int y,int w,int h);
bool InScreenCamera(RECT rt);
bool InScreenCameraCenter(float x, float y, float w, float h);

POINT GetCursorPosition();
POINT GetCursorPositionNC();
void ChangeScreenMouse(POINT& pt);
void GetCursorPosition(POINT& pt);
void GetCursorPositionNC(POINT* pt_ptr);

RECT GetScreenRect();

WHEEL GetWheelRoll();
int GetWheelDelta();
//描画関連
void Draw_Clear();
void Draw_Refresh();

//画像関連 
IMGOBJ Draw_LoadObject(const std::string& filename,
							D3DCOLOR Transparent = ARGB(255,0,0,0));
void Draw_DeleteObject(IMGOBJ	obj);
void Draw_SetRenderMode(int Mode);
void Draw_EndRenderMode();

void Draw_GraphicsCenter(
	int x, int y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_Graphics(
	int x, int y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_GraphicsCenter(
	float x, float y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_Graphics(
	float x, float y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_Line(
	int startX, int startY,
	int endX, int endY,
	float z, D3DCOLOR color, int size
);
void Draw_GraphicsCenterNC(
	int x, int y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_GraphicsNC(
	int x, int y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_GraphicsCenterNC(
	float x, float y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_GraphicsNC(
	float x, float y, float z,
	IMGOBJ img,
	int srcX, int srcY,
	int sizeX,int sizeY,
	int	degree = 0, POINT *center = nullptr,
	float	scaleX = 1.0f, float scaleY = 1.0f,
	u_char a = 255,
	u_char r = 255,
	u_char g = 255,
	u_char b = 255
);
void Draw_Box(
	int left, int top, int right, int bottom,
	float z,
	D3DCOLOR fillColor, D3DCOLOR frameColor,
	int lineSize, bool nakanuri
);
void Draw_BoxCenter(
	int x, int y, int sizeX, int sizeY,
	float z,
	D3DCOLOR fillColor, D3DCOLOR frameColor,
	int lineSize, bool nakanuri
);
void Draw_BoxNC(
	int left, int top, int right, int bottom,
	float z,
	D3DCOLOR fillColor, D3DCOLOR frameColor,
	int lineSize, bool nakanuri
);
void Draw_BoxCenterNC(
	int left, int top, int right, int bottom,
	float z,
	D3DCOLOR fillColor, D3DCOLOR frameColor,
	int lineSize, bool nakanuri
);
void Draw_LineNC(
	int startX, int startY,
	int endX, int endY,
	float z, D3DCOLOR color, int size
);
void Draw_CkRect(  const RECT& rt,D3DCOLOR color = ARGB(255,255,0,0));
void Draw_CkRectNC(const RECT& rt,D3DCOLOR color = ARGB(255,255,0,0));
D3DSURFACE_DESC Draw_GetImageSize(IMGOBJ	obj);
SIZE Draw_GetImageSize2(IMGOBJ obj);
unsigned int Draw_GetImageWidth(IMGOBJ obj);
unsigned int Draw_GetImageHeight(IMGOBJ obj);


//フォント関連	
void Draw_FontText(  int x, int y,   float z, const std::string& msg, D3DCOLOR color, int fontID);
void Draw_FontTextNC(int x, int y,   float z, const std::string& msg, D3DCOLOR color, int fontID);
int  Draw_FontText(  const RECT& rt, float z, const std::string& msg, D3DCOLOR color, int fontID);
int  Draw_FontTextNC(const RECT& rt, float z, const std::string& msg, D3DCOLOR color, int fontID);
void Draw_TextXY(  int x, int y, const std::string& msg, D3DCOLOR Textcolor, int fontID = 0);
void Draw_TextXYNC(int x, int y, const std::string& msg, D3DCOLOR Textcolor, int fontID = 0);
void Draw_CreateFont(int Num,int size,LPCTSTR	fontname);
void Draw_CreateFontItalic(int Num,int size,LPCTSTR	fontname);

//入力関連
void CheckKey();					
bool CheckPress(int keyno, int interval = 1,int player = 0);	//押されているかチェック
bool CheckAnyPress(int keyno);
bool CheckFree(int keyno, int interval = 1,int player = 0);		//はなされているかチェック
bool CheckPush(int keyno,int player = 0);		//押したかをチェック
bool CheckAnyPush(int keyno);
bool CheckPull(int keyno,int player = 0);		//はなしたかをチェック
int  GetKeyStatus(int keyno,int player = 0);	//キーの状態をチェック
int  GetKeyFrameHold(int keyno,int player = 0);	//押されているフレーム数を取得
int  GetKeyFrameFree(int keyno,int player = 0);	//はなされているフレーム数を取得
void InitKeyInfo(int player = 0);				//ステータスとフレームを初期化
void Key_GetKey(unsigned int Code,int* Flag);
void SetKeyFormat(CODE_KEY key, DWORD keycode,int playernum);
void SetKeysFormat(DWORD* keycode, int playernum);
bool IsUsePad(int playerID);

POINT GetMousePosition();				//マウスカーソルの座標を取得
void  GetMousePosition(POINT* pt_ptr);//マウスカーソルの座標を取得

//ステージ管理関連
void ChangeStage(int);
bool InitStage();
int  CheckStage();

//debug関連
void Dbg_FileOut(char *str,...);
void Dbg_ExitApp();//アプリケーションを終了させる要求をWIndowsに送ります（メモリリークに注意！）
void Dbg_DisplayToMessage(int x,int y,char * str,...);
void Dbg_TilteToMessage(char * str,...);
void Dbg_BoxToMessage(char *str,...);
void Dbg_StringOutXY(int x,int y,char* str,...);
void Draw_ShowFps();
DLG_ERROR	Dbg_InputDialog(char* get, int size, const char* title, const char* label);
void Dbg_DisplayToMessageNC(int x,int y,char * str,...);

#ifdef USE_DIRECTSHOW
typedef int DSHOWOBJ;
//BGM再生関数
bool DShow_Init();
void DShow_Del();
DSHOWOBJ DShow_LoadFile(char* filename);
bool DShow_Play(DSHOWOBJ index);
void DShow_RateControl(DSHOWOBJ index,float rate);
void DShow_EndCheck();
void DShow_Stop(DSHOWOBJ index);
void DShow_AllStop();
LONGLONG DShow_GetCurrentPos(DSHOWOBJ index);
LONGLONG DShow_GetEndPosition(int index);
void DShow_SetStartPos(int index);
void DShow_VolumeControl(DSHOWOBJ index,int volume);
#endif

#ifdef USE_DIRECTSOUND
typedef int DSOUNDOBJ;
//SE再生関数
void DSound_Init();
bool DSound_Create();
void DSound_CreateSecondaryBuffer();
bool DSound_CreatePrimaryBuffer();
bool DSound_Del();
void DSound_Play(DSOUNDOBJ SoundNo);
void DSound_PlayLoop(DSOUNDOBJ SoundNo);
bool DSound_Del();
DSOUNDOBJ	DSound_LoadFile( char* file );
void DSound_AllStop();
void DSound_Stop(DSOUNDOBJ SoundNo);
void DSound_EndCheck();
void DSound_SetFrequency(DSOUNDOBJ SoundNo,int Fre);
void DSound_SetVolume(DSOUNDOBJ SoundNo,int Vol);
#endif

//数学関連
bool RectCheck(const RECT& rt1,const RECT& rt2);
bool RectCheck(const RECT *rt1,const RECT *rt2);
bool IsFrameOut_Center(float x, float y, float width, float height);
bool IsFrameOut_LeftTop(float x, float y, float width, float height);

float Calc_XYToRad(int x,int y);		//座標XYへの角度を求める
float Calc_XYToSinCos(int x,int y,float *addx,float *addy);	//座標XYへの移動量XYを求める
float Calc_RadToDegree(float rad);		//ラジアンから角度へ
float Calc_DegreeToRad(float degree);		//角度からラジアンへ
int   SetRandomCount(int Max);
int   SetRandomCount(int Max,int Min);
//点と点の角度を求める（-180~180）
float RadianOfPoints2(float basisX, float basisY, float targetX, float targetY);
float DegreeOfPoints2(float basisX, float basisY, float targetX, float targetY);
//二点間の距離をもとめる
float Distance2(float x1, float y1, float x2, float y2);

//高精度タイマー関連
void  Time_ResetTimer(int id);
void  Time_StartTimer(int id);
bool  Time_MarkOfOneSec(int id);
bool  Time_MarkOfTime(int id,float marktime);
float Time_GetTimerCount();
float Time_GetTimerCount(int id);
float Time_Update();
float Time_GetOneFrameCount();
void  Time_StartTimerCount();
bool  Time_CheckTimer();

////拡張機能を使わない場合はコメントアウトすること
//#include "ci_ext/ci_ext.hpp"

#ifdef CI_EXT
bool IsFrameOut_Center(const ci_ext::Vec3f& pos, const ci_ext::Vec3f& size);
bool IsFrameOut_LeftTop(const ci_ext::Vec3f& pos, const ci_ext::Vec3f& size);
#endif
