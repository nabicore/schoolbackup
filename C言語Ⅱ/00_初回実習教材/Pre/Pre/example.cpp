﻿/*
//例　デバッグ練習1 for
#include<iostream>
using namespace std;
int main()
{
  int total = 0;
  for (int i = 1;
	i <= 1000;
	++i)
  {
	total += i;
  }
  cout << total;
}
//*/

/*
//例1　フィボナッチ数列
#include<iostream>
using namespace std;
int main()
{
  int tmp = 1;
  int v1 = 0;
  int v2 = 0;
  for (int i = 0; i < 11; ++i)
  {
	v1 = v2;
	v2 = tmp;
	tmp = v1 + v2;
  }//★プリントでは13行目だが、こちらの行で変数値の確認を行うこと
  cout << tmp << " " << v1 << " " << v2;
}
//*/

//*
//例2　単純な数列合計値算出
#include<iostream>
using namespace std;
int Sum1(int begin, int end)
{
	int test = 0;
	for (int i = begin; i <= end; ++i)
		test += i;
	return test;
}

int Sum2(int begin, int end)
{
	return (begin + end) * (end - begin + 1) / 2;
}
int main()
{
	int total = Sum1(1, 10) + Sum2(10, 20); //★
	cout << total << endl;
}
//*/