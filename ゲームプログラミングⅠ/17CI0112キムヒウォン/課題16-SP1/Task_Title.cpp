#include "MyPG.h"
#include "MyGameMain.h"


namespace Title {
	int logoPosY;

	void Initialize() {
		DG::Image_Create("TitleImg", "./data/image/Title.bmp");
		logoPosY = -270;
	}

	void Finalize() {
		DG::Image_Erase("TitleImg");
	}

	TaskFlag UpDate() {
		DI::VGamePad in1 = DI::GPad_GetState("P1");
		logoPosY += 9;
		if (logoPosY > 0)
			logoPosY = 0;

		TaskFlag rtv = Task_Title;
		if (logoPosY >= 0) {
			if (in1.ST.down)
				rtv = Task_NewGame;
			if (in1.SE.down)
				rtv = Task_LoadGame;
		}
		return rtv;
	}

	void Render() {
		ML::Box2D draw(0, 0, 480, 270);
		draw.Offset(0, logoPosY);
		ML::Box2D src(0, 0, 240, 135);

		DG::Image_Draw("TitleImg", draw, src);
	}
}