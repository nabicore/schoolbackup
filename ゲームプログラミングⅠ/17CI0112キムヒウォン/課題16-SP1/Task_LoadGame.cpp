#include "MyPG.h"
#include "MyGameMain.h"


namespace LoadGame {

	void Initialize() {
		DG::Font_Create("FontA", "MS ゴシック", 16, 32);
		charaPos.x = ge->screenWidth - 32;
		charaPos.y = ge->screenHeight - 16;
	}

	void Finalize() {
		DG::Font_Erase("FontA");
	}

	TaskFlag UpDate() {
		DI::VGamePad in1 = DI::GPad_GetState("P1");

		string dbg = std::to_string(charaPos.x) + " / " + std::to_string(charaPos.y) + "\n";
		OutputDebugString(dbg.c_str());

		TaskFlag rtv = Task_LoadGame;
		if (in1.ST.down)
			rtv = Task_Game;
		return rtv;
	}

	void Render() {
		string msg = "ロードゲームです。セーブファイルを選んで\nそのデータでゲームを初期化します（本来は）";
		DG::Font_Draw("FontA", ML::Box2D(0, 0, 480, 100), msg, ML::Color(0.8f, 0.5f, 0.f, 1.f));
	}
}