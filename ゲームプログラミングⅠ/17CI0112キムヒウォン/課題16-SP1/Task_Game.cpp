#include "MyPG.h"
#include "MyGameMain.h"


POINT charaPos;
namespace Game {
	POINT	myCharaPos;

	void Initialize() {
		DG::Image_Create("BGImg", "./data/image/GameBG.bmp");
		DG::Image_Create("PlayerImg", "./data/image/Chara00.png");
		myCharaPos = charaPos;
	}

	void Finalize() {
		DG::Image_Erase("BGImg");
		DG::Image_Erase("PlayerImg");
	}

	TaskFlag UpDate() {
		DI::VGamePad in1 = DI::GPad_GetState("P1");
		if (in1.LStick.L.on) myCharaPos.x -= 3;
		if (in1.LStick.R.on) myCharaPos.x += 3;
		if (in1.LStick.U.on) myCharaPos.y -= 3;
		if (in1.LStick.D.on) myCharaPos.y += 3;

		string dbg = std::to_string(charaPos.x) + " / " + std::to_string(charaPos.y) + "\n";
		OutputDebugString(dbg.c_str());

		TaskFlag rtv = Task_Game;
		if (in1.ST.down)
			rtv = Task_End;
		if (in1.SE.down)
			rtv = Task_Title;
		return rtv;
	}

	void Render() {
		{
			ML::Box2D draw(0, 0, 480, 270);
			DG::Image_Draw("BGImg", draw, draw);
		}
		{
			ML::Box2D draw(-32, -16, 64, 32);
			draw.Offset(myCharaPos.x, myCharaPos.y);
			ML::Box2D src(0, 0, 64, 32);
			DG::Image_Draw("PlayerImg", draw, src);
		}
	}
}