#include "MyGameMain.h"

//ゲーム情報
DI::Mouse			mouse;
int					posX, posY;

enum State { Normal, Hit, Non };

struct Target {
	State		target_state;
	int			target_x;
	int			target_y;
	ML::Box2D	target_hitBase;
	int			target_timeCnt;
	int			target_timeMax;
	int			target_score;
	int			target_limit;
};
Target target[3];

int			score = 0;


void Target_Initialize(Target& t_, int kind_);
void Target_Draw(Target& t_);
void Target_UpDate(Target& t_);
int Target_HitCheck(Target& t_, int x_, int y_);


//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	srand((unsigned int)time(NULL));
	DG::Image_Create("TargetImg", "./data/image/Target.bmp");
	posX = 0;
	posY = 0;

	DG::Font_Create("FontA", "MSゴシック", 16, 32);

	for (int i = 0; i < 3; ++i)
		Target_Initialize(target[i], 0);
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TargetImg");
	DG::Font_Erase("FontA");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();
	posX = mouse.cursorPos.x;
	posY = mouse.cursorPos.y;

	for (auto& t : target) {
		Target_UpDate(t);
		if (Target_HitCheck(t, posX, posY)) {
			t.target_state = Hit;
			score += t.target_score;
		}
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	for (auto& t : target)
		Target_Draw(t);

	ML::Box2D textBox(0, 0, 480, 32);
	string text = "得点：" + to_string(score);
	DG::Font_Draw("FontA", textBox, text, ML::Color(0.8f, 0.5f, 0.f, 1.f));
}





//======================================================================
void Target_Initialize(Target& t_, int kind_) {
	t_.target_state = Normal;
	t_.target_x = -1000;
	t_.target_y = 0;
	t_.target_hitBase = ML::Box2D(0, 0, 64, 64);
	t_.target_timeCnt = 0;
	t_.target_timeMax = 60;
	t_.target_score = 100;
	t_.target_limit = 10 * 60;
}

void Target_Draw(Target& t_) {
	if (t_.target_state != Non) {
		ML::Box2D draw = t_.target_hitBase;
		ML::Box2D src(0, 0, 10, 10);
		if (t_.target_state == Normal) {
			src = ML::Box2D(0, 0, 64, 64);
		}
		if (t_.target_state == Hit) {
			src = ML::Box2D(64, 0, 64, 64);
		}
		draw.x += t_.target_x;
		draw.y += t_.target_y;
		DG::Image_Draw("TargetImg", draw, src);
	}
}

void Target_UpDate(Target& t_) {


	if (t_.target_state != Non) {
		t_.target_limit--;
		if (t_.target_limit < 0) {
			t_.target_state = Non;
		}
		else {
			t_.target_timeCnt++;
			if (t_.target_timeCnt >= t_.target_timeMax) {
				t_.target_timeCnt = 0;
				t_.target_state = Normal;
				t_.target_x = rand() % (480 - 64);
				t_.target_y = rand() % (270 - 64);
			}
		}
	}

}

int Target_HitCheck(Target& t_, int x_, int y_) {

	if (mouse.LB.down) {
		if (t_.target_state == Normal) {
			ML::Box2D hit = t_.target_hitBase;
			hit.x += t_.target_x;
			hit.y += t_.target_y;
			if (hit.x <= posX && posX < hit.x + hit.w &&
				hit.y <= posY && posY < hit.y + hit.h) {
				return 1;
			}
		}
	}

	return 0;
}