#include "MyGameMain.h"
#include <array>

//ゲーム情報

const int ONE_SECOND = 60;
class Target {
	ML::Box2D pos;
	ML::Box2D crop;

	int target_score = 100;
	int frameCnt;
	bool isDead = false;

private:
	inline void randomPos() {
		pos.x = rand() % (ge->screenWidth - crop.w);
		pos.y = rand() % (ge->screenHeight - crop.h);
	}

public:
	explicit Target(const ML::Box2D p, const ML::Box2D c) :
		pos(p), crop(c) {}
	virtual ~Target() {}


	/**
		@brief		初期化を実行します
	*/
	void Initialize() {
		randomPos();
	}

	/**
		@brief		一秒ごと位置をランダムに動く
		@return		なし
	*/
	void UpdatePosition() {
		frameCnt += 1;
		if (frameCnt >= ONE_SECOND) {
			isDead = false;
			frameCnt = 0;
			randomPos();
		}

		if (frameCnt >= ONE_SECOND / 2)
			target_score = 50;
		else
			target_score = 100;

		string dbg = "フレーム経過：" + std::to_string(frameCnt) + "\n";
		OutputDebugString(dbg.c_str());
	}
	
	inline const bool isCollide(ML::Vec2 t) {
		return pos.Hit(t);
	}

	inline void Hit() {
		isDead = true;
	}

	inline bool isHited() {
		return isDead;
	}

	inline const ML::Box2D Position() {
		return pos;
	}

	inline const ML::Box2D Source() {
		return crop;
	}

	inline const int MyScore() {
		return target_score;
	}
};


DI::Mouse mouse;
static int score = 0;
Target target(ML::Box2D(0, 0, 64, 64), ML::Box2D(0, 0, 64, 64));

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize()
{
	DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
	DG::Image_Create("Target", "./data/image/Target.bmp");
	DG::Font_Create("FontA", "MS ゴシック", 16, 32);

	target.Initialize();
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TestImg");
	DG::Image_Erase("MapChipImg");
	DG::Image_Erase("Target");
	DG::Font_Erase("FontA");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();
	ML::Vec2 mousePos(mouse.cursorPos.x, mouse.cursorPos.y);
	target.UpdatePosition();
	if (mouse.LB.on && target.isCollide(mousePos) && !target.isHited()) {
		score += target.MyScore();
		target.Hit();
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	DG::Image_Draw("Target", target.Position(), target.Source());
	DG::Font_Draw("FontA", 
				  ML::Box2D(0, 0, 480, 32), 
				  "得点：" + std::to_string(score), 
				  ML::Color(0.8f, 0.5f, 0.f, 1.f));
}
