#include "MyGameMain.h"
#include <array>

//ゲーム情報

// 方向の定義
enum Direction { UP = 0, DOWN };
std::array<int, 2> dir{
	-1,
	1
};

int currentDir = dir[Direction::DOWN];
ML::Box2D slimePos(0, 0, 64, 48);
ML::Box2D slimeSrc(0, 0, 64, 48);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TestImg");
	DG::Image_Erase("MapChipImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	if (slimePos.y < 0)
		currentDir = dir[Direction::DOWN];
	else if (slimePos.y > ge->screenHeight - slimeSrc.h)
		currentDir = dir[Direction::UP];
	slimePos.y += currentDir;

	// Debug
	string dbg = std::to_string(slimePos.x) + ", " + std::to_string(slimePos.y) + "\n";
	OutputDebugString(dbg.c_str());
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	DG::Image_Draw("TestImg", slimePos, slimeSrc);
}
