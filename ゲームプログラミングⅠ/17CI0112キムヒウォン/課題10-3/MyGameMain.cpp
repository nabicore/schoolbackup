#include "MyGameMain.h"

//ゲーム情報
ML::Box2D me;
int speed;
DI::VGamePad in1;

struct Object {
	bool flag;
	bool active;
	ML::Box2D hit;
	int moveCnt;
};

Object obj[1000];

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize()
{
	DG::Image_Create("BoxRImg", "./data/image/boxRed.bmp");
	DG::Image_Create("BoxBImg", "./data/image/boxBlue.bmp");

	DI::AnalogAxisKB ls = { DIK_LEFT, DIK_RIGHT, DIK_UP, DIK_DOWN };
	DI::AnalogAxisKB rs = { 0, };
	DI::KeyDatas_KB key = {
		{ DIK_Z, DI::B1 }, { DIK_X, DI::B2 },
		{ DIK_C, DI::B3 }, { DIK_V, DI::B4 }
	};

	DI::GPad_CreateKB("P1", ls, rs, key);

	me = ML::Box2D(0, 0, 32, 32);
	speed = 2;

	for (auto& o : obj) {
		o.active = true;
		o.moveCnt = rand() % 160;
		o.hit = ML::Box2D(rand() % 435 + 40, rand() % 225 + 40, 4, 4);
	}
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("BoxRImg");
	DG::Image_Erase("BoxLImg");
	DI::GPad_Erase("P1");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	in1 = DI::GPad_GetState("P1");
	if (in1.LStick.L.on) me.x -= speed;
	if (in1.LStick.R.on) me.x += speed;
	if (in1.LStick.U.on) me.y -= speed;
	if (in1.LStick.D.on) me.y += speed;
	if (in1.B1.down) speed += 1;
	if (in1.B2.down) speed -= 1;
	if (speed < 1) speed = 1;
	if (speed > 4) speed = 4;

	for (auto& o : obj) {
		o.flag = false;
		if (o.hit.Hit(me) && o.active) {
			o.active = false;
			o.flag = true;
		}

		if (o.active) {
			if (o.moveCnt < 40)
				o.hit.y += 1;
			else if (o.moveCnt < 80)
				o.hit.x += 1;
			else if (o.moveCnt < 120)
				o.hit.y -= 1;
			else if (o.moveCnt < 160)
				o.hit.x -= 1;
			else
				o.moveCnt = 0;
			o.moveCnt += 1;
		}
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	ML::Box2D src(0, 0, 64, 64);

	for (auto& o : obj) {
		if (o.active) {
			if (o.flag)
				DG::Image_Draw("BoxRImg", o.hit, src);
			else
				DG::Image_Draw("BoxBImg", o.hit, src);
		}
	}

	DG::Image_Draw("BoxBImg", me, src, ML::Color(0.5f, 1.f, 1.f, 1.f));
}
