#include "Animation.h"



void Animation::Update() {
	timeCnt++;
	if (timeCnt >= interval) {
		if (currentFrame >= (int)(srcs.size() - 1))
			currentFrame = 0;
		else
			currentFrame++;
		timeCnt = 0;
	}
}


void Animation::Render() {
	ML::Box2D pos = ML::Box2D(x, y, srcs[currentFrame].w, srcs[currentFrame].h);
	DG::Image_Draw(spritePackName, pos, srcs[currentFrame]);
}