#pragma once
#include "MyPG.h"



class Animation {

	string					spritePackName;
	int						timeCnt = 0;
	int						interval = 10;
	int						currentFrame = 0;
	int						x = 0;
	int						y = 0;
	vector<ML::Box2D>		srcs;

public:
	Animation(string spn, const vector<ML::Box2D>& ss) : 
		spritePackName(spn), srcs(ss) {}
	Animation(string spn, const vector<ML::Box2D>& ss, int i) :
		spritePackName(spn), srcs(ss), interval(i) {}
	~Animation() {}

	void Update();
	void Render();

	inline void SetPosition(const int& x_, const int& y_) { x = x_; y = y_; }
	inline const int GetPositionX() { return x; }
	inline const int GetPositionY() { return y; }
	inline const ML::Box2D GetHitbox() { return ML::Box2D(x, y, srcs[currentFrame].w, srcs[currentFrame].h); }
};