#include "PatternLoader.h"



PatternLoader::PatternLoader(string fileName) {
	if (!Initialize(fileName))
		OutputDebugString("テキストファイル読み込みを失敗しました!");
}

PatternLoader::PatternLoader(string fileName, bool isLoop) {
	this->isLoop = isLoop;
	if (!Initialize(fileName))
		OutputDebugString("テキストファイル読み込みを失敗しました!");
}

bool PatternLoader::Initialize(string fileName) {
	FILE* fp = fopen(fileName.c_str(), "r");
	if (!fp) 
		return false;

	ML::Vec3 ptn;
	while (!feof(fp)) {
		fscanf_s(fp, "%f %f %f", &ptn.x, &ptn.y, &ptn.z);
		patterns.push_back(ptn);
		maxPattern = patterns.size();
	}
	fclose(fp);
	
	return true;
}

void PatternLoader::Update() {
	if (curPattern < maxPattern) {
		tick++;
		if (tick > patterns[curPattern].z) {
			tick = 0;
			if (curPattern < maxPattern - 1)
				curPattern++;
			else {
				if (isLoop)
					curPattern = 0;
				else
					curPattern = maxPattern - 1;
			}
		}
	}
}

ML::Vec2 PatternLoader::GetPattern() {
	return ML::Vec2(patterns[curPattern].x, patterns[curPattern].y);
}