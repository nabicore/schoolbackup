#pragma once
#include "MyPG.h"


class PatternLoader {
	vector<ML::Vec3>		patterns;
	int						curPattern = 0;
	int						maxPattern = 0;
	int						tick = 0;
	bool					isLoop = false;

private:
	bool Initialize(string fileName);

public:
	PatternLoader(string fileName);
	PatternLoader(string fileName, bool isLoop);
	virtual ~PatternLoader() {}
	


	void Update();

	/*
		@brief		現在パタンの動きです。
		@return		x: x方向増量、y: y方向増量
	*/
	ML::Vec2 GetPattern();
};