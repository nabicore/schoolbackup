#include "MyGameMain.h"

//ゲーム情報
DI::Mouse						mouse;
int								posX, posY;
int								score;

enum State {
	Normal,
	Hit,
	Non
};

struct Target {
	State						state;
	int							x;
	int							y;
	ML::Box2D					hitBase;
	int							timeCnt;
	int							score;
	int							moveX;
	int							moveY;
};
Target							target[20];


void Target_Invalidate(Target& t_, int kind_);
void Target_Initialize(Target& t_);
void Target_Draw(Target& t_);
void Target_UpDate(Target& t_);
int Target_HitCheck(Target& t_, int x_, int y_);


//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Font_Create("FontA", "MS ゴシック", 16, 32);
	DG::Image_Create("TargetImg", "./data/image/Target2.png");
	srand((unsigned int)time(NULL));

	posX = 0;
	posY = 0;
	score = 0;
	
	for (auto& t : target)
		Target_Initialize(t);
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Font_Erase("FontA");
	DG::Image_Erase("TargetImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();
	posX = mouse.cursorPos.x;
	posY = mouse.cursorPos.y;

	for (auto& t : target) {
		Target_UpDate(t);
		if (Target_HitCheck(t, posX, posY)) {
			t.state = State::Hit;
			score += t.score;
		}
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	for (auto& t : target)
		Target_Draw(t);

	DG::Font_Draw("FontA", ML::Box2D(0, 0, 600, 60), "得点：" + std::to_string(score), ML::Color(0.8f, 0.5f, 0.f, 1.f));
}











// ================================================================================
void Target_Invalidate(Target& t_, int kind_) {

}

void Target_Initialize(Target& t_) {
	t_.state = State::Normal;
	t_.timeCnt = rand() % 300;
	t_.x = -1000;
	t_.y = 0;
	t_.moveX = 0;
	t_.moveY = 0;
	t_.score = 100;
	t_.hitBase = ML::Box2D(0, 0, 40, 40);
}

void Target_Draw(Target& t_) {
	if (t_.state != State::Non) {
		auto draw = t_.hitBase;
		auto src = ML::Box2D(0, 0, 10, 10);
		switch (t_.state) {
		case State::Normal:
			src = ML::Box2D(0, 0, 40, 40);
			break;
		case State::Hit:
			src = ML::Box2D(80, 0, 40, 40);
			break;
		}
		draw.x += t_.x;
		draw.y += t_.y;
		DG::Image_Draw("TargetImg", draw, src);
	}
}

void Target_UpDate(Target& t_) {
	// 動かす部分
	if (t_.state != State::Non) {
		t_.timeCnt += 1;

		if (t_.timeCnt >= 300) {
			t_.timeCnt = 0;
			t_.state = Normal;

			int side = rand() % 2;
			switch (side) {
			case 0:		// 上から下
				t_.x = rand() % (ge->screenWidth - 40);
				t_.y = -40;
				t_.moveX = 0;
				t_.moveY = 2;
				break;
			case 1:		// 下から上
				t_.x = rand() % (ge->screenWidth - 40);
				t_.y = ge->screenHeight + 40;
				t_.moveX = 0;
				t_.moveY = -2;
				break;
			}
		}
		t_.x += t_.moveX;
		t_.y += t_.moveY;
	}
}

int Target_HitCheck(Target& t_, int x_, int y_) {
	// あたり判定
	if (t_.state != State::Hit) {
		ML::Box2D hit = t_.hitBase;
		hit.x += t_.x;
		hit.y += t_.y;

		if (hit.x <= x_ && x_ < hit.x + hit.w &&
			hit.y <= y_ && y_ < hit.y + hit.h) {
			return 1;
		}
	}

	return 0;
}
