#include "MyGameMain.h"
#include <array>

//ゲーム情報

class Slime {
	ML::Box2D pos;
	ML::Box2D crop;

public:
	Slime(const ML::Box2D& p, const ML::Box2D c) :
		pos(p), crop(c) {}

	inline bool Hit(const ML::Vec2& t) {
		if (pos.Hit(t)) {
			string dbg = "Hit! - " + std::to_string(pos.x) + "\n";
			OutputDebugString(dbg.c_str());
			return true;
		}
		return false;
	}

	inline void SetSlimeSource(const ML::Box2D& c) {
		crop = c;
	}

	inline const ML::Box2D Position() {
		return pos;
	}

	inline const ML::Box2D Source() {
		return crop;
	}
};
const ML::Box2D slimeSrc_blue(64, 48, 64, 48);
const ML::Box2D slimeSrc_pink(0, 0, 64, 48);




DI::Mouse mouse;
std::array<Slime, 3> slimes {
	Slime(ML::Box2D(100, 100, 64, 48), slimeSrc_blue),
	Slime(ML::Box2D(200, 100, 64, 48), slimeSrc_blue),
	Slime(ML::Box2D(300, 100, 64, 48), slimeSrc_blue)
};

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize()
{
	DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TestImg");
	DG::Image_Erase("MapChipImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();

	for (int i = 0; i < slimes.size(); i++) {
		if (mouse.LB.on && slimes[i].Hit(ML::Vec2(mouse.cursorPos.x, mouse.cursorPos.y)))
			slimes[i].SetSlimeSource(slimeSrc_pink);
		if (mouse.LB.off)
			slimes[i].SetSlimeSource(slimeSrc_blue);
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	for (auto slime : slimes)
		DG::Image_Draw("TestImg", slime.Position(), slime.Source());
}
