#include "MyGameMain.h"
#include <array>

//ゲーム情報

const ML::Box2D slimeSrc_blue(64, 48, 64, 48);
const ML::Box2D slimeSrc_pink(0, 0, 64, 48);

DI::Mouse mouse;
ML::Box2D slimePos(100, 100, 64, 48);
ML::Box2D slimeSrc_current(slimeSrc_blue);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize()
{
	DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TestImg");
	DG::Image_Erase("MapChipImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();
	if (mouse.LB.on && 
		slimePos.Hit(ML::Vec2(mouse.cursorPos.x, mouse.cursorPos.y)))
		slimeSrc_current = slimeSrc_pink;
	if (mouse.LB.off)
		slimeSrc_current = slimeSrc_blue;
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	DG::Image_Draw("TestImg", slimePos, slimeSrc_current);
}
