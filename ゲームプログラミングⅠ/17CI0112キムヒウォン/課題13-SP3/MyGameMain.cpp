#include "MyGameMain.h"

//ゲーム情報
ML::Box2D me;
int speed;
int score;
DI::VGamePad in1;
DI::Mouse mouse;

struct Object {
	bool flag;
	bool active;
	ML::Box2D hit;
	int moveCnt;
};

Object obj[1000];


void Object_Initialize(Object& o_);
void Object_Draw(Object& o_);
void Object_UpDate(Object& o_);
bool Object_HitCheck(Object& o_, ML::Box2D b_);


//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize()
{
	DG::Image_Create("BoxRImg", "./data/image/boxRed.bmp");
	DG::Image_Create("BoxBImg", "./data/image/boxBlue.bmp");
	DG::Font_Create("FontA", "MS ゴシック", 16, 32);

	DI::AnalogAxisKB ls = { DIK_LEFT, DIK_RIGHT, DIK_UP, DIK_DOWN };
	DI::AnalogAxisKB rs = { 0, };
	DI::KeyDatas_KB key = {
		{ DIK_Z, DI::B1 }, { DIK_X, DI::B2 },
		{ DIK_C, DI::B3 }, { DIK_V, DI::B4 }
	};

	DI::GPad_CreateKB("P1", ls, rs, key);

	me = ML::Box2D(0, 0, 32, 32);
	speed = 2;

	for (auto& o : obj)
		Object_Initialize(o);
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("BoxRImg");
	DG::Image_Erase("BoxLImg");
	DI::GPad_Erase("P1");
	DG::Font_Erase("FontA");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	// キーボード操作
	in1 = DI::GPad_GetState("P1");
	if (in1.LStick.L.on) me.x -= speed;
	if (in1.LStick.R.on) me.x += speed;
	if (in1.LStick.U.on) me.y -= speed;
	if (in1.LStick.D.on) me.y += speed;
	if (in1.B1.down) speed += 1;
	if (in1.B2.down) speed -= 1;
	if (speed < 1) speed = 1;
	if (speed > 4) speed = 4;

	// マウス操作
	mouse = DI::Mouse_GetState();
	me.x = mouse.cursorPos.x - (me.w / 2);
	me.y = mouse.cursorPos.y - (me.h / 2);

	for (auto& o : obj) {
		o.flag = false;
		if (Object_HitCheck(o, me)) {
			score++;
			o.active = false;
			o.flag = true;
		}
		Object_UpDate(o);
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	ML::Box2D src(0, 0, 64, 64);

	for (auto& o : obj)
		Object_Draw(o);

	DG::Image_Draw("BoxBImg", me, src, ML::Color(0.5f, 1.f, 1.f, 1.f));
	DG::Font_Draw("FontA",
		ML::Box2D(0, ge->screenHeight - 32, 480, 32),
		"ぱっくんちょした数：" + std::to_string(score),
		ML::Color(0.8f, 0.5f, 0.f, 1.f));
}




// =====================================================================================
void Object_Initialize(Object& o_) {
	o_.active = true;
	o_.moveCnt = rand() % 160;
	o_.hit = ML::Box2D(rand() % 435 + 40, rand() % 225 + 40, 4, 4);
}

void Object_Draw(Object& o_) {
	ML::Box2D src(0, 0, 64, 64);
	if (o_.active) {
		if (o_.flag)
			DG::Image_Draw("BoxRImg", o_.hit, src);
		else
			DG::Image_Draw("BoxBImg", o_.hit, src);
	}
}

void Object_UpDate(Object& o_) {
	if (o_.active) {
		if (o_.moveCnt < 40)
			o_.hit.y += 1;
		else if (o_.moveCnt < 80)
			o_.hit.x += 1;
		else if (o_.moveCnt < 120)
			o_.hit.y -= 1;
		else if (o_.moveCnt < 160)
			o_.hit.x -= 1;
		else
			o_.moveCnt = 0;
		o_.moveCnt += 1;
	}
}

bool Object_HitCheck(Object& o_, ML::Box2D b_) {
	return o_.hit.Hit(b_) && o_.active;
}

