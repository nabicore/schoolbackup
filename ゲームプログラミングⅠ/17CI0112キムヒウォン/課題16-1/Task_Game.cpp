#include "MyPG.h"
#include "MyGameMain.h"


namespace Game {
	POINT		charaPos;

	void Initialize() {
		DG::Image_Create("BGImg", "./data/image/GameBG.bmp");
		DG::Image_Create("PlayerImg", "./data/image/Chara00.png");
		charaPos.x = ge->screenWidth / 2;
		charaPos.y = ge->screenHeight * 2 / 3;
	}

	void Finalize() {
		DG::Image_Erase("BGImg");
		DG::Image_Erase("PlayerImg");
	}

	TaskFlag UpDate() {
		DI::VGamePad in1 = DI::GPad_GetState("P1");
		if (in1.LStick.L.on) charaPos.x -= 3;
		if (in1.LStick.R.on) charaPos.x += 3;
		if (in1.LStick.U.on) charaPos.y -= 3;
		if (in1.LStick.D.on) charaPos.y += 3;

		TaskFlag rtv = Task_Game;
		if (in1.ST.down)
			rtv = Task_End;
		if (in1.SE.down)
			rtv = Task_Title;
		return rtv;
	}

	void Render() {
		{
			ML::Box2D draw(0, 0, 480, 270);
			DG::Image_Draw("BGImg", draw, draw);
		}
		{
			ML::Box2D draw(-32, -16, 64, 32);
			draw.Offset(charaPos.x, charaPos.y);
			ML::Box2D src(0, 0, 64, 32);
			DG::Image_Draw("PlayerImg", draw, src);
		}
	}
}