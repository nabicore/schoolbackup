#include "MyGameMain.h"

//ゲーム情報
struct Object {
	bool	active;
	float	x;
	float	y;
	float	angle;
};

DI::Mouse	mouse;
Object		obj;

void Object_Initialize(Object& o_, float ang_);
void Object_Draw(Object& o_);
void Object_UpDate(Object& o_);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Image_Create("RotTestImg", "./data/image/RotTest.bmp");
	DG::Font_Create("FontA", "MSゴシック", 12, 24);
	obj.active = false;
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("RotTestImg");
	DG::Font_Erase("FontA");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();
	if (mouse.LB.down) {
		int mx = mouse.cursorPos.x - (ge->screenWidth / 2);
		int my = mouse.cursorPos.y - (ge->screenHeight / 2);

		if (mx != 0 || my != 0) {
			float angle = atan2(my, mx);
			Object_Initialize(obj, angle);
		}
	}

	Object_UpDate(obj);
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	Object_Draw(obj);

	if (obj.active) {
		DG::Font_Draw("FontA",
			ML::Box2D(0, 0, 480, 32),
			"現在：" + std::to_string(ML::ToDegree(obj.angle)) + "度の方向に進んでいます",
			ML::Color(0.8f, 0.5f, 0.f, 1.f));
	}
}





void Object_Initialize(Object& o_, float ang_) {
	o_.active = true;
	o_.x = 240.f;
	o_.y = 135.f;
	o_.angle = ang_;
}

void Object_Draw(Object& o_) {
	if (o_.active) {
		ML::Box2D draw(-16, -16, 32, 32);
		ML::Box2D src(0, 0, 32, 32);
		draw.Offset(o_.x, o_.y);
		DG::Image_Rotation("RotTestImg", o_.angle, ML::Vec2(0, 0));
		DG::Image_Draw("RotTestImg", draw, src);
	}
}

void Object_UpDate(Object& o_) {
	if (o_.active) {
		float mx = cos(o_.angle);
		float my = sin(o_.angle);
		o_.x += mx * 3;
		o_.y += my * 3;
	}
}