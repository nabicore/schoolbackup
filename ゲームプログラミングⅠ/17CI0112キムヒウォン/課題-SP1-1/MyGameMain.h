#pragma once
#include "MyPG.h"

enum TaskFlag {
	Task_Non,
	Task_Title,
	Task_Game,
	Task_End
};

namespace Title {
	extern void Initialize();
	extern TaskFlag UpDate();
	extern void Render();
	extern void Finalize();
}

namespace Game {
	extern void Initialize();
	extern TaskFlag UpDate();
	extern void Render();
	extern void Finalize();
}

namespace End {
	extern void Initialize();
	extern TaskFlag UpDate();
	extern void Render();
	extern void Finalize();
}

extern  void  MyGameMain_Finalize( );
extern  void  MyGameMain_Initalize( );
extern  void  MyGameMain_UpDate( );
extern  void  MyGameMain_Render2D( );
