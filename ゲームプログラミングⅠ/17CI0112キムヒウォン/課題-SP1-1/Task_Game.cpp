#include "MyPG.h"
#include "Animation.h"
#include "PatternLoader.h"
#include "MyGameMain.h"


namespace Game {
	//ゲーム情報
	DI::VGamePad			in1;

	typedef struct _tagChara {
		bool				active;
		int					x, y;
		int					delay;
		int					moveCnt;
		ML::Box2D			hitBase;
		Animation*			animation;
		PatternLoader*		myPattern;
	}Chara;

	Chara player;
	Chara enemy[30];
	Chara shots[10];
	Chara shotBoom[10];
	Chara backs[2];

	void Player_Draw(Chara& c_);
	void Shot_Initialize(Chara& c_, int x_, int y_);
	void Shot_Draw(Chara& c_, bool isBoom);
	void Shot_UpDate(Chara& c_, bool isBoom);
	void Back_Initialize(Chara& b_, int x_, int y_);


	void Initialize() {
		
		//　背景画像を読み込む
		DG::Image_Create("BackImg", "./data/image/back.png");
		for (int b = 0; b < 2; b++)
			Back_Initialize(backs[b], b * 827, 0);

		//　画像の読み込み
		DG::Image_Create("PlayerImg", "./data/image/player.png");
		DG::Image_Create("Enemy1Img", "./data/image/enemy1.png");
		DG::Image_Create("Enemy2Img", "./data/image/enemy2.png");
		DG::Image_Create("Enemy3Img", "./data/image/enemy3.png");
		DG::Image_Create("ShotImg", "./data/image/Shot.png");

		//　プレイヤーの初期化
		player.active = true;
		player.x = 50;
		player.y = 270 / 2;

		//　敵の初期化
		std::vector<ML::Box2D> enemy1{
			ML::Box2D(0, 0, 38, 40),
			ML::Box2D(38, 0, 38, 40),
			ML::Box2D(38 * 2, 0, 38, 40),
			ML::Box2D(38 * 3, 0, 38, 40),
			ML::Box2D(38 * 2, 0, 38, 40),
			ML::Box2D(38, 0, 38, 40),
		}; 
		std::vector<ML::Box2D> enemy2{
			ML::Box2D(0, 0, 32, 32),
			ML::Box2D(32, 0, 32, 32),
			ML::Box2D(32 * 2, 0, 32, 32),
			ML::Box2D(32 * 3, 0, 32, 32),
			ML::Box2D(32 * 2, 0, 32, 32),
			ML::Box2D(32, 0, 32, 32),
		};
		std::vector<ML::Box2D> enemy3{
			ML::Box2D(0, 0, 40, 40)
		};

		for (int i = 0; i < 30; ++i) {
			enemy[i].active = true;
			enemy[i].moveCnt = 0;
			enemy[i].hitBase = ML::Box2D(-20, -20, 40, 40);
			switch (i % 3) {
			case 0:
				enemy[i].animation = new Animation("Enemy1Img", enemy1);
				enemy[i].myPattern = new PatternLoader("./data/emeny_1.txt");
				enemy[i].x = i * 38 + ge->screenWidth;
				enemy[i].y = ge->screenHeight / 2 + 40;
				break;

			case 1:
				enemy[i].animation = new Animation("Enemy2Img", enemy2);
				enemy[i].myPattern = new PatternLoader("./data/emeny_2.txt");
				enemy[i].x = i * 32 + ge->screenWidth;
				enemy[i].y = ge->screenHeight / 2 - 40;
				break;

			case 2:
				enemy[i].animation = new Animation("Enemy3Img", enemy3);
				enemy[i].myPattern = new PatternLoader("./data/emeny_3.txt");
				enemy[i].x = i * 40 + ge->screenWidth;
				enemy[i].y = ge->screenHeight / 2;
				break;
			}
		}

		//　弾の初期化（無効科）
		for (auto& s : shots)
			s.active = false;
		for (auto& b : shotBoom)
			b.active = false;
	}

	void Finalize() {
		DG::Image_Erase("PlayerImg");
		DG::Image_Erase("Enemy1Img");
		DG::Image_Erase("Enemy2Img");
		DG::Image_Erase("Enemy3Img");
		DG::Image_Erase("ShotImg");

		//　敵アニメーションを解除
		for (auto& e : enemy) {
			delete e.animation;
			delete e.myPattern;
		}
	}

	TaskFlag UpDate() {
		in1 = DI::GPad_GetState("P1");

		//　壁紙の移動
		for (auto& b : backs) {
			b.x--;
			if (b.x < -827)
				b.x = 827;
		}

		//　プレイヤー移動
		if (player.active) {
			if (in1.LStick.L.on)	player.x -= 3;
			if (in1.LStick.R.on)	player.x += 3;
			if (in1.LStick.U.on)	player.y -= 3;
			if (in1.LStick.D.on)	player.y += 3;

			//　プレイヤーの弾発射
			if (in1.B1.down) 
				for (auto& s : shots) 
					if (!s.active) {
						Shot_Initialize(s, player.x, player.y);
						break;
					}
				
			if (in1.B2.down)
				for (auto& b : shotBoom)
					if (!b.active) {
						Shot_Initialize(b, player.x, player.y);
						break;
					}
		}


		//　プレイヤーの弾の移動
		for (auto& s : shots)
			Shot_UpDate(s, false);
		for (auto& b : shotBoom)
			Shot_UpDate(b, true);



		for (auto& e : enemy) {
			e.animation->Update();
			if (e.active) {
				//　敵キャラの移動
				e.myPattern->Update();
				ML::Vec2 movePattern = e.myPattern->GetPattern();
				e.x += (int)movePattern.x;
				e.y += (int)movePattern.y;
				
				//　画面外に出たら消滅
				if (e.x + 32 < 0)
					e.active = false;
				
				//　プレイヤーと当たり判定
				ML::Box2D playerbox = player.hitBase.OffsetCopy(player.x, player.y);
				ML::Box2D enemybox = e.hitBase.OffsetCopy(e.x, e.y);
				if (enemybox.Hit(playerbox))
					return TaskFlag::Task_End;
			}
		}

		return TaskFlag::Task_Game;
	}

	void Render() {

		// 背景を表示
		for (auto& b : backs)
			DG::Image_Draw("BackImg", ML::Box2D(b.x, 0, 827, 270), ML::Box2D(0, 0, 827, 270));


		Player_Draw(player);
		//　プレイヤーの弾を表示
		for (auto s : shots)
			Shot_Draw(s, false);
		for (auto b : shotBoom)
			Shot_Draw(b, true);

		//　敵キャラの表示処理
		for (auto& e : enemy) {
			if (e.active) {
				ML::Box2D draw(-20, -20, 40, 40);
				draw.Offset(e.x, e.y);
				e.animation->SetPosition(draw.x, draw.y);
				e.animation->Render();
			}
		}
	}



	// ---------------------------------------------------------------



	void Player_Draw(Chara& c_) {
		//　プレイヤーの表示
		if (c_.active) {
			ML::Box2D draw(-32, -12, 64, 25);
			draw.Offset(c_.x, c_.y);
			ML::Box2D src(0, 0, 64, 25);
			DG::Image_Draw("PlayerImg", draw, src);
		}
	}

	void Shot_Initialize(Chara& c_, int x_, int y_) {
		c_.active = true;
		c_.x = x_;
		c_.y = y_;
	}

	//　弾を打つ
	void Shot_Draw(Chara& c_, bool isBoom) {
		if (c_.active) {
			if (isBoom) {
				ML::Box2D draw(-8, -8, 16, 16);
				draw.Offset(c_.x, c_.y);
				ML::Box2D src(0, 32, 16, 16);
				DG::Image_Draw("ShotImg", draw, src);
			}
			else {
				ML::Box2D draw(-16, -16, 32, 32);
				draw.Offset(c_.x, c_.y);
				ML::Box2D src(0, 96, 32, 32);
				DG::Image_Draw("ShotImg", draw, src);
			}
		}
	}

	void Shot_UpDate(Chara& c_, bool isBoom) {

		if (c_.active) {
			if (isBoom) {
				c_.x++;
				c_.y++;
			}
			else
				c_.x += 4;
		}
		if (c_.x > (int)ge->screenWidth)
			c_.active = false;

		//　弾と敵とのあたり判定
		if (c_.active) {
			ML::Box2D me = c_.hitBase.OffsetCopy(c_.x, c_.y);
			for (auto& e : enemy)
				if (e.active) {
					ML::Box2D you = e.hitBase.OffsetCopy(e.x, e.y);
					if (you.Hit(me)) {
						e.active = false;
						c_.active = false;
					}
				}
		}
	}


	void Back_Initialize(Chara& b_, int x_, int y_){
		b_.x = x_;
		b_.y = y_;
	}
}