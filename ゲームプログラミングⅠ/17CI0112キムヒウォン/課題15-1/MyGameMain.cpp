#include "MyGameMain.h"

//ゲーム情報
struct Object {
	bool	active;
	float	x;
	float	y;
	float	angle;
};

Object		obj;

void Object_Initialize(Object& o_, float ang_);
void Object_Draw(Object& o_);
void Object_UpDate(Object& o_);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Image_Create("RotTestImg", "./data/image/RotTest.bmp");
	Object_Initialize(obj, ML::ToRadian(0));
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("RotTestImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	Object_UpDate(obj);
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	Object_Draw(obj);
}









void Object_Initialize(Object& o_, float ang_) {
	o_.active = true;
	o_.angle = ang_;
	o_.x = (ge->screenWidth / 2) + cos(o_.angle) * 100.f;
	o_.y = (ge->screenHeight / 2) + sin(o_.angle) * 100.f;
}

void Object_Draw(Object& o_) {
	if (o_.active) {
		ML::Box2D draw(-16, -16, 32, 32);
		ML::Box2D src(0, 0, 32, 32);
		draw.Offset(o_.x, o_.y);
		DG::Image_Rotation("RotTestImg", o_.angle, ML::Vec2(0, 0));
		DG::Image_Draw("RotTestImg", draw, src);
	}
}

void Object_UpDate(Object& o_) {
	if (o_.active) {
		o_.angle += ML::ToRadian(1);
		o_.x = (ge->screenWidth / 2) + cos(o_.angle) * 100.f;
		o_.y = (ge->screenHeight / 2) + sin(o_.angle) * 100.f;
	}
}