#include "MyGameMain.h"

//ゲーム情報
const int CENTER_X = 120;
const int CENTER_Y = 360;
struct Object {
	bool	active;
	float	x;
	float	y;
	float	angle;
	float	addAngle;
	float	distance;
	float	maxDist;
};

Object		obj[12];

void Object_Initialize(Object& o_, float ang_, float aa_, float distance_);
void Object_Draw(Object& o_);
void Object_UpDate(Object& o_);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Image_Create("RotTestImg", "./data/image/RotTest.bmp");
	
	for (int i = 0; i < 12; ++i) {
		float aa = 0.f;
		float dist = 0.f;
		if (i % 2 == 0) {
			aa = 1.f;
			dist = 1.f;
		}
		else {
			aa = -0.5f;
			dist = 2.f;
		}
		Object_Initialize(obj[i], ML::ToRadian(i * 30.f), aa, dist);
	}
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("RotTestImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	for (auto& o : obj)
		Object_UpDate(o);
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	for (auto& o : obj)
		Object_Draw(o);
}









void Object_Initialize(Object& o_, float ang_, float aa_, float distance_) {
	o_.active = true;
	o_.angle = ang_;
	o_.addAngle = aa_;
	o_.x = CENTER_X + cos(o_.angle) * 5.f;
	o_.y = CENTER_Y + sin(o_.angle) * 5.f;
	o_.distance = 0.f;
	o_.maxDist = distance_;
}

void Object_Draw(Object& o_) {
	if (o_.active) {
		ML::Box2D draw(-16, -16, 32, 32);
		ML::Box2D src(0, 0, 32, 32);
		draw.Offset(o_.x, o_.y);
		DG::Image_Rotation("RotTestImg", o_.angle, ML::Vec2(0, 0));
		DG::Image_Draw("RotTestImg", draw, src);
	}
}

void Object_UpDate(Object& o_) {
	if (o_.active) {
		if (o_.distance > o_.maxDist * 100.f)
			Object_Initialize(o_, o_.angle, o_.addAngle, o_.maxDist);
		o_.distance++;

		int mx = o_.x - CENTER_X;
		int my = o_.y - CENTER_Y;
		o_.angle = atan2(my, mx);
		float rotate = o_.angle + o_.addAngle;

		o_.x += cos(rotate);
		o_.y += sin(rotate);
	}
}