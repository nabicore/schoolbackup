#include "MyGameMain.h"
#include <array>

//ゲーム情報

// 方向の定義
enum Direction { NONE = 0, UP, DOWN, LEFT = 1, RIGHT = 2 };
std::array<int, 3> dir{
	0,
	-1,
	1
};

ML::Vec2 currentDir(dir[Direction::NONE], dir[Direction::DOWN]);
ML::Box2D slimePos(0, 0, 64, 48);
ML::Box2D slimeSrc(0, 0, 64, 48);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TestImg");
	DG::Image_Erase("MapChipImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	int right = ge->screenWidth - slimeSrc.w;
	int bottom = ge->screenHeight - slimeSrc.h;

	// LEFT TOP
	if (slimePos.x <= 0 && slimePos.y <= 0) {
		currentDir = ML::Vec2(dir[Direction::NONE], dir[Direction::DOWN]);
		OutputDebugString("DOWN\n");
	}
	// LEFT BOTTOM
	if (slimePos.x <= 0 && slimePos.y > bottom) {
		currentDir = ML::Vec2(dir[Direction::RIGHT], dir[Direction::NONE]);
		OutputDebugString("RIGHT\n");
	}
	// RIGHT BOTTOM
	if (slimePos.x > right && slimePos.y >= bottom) {
		currentDir = ML::Vec2(dir[Direction::NONE], dir[Direction::UP]);
		OutputDebugString("UP\n");
	}
	// RIGHT TOP
	if (slimePos.x >= right && slimePos.y < 0) {
		currentDir = ML::Vec2(dir[Direction::LEFT], dir[Direction::NONE]);
		OutputDebugString("LEFT\n");
	}	

	slimePos.x += currentDir.x;
	slimePos.y += currentDir.y;

	// Debug
	string dbg = std::to_string(slimePos.x) + ", " + std::to_string(slimePos.y) + "\n";
	OutputDebugString(dbg.c_str());
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	DG::Image_Draw("TestImg", slimePos, slimeSrc);
}
