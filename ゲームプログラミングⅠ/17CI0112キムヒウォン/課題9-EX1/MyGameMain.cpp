#include "MyGameMain.h"

//ゲーム情報
DI::Mouse						mouse;
int								posX, posY;
int								score;

enum State {
	Normal,
	Hit,
	Non
};

struct Target {
	State						state;
	int							x;
	int							y;
	ML::Box2D					hitBase;
	int							timeCnt;
	int							score;
	int							moveX;
	int							moveY;
};
Target							target[20];


//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Font_Create("FontA", "MS ゴシック", 16, 32);
	DG::Image_Create("TargetImg", "./data/image/Target2.png");
	srand((unsigned int)time(NULL));

	posX = 0;
	posY = 0;
	score = 0;
	
	for (auto& t : target) {
		t.state = State::Normal;
		t.timeCnt = rand() % 300;
		t.x = -1000;
		t.y = 0;
		t.moveX = 0;
		t.moveY = 0;
		t.score = 100;
		t.hitBase = ML::Box2D(0, 0, 40, 40);
	}
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Font_Erase("FontA");
	DG::Image_Erase("TargetImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	mouse = DI::Mouse_GetState();
	posX = mouse.cursorPos.x;
	posY = mouse.cursorPos.y;

	for (auto& t : target) {

		// 動かす部分
		if (t.state != State::Non) {
			t.timeCnt += 1;

			if (t.timeCnt >= 300) {
				t.timeCnt = 0;
				t.state = Normal;

				int side = rand() % 2;
				switch (side) {
				case 0:		// 上から下
					t.x = rand() % (ge->screenWidth - 40);
					t.y = -40;
					t.moveX = 0;
					t.moveY = 2;
					break;
				case 1:		// 下から上
					t.x = rand() % (ge->screenWidth - 40);
					t.y = ge->screenHeight + 40;
					t.moveX = 0;
					t.moveY = -2;
					break;
				}
			}
			t.x += t.moveX;
			t.y += t.moveY;
		}

		// あたり判定
		if (t.state != State::Hit) {
			ML::Box2D hit = t.hitBase;
			hit.x += t.x;
			hit.y += t.y;

			float distX = posX - hit.x - 20;
			float distY = posY - hit.y - 20;
			float distance = sqrt((distX * distX) + (distY * distY));

			if (distance <= 20) {
				t.state = State::Hit;
				score += t.score;
			}
		}
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	for (auto& t : target) {
		if (t.state != State::Non) {
			auto draw = t.hitBase;
			auto src = ML::Box2D(0, 0, 10, 10);
			switch (t.state) {
			case State::Normal:
				src = ML::Box2D(0, 0, 40, 40);
				break;
			case State::Hit:
				src = ML::Box2D(80, 0, 40, 40);
				break;
			}
			draw.x += t.x;
			draw.y += t.y;
			DG::Image_Draw("TargetImg", draw, src);
		}
	}

	DG::Font_Draw("FontA", ML::Box2D(0, 0, 600, 60), "得点：" + std::to_string(score), ML::Color(0.8f, 0.5f, 0.f, 1.f));
}
