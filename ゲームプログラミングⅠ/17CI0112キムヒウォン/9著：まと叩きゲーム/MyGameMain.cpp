#include "MyGameMain.h"
#include <cstdlib>
#include <vector>

//ゲーム情報

const ML::Box2D							BUBBLE_CROP(64, 0, 64, 64);
const int								BUBBLE_MAX = 30;
const int								SECOND = 20;
const int								BUBBLE_SPAWN_INTERVAL = 1 * SECOND;
const int								BUBBLE_SCORE = 100;


class Bubble {

	ML::Box2D							pos;
	ML::Box2D							crop;
	bool								_isAlive = false;

public:
	Bubble() {
		pos = ML::Box2D(0, 0, 64, 64);
		crop = BUBBLE_CROP;
	}
	Bubble(ML::Box2D p, ML::Box2D c) : pos(p), crop(c) {}
	virtual ~Bubble() {}

	inline const ML::Box2D Position() { return pos; }
	inline const ML::Box2D Source() { return crop; }

	inline const bool& isAlive() { return _isAlive; }
	inline void Dead() { _isAlive = false; }
	inline void Spawn(const MyPG::MyGameEngine* pGE) { 
		pos.x = rand() % pGE->screenWidth;
		pos.y = rand() % pGE->screenHeight;
		_isAlive = true;
	}

	inline const bool isCollide(const ML::Vec2& t) { 
		return pos.Hit(t);	
	}

};



int										score = 0;
DI::Mouse								mouse;
std::vector<Bubble*>					bubbles;
int										timer = 0;


//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Font_Create("FontA", "MS ゴシック", 16, 32);
	DG::Image_Create("BubbleSource", "./data/image/Target.bmp");
	srand((unsigned int)NULL);

	for (int i = 0; i < BUBBLE_MAX; ++i) {
		Bubble* bubble = new Bubble();
		bubbles.push_back(bubble);
	}
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Font_Erase("FontA");
	DG::Image_Erase("BubbleSource");

	for (int i = 0; i < BUBBLE_MAX; ++i)
		delete bubbles[i];
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	timer += 1;
	mouse = DI::Mouse_GetState();
	ML::Vec2 mousePos((int)mouse.cursorPos.x, (int)mouse.cursorPos.y);

	// 一定時間後、画面上に新しいまとを作る
	if (timer >= BUBBLE_SPAWN_INTERVAL) {

		// まとを作って管理リストに追加
		for (auto bubble : bubbles) 
			if (!bubble->isAlive()) {
				bubble->Spawn(ge);
				break;
			}
		timer = 0;
	}

	// ぶつかる判定
	for (auto bubble : bubbles) {
		if (bubble->isCollide(mousePos) && bubble->isAlive()) {
			score += BUBBLE_SCORE;
			bubble->Dead();
		}
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	for (auto bubble : bubbles)
		if(bubble->isAlive())
			DG::Image_Draw("BubbleSource", bubble->Position(), bubble->Source());

	string scoreBoard = "点数：" + std::to_string(score);
	DG::Font_Draw("FontA",
		ML::Box2D(0, 0, 480, 32),
		scoreBoard,
		ML::Color(0.8f, 0.5f, 0.f, 1.f));
}
