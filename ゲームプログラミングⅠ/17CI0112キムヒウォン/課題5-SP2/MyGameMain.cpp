#include "MyGameMain.h"
#include <array>

//ゲーム情報
int kasoku = 1;
DI::VGamePad gamePad;

ML::Box2D slimePos(0, 0, 64, 48);
ML::Box2D slimeSrc(0, 0, 64, 48);

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize()
{
	DI::KeyDatas_GP gKey = {
		{ DI::GPB::B01, DI::B1 },
		{ DI::GPB::B02, DI::B2 },
		{ DI::GPB::B03, DI::B3 },
		{ DI::GPB::B04, DI::B4 },
	};
	DI::GPad_CreateGP("G1", 0, gKey);

	DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DI::GPad_Erase("G1");

	DG::Image_Erase("TestImg");
	DG::Image_Erase("MapChipImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	gamePad = DI::GPad_GetState("G1");

	if (gamePad.LStick.L.on)
		slimePos.x -= kasoku;
	if (gamePad.LStick.R.on)
		slimePos.x += kasoku;
	if (gamePad.LStick.U.on)
		slimePos.y -= kasoku;
	if (gamePad.LStick.D.on)
		slimePos.y += kasoku;

	if (gamePad.B1.down)
		if (kasoku < 5)
			kasoku += 1;

	if (gamePad.B2.down)
		if (kasoku > 1)
			kasoku -= 1;

	OutputDebugString(std::to_string(kasoku).c_str());
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D()
{
	DG::Image_Draw("TestImg", slimePos, slimeSrc);
}
