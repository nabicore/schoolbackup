#include "MyGameMain.h"

//ゲーム情報
DI::VGamePad			in1;

struct Chara {
	bool				active;
	int					x, y;
	int					moveCnt;
	ML::Box2D			hitBase;
};

Chara player;
Chara enemy[30];
Chara shots[10];

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	//　キーボードの入力を受け取るオブジェクトを生成する
	DI::AnalogAxisKB ls{
		DIK_LEFT, DIK_RIGHT,
		DIK_UP,	  DIK_DOWN
	};
	DI::AnalogAxisKB rs{ 0, };
	DI::KeyDatas_KB key{
		{DIK_Z, DI::B1}, {DIK_X, DI::B2},
		{DIK_C, DI::B3}, {DIK_V, DI::B4}
	};
	DI::GPad_CreateKB("P1", ls, rs, key);

	//　画像の読み込み
	DG::Image_Create("PlayerImg", "./data/image/player.png");
	DG::Image_Create("Enemy1Img", "./data/image/enemy1.png");
	DG::Image_Create("ShotImg", "./data/image/Shot.png");

	//　プレイヤーの初期化
	player.active = true;
	player.x = 50;
	player.y = 270 / 2;

	//　敵の初期化
	for (int i = 0; i < 30; ++i) {
		enemy[i].active = true;
		enemy[i].x = (i % 6) * 40 + 160;
		enemy[i].y = (i / 6) * 40 + 40;
		enemy[i].moveCnt = 0;
		enemy[i].hitBase = ML::Box2D(-20, -20, 40, 40);
	}

	//　弾の初期化（無効科）
	for (auto& s : shots)
		s.active = false;
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("PlayerImg");
	DG::Image_Erase("Enemy1Img");
	DG::Image_Erase("ShotImg");
	DI::GPad_Erase("P1");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
	in1 = DI::GPad_GetState("P1");

	//　プレイヤー移動
	if (player.active) {
		if (in1.LStick.L.on)	player.x -= 3;
		if (in1.LStick.R.on)	player.x += 3;
		if (in1.LStick.U.on)	player.y -= 3;
		if (in1.LStick.D.on)	player.y += 3;

		//　プレイヤーの弾発射
		if (in1.B1.down) {
			for (auto& s : shots) {
				if (!s.active) {
					s.active = true;
					s.x = player.x;
					s.y = player.y;
					break;
				}

				//　弾と敵とのあたり判定
				if (s.active) {
					ML::Box2D me = s.hitBase.OffsetCopy(s.x, s.y);
					for (auto& e : enemy)
						if (e.active) {
							ML::Box2D you = e.hitBase.OffsetCopy(e.x, e.y);
							if (you.Hit(me)) {
								e.active = false;
								s.active = false;
							}
						}
				}
			}
		}
	}


	//　プレイヤーの弾の移動
	for (auto& s : shots) {
		if (s.active)
			s.x += 4;
		if (s.x > ge->screenWidth)
			s.active = false;
	}



	//　敵キャラの移動
	for (auto& e : enemy) {
		if (e.active) {
			if (e.moveCnt < 100)
				e.x += 1;
			else if (e.moveCnt < 200)
				e.x -= 1;
			else
				e.moveCnt = 0;
			e.moveCnt++;
		}

		//　プレイヤーと当たり判定
		ML::Box2D playerbox = player.hitBase.OffsetCopy(player.x, player.y);
		ML::Box2D enemybox = e.hitBase.OffsetCopy(e.x, e.y);
		if (enemybox.Hit(playerbox)) 
			player.active = false;
		
	}
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	//　プレイヤーの表示
	if (player.active) {
		ML::Box2D draw(-32, -12, 64, 25);
		draw.Offset(player.x, player.y);
		ML::Box2D src(0, 0, 64, 25);
		DG::Image_Draw("PlayerImg", draw, src);
	}

	//　プレイヤーの弾を表示
	for (auto s : shots)
		//　弾を打つ
		if (s.active) {
			ML::Box2D draw(-16, -16, 32, 32);
			draw.Offset(s.x, s.y);
			ML::Box2D src(0, 96, 32, 32);
			DG::Image_Draw("ShotImg", draw, src);
		}

	//　敵キャラの表示処理
	for (auto& e : enemy) {
		if (e.active) {
			ML::Box2D draw(-20, -20, 40, 40);
			draw.Offset(e.x, e.y);
			ML::Box2D src(0, 0, 40, 40);
			DG::Image_Draw("Enemy1Img", draw, src);
		}
	}
}
