#include "MyPG.h"
#include "Flats.h"



Flat* Flat::Create() {
	Flat* ret = new Flat();

	ret->mySpriteName = "NormalFlat";
	ret->src = ML::Box2D(0, 0, 70, 70);
	
	return ret;
}

void Flat::Draw() {
	DG::Image_Draw(mySpriteName, this->OffsetCopy(0, 0), src);
}

void Flat::SetPosition(ML::Box2D pos) {
	this->x = pos.x;
	this->y = pos.y;
	this->w = pos.w;
	this->h = pos.h;
}

void Flat::SetPosition(int x, int y) {
	this->x = x;
	this->y = y;
}
