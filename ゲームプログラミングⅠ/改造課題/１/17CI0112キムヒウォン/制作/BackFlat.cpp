#include "MyPG.h"
#include "BackFlat.h"



void BackFlat::Initialize() {
	backScroll[0] = ML::Box2D(0, 0, 180, 320);
	backScroll[1] = ML::Box2D(320, 0, 180, 320);

	for (int i = 0; i < blockCnt; ++i) {
		auto b = Flat::Create();
		int x = rand() % (int)ge->screenWidth + 0;
		int y = rand() % (int)ge->screenHeight + 0;
		b->SetPosition(ML::Box2D(x, y, 50, 50));
		flats.push_back(b);
	}
}

void BackFlat::Update() {
	// �w�i�X�N���[��
	for (auto& b : backScroll) {
		if (b.y > (int)ge->screenHeight)
			b.y = -(int)ge->screenHeight;
		b.y += speed;
	}

	for (auto f : flats) {
		if (f->y > ge->screenHeight)
			RefreshFlat(f);
		f->SetPosition(f->x, f->y + speed);
	}
}

void BackFlat::RefreshFlat(Flat* f) {
	int new_x = rand() % (int)ge->screenWidth + 0;
	int new_y = ge->screenHeight + rand() % (int)ge->screenHeight + 0;
	f->SetPosition(new_x, new_y);
}