#pragma once


class Flat : public ML::Box2D {

	string			mySpriteName;
	ML::Box2D		src;

public:
	explicit Flat() {};
	virtual ~Flat() {};

	static Flat* Create();
	void Draw();

	void SetPosition(ML::Box2D);
	void SetPosition(int x, int y);
};