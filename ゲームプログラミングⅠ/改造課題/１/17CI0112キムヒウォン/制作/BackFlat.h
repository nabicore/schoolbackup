#pragma once
#include "Flats.h"

class BackFlat {

	int				blockCnt = 20;
	int				speed = 0;
	ML::Box2D		backScroll[2];
	vector<Flat*>	flats;

public:
	explicit BackFlat() {};
	virtual ~BackFlat() {};

	void Initialize();
	void Update();

	void RefreshFlat(Flat*);
};