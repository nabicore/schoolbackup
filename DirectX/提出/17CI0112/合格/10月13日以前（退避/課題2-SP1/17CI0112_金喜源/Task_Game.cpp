#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad  in1;
	int			  tick = 0;
	ML::Vec3	  duration[3];

	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		//カメラの設定
		ge->camera[0] = MyPG::Camera::Create(
			ML::Vec3(0.0f, 0.0f, 0.0f),				//	ターゲット位置
			ML::Vec3(400.0f, 1200.0f, -800.0f),			//	カメラ位置
			ML::Vec3(0.0f, 1.0f, 0.0f),					//	カメラの上方向ベクトル
			35.0f * ML::PI / 180.0f, 10.0f, 4000.0f,	//	視野角・視野距離
			(float)ge->screenWidth / (float)ge->screenHeight);		//	画面比率
		DG::EffectState().param.bgColor = ML::Color(1, 1, 1, 1);

		//メッシュの読み込み
		DG::Mesh_CreateFromSOBFile("ArrowMesh", "./data/mesh/arrow.sob");
		DG::Mesh_CreateFromSOBFile("Yuka", "./data/res/box1.sob");
		DG::Mesh_CreateFromSOBFile("Yuka2", "./data/res/box2.sob");
		DG::Mesh_CreateFromSOBFile("Yuka3", "./data/res/box3.sob");
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Mesh_Erase("ArrowMesh");
		DG::Mesh_Erase("Yuka");
		DG::Mesh_Erase("Yuka2");
		DG::Mesh_Erase("Yuka3");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	2D
	//-----------------------------------------------------------------------------
	void Render2D()
	{
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	3D
	//-----------------------------------------------------------------------------
	void Render3D()
	{
		//平行移動行列を生成 //ワールド変換を適用する
		ML::Mat4x4  matT;
		matT.Translation(ML::Vec3(0, 0, 0));
		DG::EffectState().param.matWorld = matT;
		DG::Mesh_Draw("ArrowMesh");

		string yukaName[] = {
			"Yuka",
			"Yuka2",
			"Yuka3"
		};

		for (int i = 0; i < 3; ++i) {
			matT.Translation(duration[i]);
			DG::EffectState().param.matWorld = matT;
			DG::Mesh_Draw(yukaName[i]);
		}

		if (tick < 100) {
			duration[0].x += 10;
			duration[1].y += 6;
			duration[2].z += 3;
		}
		else {
			tick = 0;
			for (auto& d : duration)
				d = ML::Vec3(0.f, 0.f, 0.f);
		}
		tick += 1;
	}
}