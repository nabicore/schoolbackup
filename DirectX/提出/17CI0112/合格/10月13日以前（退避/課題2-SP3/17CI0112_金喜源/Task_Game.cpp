#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad  in1;

	struct Object {
		bool			active;
		ML::Vec3		pos;
		ML::Vec3		centor;
		float			angle;
		float			dist;
	};
	void Object_Initialize(Object& o_, float ang_, float d_, const ML::Vec3& c_);
	void Object_Render(Object& o_);
	void Object_Update(Object& o_);
	
	Object objs[12];

	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		//カメラの設定
		ge->camera[0] = MyPG::Camera::Create(
			ML::Vec3(400.f, 400.f, 0.0f),				//	ターゲット位置
			ML::Vec3(400.f, 400.f , -800.0f),			//	カメラ位置
			ML::Vec3(0.0f, 1.0f, 0.0f),					//	カメラの上方向ベクトル
			35.0f * ML::PI / 180.0f, 10.0f, 4000.0f,	//	視野角・視野距離
			(float)ge->screenWidth / (float)ge->screenHeight);		//	画面比率
		DG::EffectState().param.bgColor = ML::Color(1, 1, 1, 1);

		//メッシュの読み込み
		DG::Mesh_CreateFromSOBFile("ArrowMesh", "./data/mesh/arrow.sob");
		DG::Mesh_CreateFromSOBFile("Yuka", "./data/res/box1.sob");
		DG::Mesh_CreateFromSOBFile("Yuka2", "./data/res/box2.sob");
		DG::Mesh_CreateFromSOBFile("Yuka3", "./data/res/box3.sob");

		for (int i = 0; i < 12; ++i)
			Object_Initialize(objs[i], ML::ToRadian(30.f * i), 400.f, ML::Vec3(400.f, 400.f, 300.f));
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Mesh_Erase("ArrowMesh");
		DG::Mesh_Erase("Yuka");
		DG::Mesh_Erase("Yuka2");
		DG::Mesh_Erase("Yuka3");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		for (auto& o : objs)
			Object_Update(o);

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	2D
	//-----------------------------------------------------------------------------
	void Render2D()
	{
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	3D
	//-----------------------------------------------------------------------------
	void Render3D()
	{
		//平行移動行列を生成 //ワールド変換を適用する
		ML::Mat4x4  matT;
		matT.Translation(ML::Vec3(0, 0, 0));
		DG::EffectState().param.matWorld = matT;
		DG::Mesh_Draw("ArrowMesh");

		for (auto& o : objs)
			Object_Render(o);
	}



	void Object_Initialize(Object & o_, float ang_, float d_, const ML::Vec3 & c_)
	{
		o_.active = true;
		o_.angle = ang_;
		o_.dist = d_;
		o_.centor = c_;
	}
	void Object_Render(Object& o_)
	{
		if (o_.active) {
			ML::Mat4x4  matT;
			matT.Translation(o_.pos);
			DG::EffectState().param.matWorld = matT;
			DG::Mesh_Draw("Yuka");
		}
	}
	void Object_Update(Object& o_)
	{
		o_.angle -= 1.f;
		o_.pos = ML::Vec3(
			o_.centor.x + cos(o_.angle) * o_.dist, 
			o_.centor.y + sin(o_.angle) * o_.dist, 
			o_.centor.z);
	}
}