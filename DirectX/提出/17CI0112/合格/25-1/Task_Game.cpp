#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	struct Map3D {
		int			arr[30][30][30];
		int			sizeX, sizeY, sizeZ;
		float		chipSize;
		ML::Box3D	hitBase;
		string		chipName[10];
	};
	void Map_Initialize(Map3D& m_);
	bool Map_Load(Map3D& m_, string f_);
	void Map_Render(Map3D& m_);
	void Map_Finalize(Map3D& m_);
	bool Map_CheckHit(Map3D& m_, const ML::Box3D& hit_);

	struct Chara {
		string		meshName;
		ML::Vec3	pos;
		ML::Vec3	angle;
		ML::Box3D	hitBase;
		float		dist;
		float		ajast_CM;
		float		ajast_CM_min;
		float		ajast_CM_std;
		float		ajast_CM_max;
		float		ajast_TG;
		int			ajast_CM_tick;
		float		fov;
		float		fov_min;
		float		fov_std;
		float		fov_max;
		float		fallSpeed;
		float		jumpPow;
	};
	void Chara_Initialize(Chara& c_);
	void Chara_CheckMove(Chara& c_, ML::Vec3& est_);
	bool Chara_CheckFoot(Chara& c_);
	void Player_Initialize(Chara& c_, ML::Vec3& p_);
	void Player_Render(Chara& c_);
	void Player_Update(Chara& c_);
	void CameraMan_Initialize(Chara& c_);
	void CameraMan_Update(Chara& c_);
	void Sky_Initialize(Chara& c_, ML::Vec3& p_);
	void Sky_Update(Chara& c_);
	void Sky_Render(Chara& c_);
	void Cloud_Initialize(Chara& c_, ML::Vec3& p_);
	void Cloud_Update(Chara& c_);
	void Cloud_Render(Chara& c_);

	struct PolygonTest1 {
		bool						active;
		DG_::InputLayout::SP		il;
		string						tecName;
		DG_::VertexBuffer::SP		vb_0;
		D3D10_PRIMITIVE_TOPOLOGY	priTopo;
		ML::Vec3					pos;
		ML::Vec3					angle;
		ML::Vec3					scale;
	};
	void Polygon1_Initialize(PolygonTest1& c_);
	void Polygon1_UpDate(PolygonTest1& c_);
	void Polygon1_Render(PolygonTest1& c_);



	inline float Lerp(float x1, float x2, float tick) {
		return x1 + (x2 - x1) * tick;
	}


	//ゲーム情報
	DI::VGamePad  in1;
	Map3D		  mapData;
	Chara		  player;
	Chara		  cameraMan;
	Chara		  sky;
	Chara		  cloud;
	PolygonTest1  pt1;

	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		//カメラの設定
		ge->camera[0] = MyPG::Camera::Create(
			ML::Vec3(500.0f, 0.0f, 400.0f),				//	ターゲット位置
			ML::Vec3(400.0f, 1200.0f, -800.0f),			//	カメラ位置
			ML::Vec3(0.0f, 1.0f, 0.0f),					//	カメラの上方向ベクトル
			35.0f * ML::PI / 180.0f, 10.0f, 80000.f,	//	視野角・視野距離
			(float)ge->screenWidth / (float)ge->screenHeight);		//	画面比率
		DG::EffectState().param.bgColor = ML::Color(1, 0, 0, 0.55);
		DG::EffectState().param.lightsEnable = true;
		DG::EffectState().param.lightAmbient = ML::Color(1.f, 0.3f, 0.3f, 0.3f);

		auto& light = DG::EffectState().param.light[0];
		light.enable = true;
		light.kind = DG_::Light::Directional;
		light.direction = ML::Vec3(1.f, -1.f, 2.f).Normalize();
		light.color = ML::Color(1.f, 0.8f, 0.8f, 0.8f);

		DG::Font_Create("FontA", "MS ゴシック", 10, 20);
		DG::Mesh_CreateFromSOBFile("ArrowMesh", "./data/mesh/arrow.sob");
		DG::Mesh_CreateFromSOBFile("PlayerMesh", "./data/res/char_Stand.sob");
		DG::Mesh_CreateFromSOBFile("SkyMesh", "./data/res/sky_data_blue_xfile.sob");
		DG::Mesh_CreateFromSOBFile("CloudMesh", "./data/res/sky_data_cloud_xfile.sob");
		
		Polygon1_Initialize(pt1);
		Player_Initialize(player, ML::Vec3(150.f, 100.f, 150.f));
		CameraMan_Initialize(cameraMan);
		Map_Initialize(mapData);
		Sky_Initialize(sky, ML::Vec3(0, 0, 0));
		Cloud_Initialize(cloud, ML::Vec3(0, 0, 0));
		Map_Load(mapData, "./data/res/Map00.txt");
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Mesh_Erase("ArrowMesh");
		DG::Mesh_Erase("PlayerMesh");
		DG::Mesh_Erase("SkyMesh");
		DG::Mesh_Erase("CloudMesh");
		Map_Finalize(mapData);
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		Polygon1_UpDate(pt1);
		CameraMan_Update(cameraMan);
		Sky_Update(sky);
		Cloud_Update(cloud);
		Player_Update(player);

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	2D
	//-----------------------------------------------------------------------------
	void Render2D()
	{
		auto p = &player;
		auto c = &cameraMan;

		char buf[1024];
		sprintf_s(buf,
			"プレイヤ　  座標(%6.2f, %6.2f, %6.2f) 向き：%6.2f \n"
			"カメラマン　座標(%6.2f, %6.2f, %6.2f) 向き：%6.2f \n 視点高さ+%6.2f 注視点高さ+%6.2f\n"
			"視野角：%6.2f",
			p->pos.x, p->pos.y, p->pos.z, ML::ToDegree(p->angle.y),
			c->pos.x, c->pos.y, c->pos.z, ML::ToDegree(c->angle.y), c->ajast_CM, c->ajast_TG,
			ML::ToDegree(c->fov)
		);
		ML::Box2D draw(300, 0, 600, 200);
		DG::Font_Draw("FontA", draw, buf, ML::Color(1, 1, 0, 1));

	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	3D
	//-----------------------------------------------------------------------------
	void Render3D()
	{
		//平行移動行列を生成 //ワールド変換を適用する
		ML::Mat4x4  matT;
		matT.Translation(ML::Vec3(0, 0, 0));
		DG::EffectState().param.matWorld = matT;
		DG::Mesh_Draw("ArrowMesh");

		Sky_Render(sky);
		Cloud_Render(cloud);
		Map_Render(mapData);
		Polygon1_Render(pt1);
		Player_Render(player);
	}



	void Map_Initialize(Map3D & m_)
	{
		ZeroMemory(m_.arr, sizeof(m_.arr));
		m_.sizeX = 0;
		m_.sizeY = 0;
		m_.sizeZ = 0;
		m_.hitBase = ML::Box3D(0, 0, 0, 0, 0, 0);
		m_.chipSize = 100.f;
		for (int i = 0; i < _countof(m_.chipName); ++i) {
			m_.chipName[i] = "";
		}
	}
	bool Map_Load(Map3D & m_, string f_)
	{
		ifstream fin(f_);
		if (!fin)
			return false;
		int chipNum;
		fin >> chipNum;

		for (int c = 1; c <= chipNum; ++c) {
			string chipFileName, chipFilePath;
			fin >> chipFileName;
			chipFilePath = "./data/res/" + chipFileName;
			m_.chipName[c] = "MapChip" + std::to_string(c);
			DG::Mesh_CreateFromSOBFile(m_.chipName[c], chipFilePath);
		}

		fin >> m_.sizeX >> m_.sizeY >> m_.sizeZ;
		m_.hitBase = ML::Box3D(0, 0, 0,
			m_.sizeX * (int)m_.chipSize,
			m_.sizeY * (int)m_.chipSize,
			m_.sizeZ * (int)m_.chipSize);
		
		for (int y = 0; y < m_.sizeZ - 1; ++y)
			for (int z = m_.sizeZ - 1; z >= 0; --z)
				for (int x = 0; x < m_.sizeX; ++x)
					fin >> m_.arr[z][y][x];

		fin.close();
		return true;
	}
	void Map_Render(Map3D & m_)
	{
		ML::Mat4x4 matS;
		matS.Scaling(m_.chipSize / 100.f);
		for (int y = 0; y < m_.sizeY; ++y)
			for (int z = 0; z < m_.sizeZ; ++z)
				for (int x = 0; x < m_.sizeX; ++x) {
					
					int cn = m_.arr[z][y][x];
					if (cn == 0) 
						continue;

					ML::Mat4x4 matT;
					matT.Translation(
						ML::Vec3(
							x * m_.chipSize + m_.chipSize / 2,
							y * m_.chipSize + m_.chipSize / 2,
							z * m_.chipSize + m_.chipSize / 2));
					ML::Mat4x4 matW;
					matW = matS * matT;
					DG::EffectState().param.matWorld = matW;
					DG::Mesh_Draw(m_.chipName[cn]);
				}
	}
	void Map_Finalize(Map3D & m_)
	{
		for (int i = 0; i < _countof(m_.chipName); ++i) {
			if (m_.chipName[i] != "")
				DG::Mesh_Erase(m_.chipName[i]);
		}
		m_.sizeX = 0;
		m_.sizeY = 0;
		m_.sizeZ = 0;
	}
	bool Map_CheckHit(Map3D & m_, const ML::Box3D & hit_)
	{
		struct Box3D_2Point {
			int fx, fy, fz;
			int bx, by, bz;
		};

		Box3D_2Point r = {
			hit_.x,				hit_.y,				hit_.z,
			hit_.x + hit_.w,	hit_.y + hit_.h,	hit_.z + hit_.d
		};

		Box3D_2Point m = {
			m_.hitBase.x,		m_.hitBase.y,		m_.hitBase.z,
			m_.hitBase.x + m_.hitBase.w,
			m_.hitBase.y + m_.hitBase.h,
			m_.hitBase.z + m_.hitBase.d
		};

		if (r.fx < m.fx) { r.fx = m.fx;}
		if (r.fy < m.fy) { r.fy = m.fy; }
		if (r.fz < m.fz) { r.fz = m.fz; }
		if (r.bx > m.bx) { r.bx = m.bx; }
		if (r.by > m.by) { r.by = m.by; }
		if (r.bz > m.bz) { r.bz = m.bz; }

		if (r.bx <= r.fx || r.by <= r.fy || r.bz <= r.fz)
			return false;

		int sx = r.fx / (int)m_.chipSize,
			sy = r.fy / (int)m_.chipSize,
			sz = r.fz / (int)m_.chipSize,
			ex = (r.bx - 1) / (int)m_.chipSize,
			ey = (r.by - 1) / (int)m_.chipSize,
			ez = (r.bz - 1) / (int)m_.chipSize;

		for (int z = sz; z <= ez; ++z)
			for (int y = sy; y <= ey; ++y)
				for (int x = sx; x <= ex; ++x)
					if (0 < m_.arr[z][y][x])
						return true;


		return false;
	}
	void Chara_Initialize(Chara & c_)
	{
		c_.meshName = "";
		c_.pos = ML::Vec3(0.f, 0.f, 0.f);
		c_.angle = ML::Vec3(0.f, 0.f, 0.f);
		c_.hitBase = ML::Box3D(0, 0, 0, 0, 0, 0);
		c_.fallSpeed = 0.f;
		c_.jumpPow = 0.f;
	}
	void Chara_CheckMove(Chara & c_, ML::Vec3 & est_)
	{
		float temp, temp2;
		while (est_.x != 0.f) {
			float preX = c_.pos.x;

			if (est_.x >= 1.f) {
				c_.pos.x += 1.f;
				est_.x -= 1.f;
			}
			else if (est_.x <= -1.f) {
				c_.pos.x -= 1.f;
				est_.x += 1.f;
			}
			else {
				c_.pos.x += est_.x;
				est_.x = 0.f;
			}

			ML::Box3D hit = c_.hitBase;
			hit.Offset((int)c_.pos.x, (int)c_.pos.y, (int)c_.pos.z);
			if (Map_CheckHit(mapData, hit)) {
				c_.pos.x = preX;
				break;
			}
		}

		while (est_.z != 0.f) {
			float preZ = c_.pos.z;

			if (est_.z >= 1.f) {
				c_.pos.z += 1.f;
				est_.z -= 1.f;
			}
			else if (est_.z <= -1.f) {
				c_.pos.z -= 1.f;
				est_.z += 1.f;
			}
			else {
				c_.pos.z += est_.z;
				est_.z = 0.f;
			}

			ML::Box3D hit = c_.hitBase;
			hit.Offset((int)c_.pos.x, (int)c_.pos.y, (int)c_.pos.z);
			if (Map_CheckHit(mapData, hit)) {
				c_.pos.z = preZ;
				break;
			}
		}

		while (est_.y != 0.f) {
			float preY = c_.pos.y;

			if (est_.y >= 1.f) {
				c_.pos.y += 1.f;
				est_.y -= 1.f;
			}
			else if (est_.y <= -1.f) {
				c_.pos.y -= 1.f;
				est_.y += 1.f;
			}
			else {
				c_.pos.y += est_.y;
				est_.y = 0.f;
			}

			ML::Box3D hit = c_.hitBase;
			hit.Offset((int)c_.pos.x, (int)c_.pos.y, (int)c_.pos.z);
			if (Map_CheckHit(mapData, hit)) {
				c_.pos.y = preY;
				break;
			}
		}
	}
	bool Chara_CheckFoot(Chara & c_)
	{
		ML::Box3D hit(
			c_.hitBase.x,
			c_.hitBase.y - 1,
			c_.hitBase.z,
			c_.hitBase.w,
			1,
			c_.hitBase.d
		);

		hit.Offset(c_.pos);
		return Map_CheckHit(mapData, hit);
	}
	void Player_Initialize(Chara & c_, ML::Vec3 & p_)
	{
		Chara_Initialize(c_);
		c_.pos = p_;
		c_.meshName = "PlayerMesh";
		c_.hitBase = ML::Box3D(-40, 0, -40, 80, 160, 80);
		c_.jumpPow = 20.f;
	}
	void Player_Render(Chara & c_)
	{
		ML::Mat4x4 matS;
		matS.Scaling(ML::Vec3(1.f, 1.f, 1.f));

		ML::Mat4x4 matT;
		matT.Translation(c_.pos);

		ML::Mat4x4 matR;
		matR.RotationY(c_.angle.y);

		ML::Mat4x4 matW = matS * matR * matT;
		DG::EffectState().param.matWorld = matW;
		DG::Mesh_Draw(c_.meshName);
	}
	void Player_Update(Chara & c_)
	{
		ML::Vec3 est(0, 0, 0);

		if (in1.LStick.volume > 0) {
			c_.angle.y = in1.LStick.angle + ML::ToRadian(90);
			c_.angle.y += cameraMan.angle.y;
			ML::Mat4x4 matR;
			matR.RotationY(c_.angle.y);
			est.x = 10 * in1.LStick.volume;
			est = matR.TransformCoord(est);
		}

		if (Chara_CheckFoot(c_) && in1.B2.down)
			c_.fallSpeed = c_.jumpPow;
		else
			c_.fallSpeed -= ML::Gravity(100) * 4;

		est.y = c_.fallSpeed;
		Chara_CheckMove(c_, est);
	}
	void CameraMan_Initialize(Chara & c_)
	{
		Chara_Initialize(c_);
		c_.dist = 700.f;
		c_.ajast_TG = 150;
		c_.ajast_CM_min = 50;
		c_.ajast_CM_std = 250;
		c_.ajast_CM_max = 450;
		c_.ajast_CM = c_.ajast_CM_std;
		c_.fov_min = ML::ToRadian(5);
		c_.fov_std = ML::ToRadian(35);
		c_.fov_max = ML::ToRadian(65);
		c_.fov = c_.fov_std;
	}
	void CameraMan_Update(Chara & c_)
	{
		if (in1.R3.on) {
			c_.fov = c_.fov_std;
			c_.angle = player.angle;
		}
		c_.angle.y += in1.RStick.axis.x * ML::ToRadian(2);

		ML::Vec3 vec(-c_.dist, 0.f, 0.f);
		ML::Mat4x4 matR;
		matR.RotationY(c_.angle.y);
		vec = matR.TransformCoord(vec);
		c_.pos = player.pos + vec;

		// 上下動
		if (in1.RStick.volume > 0) {
			if (in1.RStick.U.on) {
				if (c_.ajast_CM > c_.ajast_CM_max)
					c_.ajast_CM = c_.ajast_CM_max;
				c_.ajast_CM += in1.RStick.volume * 5.f;
			}
			if (in1.RStick.D.on) {
				if (c_.ajast_CM < c_.ajast_CM_min)
					c_.ajast_CM = c_.ajast_CM_min;
				c_.ajast_CM -= in1.RStick.volume * 5.f;
			}
		}
		else {
			if (c_.ajast_CM_tick > 30) 
				c_.ajast_CM = Lerp(c_.ajast_CM, c_.ajast_CM_std, (float)c_.ajast_CM_tick * 0.01f);
			else
				c_.ajast_CM_tick += 1;
		}
		

		// 視野角
		if (in1.L1.down) {
			if (c_.fov <= c_.fov_min)
				c_.fov = c_.fov_min;
			else
				c_.fov -= ML::ToRadian(2.f);
		}
		if (in1.R1.down) {
			if (c_.fov >= c_.fov_max)
				c_.fov = c_.fov_max;
			else
				c_.fov += ML::ToRadian(2.f);
		}

		ge->camera[0]->target = player.pos + ML::Vec3(0.f, c_.ajast_TG, 0.f);
		ge->camera[0]->pos = c_.pos + ML::Vec3(0.f, c_.ajast_CM, 0.f);
		ge->camera[0]->fov = c_.fov;
	}
	void Sky_Initialize(Chara & c_, ML::Vec3 & p_)
	{
		Chara_Initialize(c_);
		c_.meshName = "SkyMesh";
	}
	void Sky_Update(Chara & c_)
	{
		c_.angle.y += ML::ToRadian(0.01f);
		c_.pos.x = cameraMan.pos.x;
		c_.pos.z = cameraMan.pos.z;
	}
	void Sky_Render(Chara & c_)
	{
		ML::Mat4x4 matT, matR;
		matT.Translation(c_.pos);
		matR.RotationY(c_.angle.y);

		ge->camera[0]->forePlane = 80000.f;
		ge->camera[0]->UpDate();
		DG::EffectState().DSS_Def3D_Dis();

		DG::EffectState().param.matWorld = matR * matT;
		DG::EffectState().param.objectColor = ML::Color(1.f, 0.f, 0.f, 0.55f);
		DG::Mesh_Draw(c_.meshName);
		DG::EffectState().param.objectColor = ML::Color(1.f, 1.f, 1.f, 1.f);
	}
	void Cloud_Initialize(Chara & c_, ML::Vec3 & p_)
	{
		Chara_Initialize(c_);
		c_.meshName = "CloudMesh";
	}
	void Cloud_Update(Chara & c_)
	{
		c_.angle.y += ML::ToRadian(0.01f);
		c_.pos.x = cameraMan.pos.x;
		c_.pos.z = cameraMan.pos.z;
	}
	void Cloud_Render(Chara & c_)
	{
		ML::Mat4x4 matT, matR;
		matT.Translation(c_.pos);
		matR.RotationY(c_.angle.y);

		DG::EffectState().param.matWorld = matR * matT;
		auto bsBup = DG::EffectState().BS_Get();
		DG::EffectState().BS_Alpha();
		DG::EffectState().param.objectColor = ML::Color(1.f, 0.8f, 0.f, 0.f);
		DG::Mesh_Draw(c_.meshName);
		DG::EffectState().param.objectColor = ML::Color(1.f, 1.f, 1.f, 1.f);
		DG::EffectState().BS_Set(bsBup);

		DG::EffectState().DSS_Def3D();
		ge->camera[0]->forePlane = 4000.f;
		ge->camera[0]->UpDate();
	}


	void Polygon1_Initialize(PolygonTest1 & c_)
	{
		c_.active = false;
		D3D10_INPUT_ELEMENT_DESC layout[]{ {
				"POSITION", 
				0, 
				DXGI_FORMAT_R32G32B32_FLOAT, 
				0, 0, 
				D3D10_INPUT_PER_VERTEX_DATA, 
				0
			} };
		c_.tecName = "Test1";
		c_.il = DG_::InputLayout::Create(c_.tecName, layout, _countof(layout));
		if (!c_.il)
			return;

		ML::Vec3 pos[]{
			ML::Vec3(-50, 0, +50),
			ML::Vec3(+50, 0, +50),
			ML::Vec3(0, 150, 0),

			ML::Vec3(-50, 0, -50),
			ML::Vec3(-50, 0, +50),
			ML::Vec3(0, 150, 0),

			ML::Vec3(+50, 0, +50),
			ML::Vec3(+50, 0, -50),
			ML::Vec3(0, 150, 0),

			ML::Vec3(+50, 0, -50),
			ML::Vec3(-50, 0, -50),
			ML::Vec3(0, 150, 0),
		};
		c_.vb_0 = DG_::VertexBuffer::Create((BYTE*)pos, sizeof(ML::Vec3), _countof(pos));
		c_.priTopo = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		c_.active = true;

		c_.pos = ML::Vec3(300, 100, 300);
		c_.angle = ML::Vec3(0, 0, 0);
		c_.scale = ML::Vec3(1, 1, 1);
	}
	void Polygon1_UpDate(PolygonTest1 & c_)
	{
		if (!c_.active) return;
		c_.pos.z += 1;
		c_.angle.y += ML::ToRadian(2);
		c_.scale.x += c_.scale.x / 100.f;
	}
	void Polygon1_Render(PolygonTest1 & c_)
	{
		if (!c_.active) return;
		
		ML::Mat4x4 matT;
		matT.Translation(c_.pos);

		ML::Mat4x4 matR;
		matR.RotationY(c_.angle.y);

		ML::Mat4x4 matW = matR * matT;
		DG::EffectState().param.matWorld = matW;
		DG::EffectState().Update_Params();

		DG::EffectTec().GetPassByName(c_.tecName.c_str())->Apply(0);
		c_.il->IASet();
		DG::Device().IASetPrimitiveTopology(c_.priTopo);

		UINT ofs = 0;
		DG::Device().IASetVertexBuffers(
			0,
			1,
			&c_.vb_0->buf,
			&c_.vb_0->stride,
			&ofs
		);
		DG::Device().Draw(c_.vb_0->num, 0);
	}
}