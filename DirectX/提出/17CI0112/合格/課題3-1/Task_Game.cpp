#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	struct Map3D {
		int			arr[30][30][30];
		int			sizeX, sizeY, sizeZ;
		float		chipSize;
		ML::Box3D	hitBase;
		string		chipName[10];
	};
	void Map_Initialize(Map3D& m_);
	bool Map_Load(Map3D& m_, string f_);
	void Map_Render(Map3D& m_);
	void Map_Finalize(Map3D& m_);


	//ゲーム情報
	DI::VGamePad  in1;
	Map3D		  mapData;

	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		//カメラの設定
		ge->camera[0] = MyPG::Camera::Create(
			ML::Vec3(500.0f, 0.0f, 400.0f),				//	ターゲット位置
			ML::Vec3(400.0f, 1200.0f, -800.0f),			//	カメラ位置
			ML::Vec3(0.0f, 1.0f, 0.0f),					//	カメラの上方向ベクトル
			35.0f * ML::PI / 180.0f, 10.0f, 4000.0f,	//	視野角・視野距離
			(float)ge->screenWidth / (float)ge->screenHeight);		//	画面比率
		DG::EffectState().param.bgColor = ML::Color(1, 1, 1, 1);

		//メッシュの読み込み
		DG::Mesh_CreateFromSOBFile("ArrowMesh", "./data/mesh/arrow.sob");

		Map_Initialize(mapData);
		Map_Load(mapData, "./data/res/Map00.txt");
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Mesh_Erase("ArrowMesh");
		Map_Finalize(mapData);
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	2D
	//-----------------------------------------------------------------------------
	void Render2D()
	{
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理	3D
	//-----------------------------------------------------------------------------
	void Render3D()
	{
		//平行移動行列を生成 //ワールド変換を適用する
		ML::Mat4x4  matT;
		matT.Translation(ML::Vec3(0, 0, 0));
		DG::EffectState().param.matWorld = matT;
		DG::Mesh_Draw("ArrowMesh");

		Map_Render(mapData);
	}



	void Map_Initialize(Map3D & m_)
	{
		ZeroMemory(m_.arr, sizeof(m_.arr));
		m_.sizeX = 0;
		m_.sizeY = 0;
		m_.sizeZ = 0;
		m_.hitBase = ML::Box3D(0, 0, 0, 0, 0, 0);
		m_.chipSize = 100.f;
		for (int i = 0; i < _countof(m_.chipName); ++i) {
			m_.chipName[i] = "";
		}
	}
	bool Map_Load(Map3D & m_, string f_)
	{
		ifstream fin(f_);
		if (!fin)
			return false;
		int chipNum;
		fin >> chipNum;

		for (int c = 1; c <= chipNum; ++c) {
			string chipFileName, chipFilePath;
			fin >> chipFileName;
			chipFilePath = "./data/res/" + chipFileName;
			m_.chipName[c] = "MapChip" + std::to_string(c);
			DG::Mesh_CreateFromSOBFile(m_.chipName[c], chipFilePath);
		}

		fin >> m_.sizeX >> m_.sizeY >> m_.sizeZ;
		m_.hitBase = ML::Box3D(0, 0, 0,
			m_.sizeX * (int)m_.chipSize,
			m_.sizeY * (int)m_.chipSize,
			m_.sizeZ * (int)m_.chipSize);
		
		for (int y = 0; y < m_.sizeZ - 1; ++y)
			for (int z = m_.sizeZ - 1; z >= 0; --z)
				for (int x = 0; x < m_.sizeX; ++x)
					fin >> m_.arr[z][y][x];

		fin.close();
		return true;
	}
	void Map_Render(Map3D & m_)
	{
		ML::Mat4x4 matS;
		matS.Scaling(m_.chipSize / 100.f);
		for (int y = 0; y < m_.sizeY; ++y)
			for (int z = 0; z < m_.sizeZ; ++z)
				for (int x = 0; x < m_.sizeX; ++x) {
					
					int cn = m_.arr[z][y][x];
					if (cn == 0) 
						continue;

					ML::Mat4x4 matT;
					matT.Translation(
						ML::Vec3(
							x * m_.chipSize + m_.chipSize / 2,
							y * m_.chipSize + m_.chipSize / 2,
							z * m_.chipSize + m_.chipSize / 2));
					ML::Mat4x4 matW;
					matW = matS * matT;
					DG::EffectState().param.matWorld = matW;
					DG::Mesh_Draw(m_.chipName[cn]);
				}
	}
	void Map_Finalize(Map3D & m_)
	{
		for (int i = 0; i < _countof(m_.chipName); ++i) {
			if (m_.chipName[i] != "")
				DG::Mesh_Erase(m_.chipName[i]);
		}
		m_.sizeX = 0;
		m_.sizeY = 0;
		m_.sizeZ = 0;
	}
}