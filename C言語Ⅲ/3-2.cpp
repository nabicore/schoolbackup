#include <iostream>
#include <string>
#include <vector>
using namespace std;

 
class Vec2 {
	int x_, y_;
	
public:
	Vec2() : x_(0), y_(0) {}
	Vec2(const int& x) : x_(x), y_(0) {}
	Vec2(const int& x, const int& y) : x_(x), y_(y) {}

	void output() const {
		std::cout << x_ + y_ << std::endl;
	}
};


int main() {
	vector<Vec2> pos;
	pos.push_back(Vec2(1, 2));
	pos.push_back(Vec2(7));
	pos.push_back(Vec2());
	for (const Vec2& p : pos)
		p.output();
	return 0;
}