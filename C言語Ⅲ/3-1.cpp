#include <iostream>
#include <string>
using namespace std;

 
class Vec2 {
	int x_, y_;
	
public:
	void setX(const int& x) {
		x_ = x;
	}

	int getX() const {
		return x_;
	}

	void setY(const int& y) {
		y_ = y;
	}

	int getY() const {
		return y_;
	}

};


int main() {
	return 0;
}