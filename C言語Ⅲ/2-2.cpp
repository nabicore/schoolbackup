#include <iostream>
#include <string>
#include <array>
using namespace std;


class Month {
public:
	Month();
	void input();
	string getMonthName() const;
	int getMonth() const;

private:
	int m_;
	const array<string, 13> MONTHS {
		"",
		"January",
		"Febuary",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October", 
		"November",
		"December"
	};
};

int main() {
	Month m;
	m.input();
	cout << m.getMonth() << "月は" << m.getMonthName() << endl;
	return 0;
}





// ============================================================
Month::Month() : m_(0) { }

void Month::input() {
	do {
		cout << "月を入力してください(1から12まで)：";
		cin >> m_;
	} while (m_ < 1 || m_ > 12);
}

int Month::getMonth() const {
	return m_;
}

string Month::getMonthName() const {
	return MONTHS[m_];
}