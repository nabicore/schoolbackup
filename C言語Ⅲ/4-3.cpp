#include <iostream>
#include <vector>
using namespace std;



#include <Windows.h>


class CounterBase {
protected:
	int now_, min_, max_;

public:
	CounterBase(int init, int min, int max)
		: now_(init)
		, min_(min)
		, max_(max) {}

	virtual void forward() = 0;
	virtual int now() const = 0;
};

class CounterA : public CounterBase {
public:
	CounterA(int init, int min, int max)
		: CounterBase(init, min, max) {}

	virtual void forward() override {
		now_ += 1;
		if (now_ > max_) {
			now_ = min_;
		}
	}

	virtual int now() const override {
		return now_;
	}
};

class CounterB : public CounterBase {
public:
	CounterB(int init, int min, int max)
		: CounterBase(init, min, max) {}

	virtual void forward() override {
		now_ += increase;
		if (now_ <= min_) {
			now_ = min_;
			increase = 1;
		}
		if (now_ >= max_) {
			now_ = max_;
			increase = -1;
		}
	}

	virtual int now() const override {
		return now_;
	}

private:
	int increase = 1;
};

int main() {
	CounterBase* c = new CounterB(3, -2, 4);
	while (!(GetAsyncKeyState(VK_ESCAPE) & 0x8000)) {
		cout << c->now() << " ";
		c->forward();
		Sleep(100);
	}
	delete c;
}