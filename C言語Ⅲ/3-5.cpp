#include <iostream>
#include <ctime>
#include <string>
#include <vector>
using namespace std;


class Kazuate {
	int seikai_;
	int yosou_;

public:
	Kazuate() {
		srand((unsigned int)time(NULL));
		seikai_ = rand() % 10;
	}

	void input() {
		std::cout << "0-9までの数を入力して下さい" << std::endl;
		std::cin >> yosou_;
	}

	bool judge() {
		return seikai_ == yosou_;
	}
	
	bool result() {
		if (judge()) {
			std::cout << "正解！";
			return true;
		}
		else {
			std::cout << "不正解！正解は" << seikai_ << "でした";
			return false;
		}
	}
};

int main() {
	Kazuate k;
	k.input();
	k.result();
}