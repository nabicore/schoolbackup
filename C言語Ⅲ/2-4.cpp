#include <iostream>
using namespace std;



class Vec2 {
public:
	Vec2();
	~Vec2();

	float x() const;

private:
	float x_;
	float y_;
};

int main() {
	Vec2 pos;
	cout << pos.x();
}


// ====================================
Vec2::Vec2() : x_(0.f), y_(0.f) { }
Vec2::~Vec2() { }

float Vec2::x() const {
	return x_;
}