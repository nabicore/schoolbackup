#include <iostream>
#include <string>
#include <vector>
using namespace std;

 
class Object {
	static int uid_;
	int id_;

public:
	Object() {
		uid_++;
		id_ = uid_;
	}
	~Object() {
		uid_--;
	}
	int id() const {
		return id_;
	}

	static int uid() {
		return uid_;
	}
};
int Object::uid_ = 0;

int main() {
	Object a, b, c;
	cout << "生成したオブジェクトの数：" << Object::uid() << endl;
	cout << a.id() << " " << b.id() << " " << c.id() << endl;
}