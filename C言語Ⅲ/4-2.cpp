#include <iostream>
#include <vector>
using namespace std;

class Shape {
public:
	virtual void outputArea() = 0;
	virtual ~Shape() {
		std::cout << "destractor" << " ";
	}
};

class Rectangle : public Shape {
public:
	Rectangle(float w, float h) : height(h), width(w) {}
	virtual void outputArea() override {
		cout << width * height << " ";
	}

private:
	float width = 0.f;
	float height = 0.f;
};

class Triangle : public Shape {
public:
	Triangle(float w, float h) : height(h), width(w) {}
	virtual void outputArea() override {
		cout << (width * height) * 0.5f << " ";
	}

private:
	float width = 0.f;
	float height = 0.f;
};

class Ellipse : public Shape {
public:
	Ellipse(float w, float h) : height(h), width(w) {}
	virtual void outputArea() override {
		cout << 3.14 * width * height << " ";
	}

private:
	float width = 0.f;
	float height = 0.f;
};

class Circle : public Shape {
public:
	Circle(float r) : radius(r) {}
	virtual void outputArea() override {
		cout << radius * radius * 3.14 << " ";
	}

private:
	float radius = 0.f;
};

int main() {
	vector<Shape*> shapes;
	shapes.push_back(new Rectangle(10, 20));
	shapes.push_back(new Triangle(10, 20));
	shapes.push_back(new Ellipse(10, 20));
	shapes.push_back(new Circle(10));

	for (const auto& shape : shapes)
		shape->outputArea();
	cout << endl;
	for (auto& shape : shapes)
		delete shape;
}