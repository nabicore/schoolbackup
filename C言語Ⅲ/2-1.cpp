#include <iostream>
using namespace std;



class Wallet {
public:
	Wallet(int);
	~Wallet();

	void payment(const int&);
	void receive(const int&);
	int yen() const;

private:
	int yen_;
};


int main() {
	Wallet wallet(50);
	wallet.receive(100);
	wallet.payment(20);
	cout << "���z�̎c���F" << wallet.yen() << "�~" << endl;
	return 0;
}






// ----------------------------------------------------
Wallet::Wallet(int init) : yen_(init) {}
Wallet::~Wallet() {}

void Wallet::payment(const int& cost) {
	yen_ -= cost;
}

void Wallet::receive(const int& benefit) {
	yen_ += benefit;
}

int Wallet::yen() const {
	return yen_;
}