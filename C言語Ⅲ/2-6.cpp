#include <iostream>
#include <array>
#include <string>
using namespace std;



class Vec2 {
public:
	Vec2(float x = 0.f, float y = 0.f) 
		: x_(x), y_(y) { }
	
	void output(const string& msg) const {
		cout << msg << x_ << " " << y_ << endl;
	}

	void offset(float f) {
		x_ += f;
		y_ += f;
	}

	void offset(const Vec2& v) {
		x_ += v.x_;
		y_ += v.y_;
	}

	void set(const float& x, const float& y) {
		x_ = x;
		y_ = y;
	}

	void set(const Vec2& v) {
		set(v.x_, v.y_);
	}

	Vec2 operator+ (const Vec2& v) {
		Vec2 ret(x_, y_);
		ret.offset(v);
		return ret;
	}

private:
	float x_;
	float y_;
};


int main() {
	Vec2 pos1(1.5f, 2.5f);
	Vec2 pos2;

	pos2.set(10.5f, 22.2f);
	pos2.output("1 : ");

	Vec2 pos3;
	pos3.set(pos2);
	pos3.output("2 : ");

	pos1.offset(Vec2(10.f, 0.5f));
	pos1.output("3 : ");

	pos2.offset(2.f);
	pos2.output("4 : ");

	pos1.set(10.f, 20.f);
	pos2.set(5.f, 7.f);
	pos3 = pos1 + pos2;
	pos3.output("5 : ");

	return 0;
}