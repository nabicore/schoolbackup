#include <iostream>
#include <string>
#include <array>
using namespace std;


class Counter {
public:
	Counter(int initval, unsigned int addval, int min, int max)
		: now_(initval), addval_(addval), min_(min), max_(max) { }
	~Counter() { }

	void operator++() {
		now_ += addval_;
		if (now_ > max_)
			now_ = min_;
	}

	void operator--() {
		now_ -= addval_;
		if (now_ < min_)
			now_ = max_;
	}

	int operator() ()const {
		return now_;
	}

private:
	int now_;
	int addval_;
	int min_;
	int max_;
};



int main() {
	Counter cnt(0, 1, -2, 3);
	cout << "�����@";
	for (int i = 0; i < 15; ++i, ++cnt)
		cout << cnt() << " ";
	cout << endl << "�t���@";
	for (int i = 0; i < 15; ++i, --cnt)
		cout << cnt() << " ";

	return 0;
}