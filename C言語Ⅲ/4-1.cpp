#include <iostream>


struct Employee {
	int no;
	std::string name;
	std::string department;
};

struct Manager : public Employee {
	int group;
};

struct ParttimeJob : public Employee {
	int hourlyWage;
};