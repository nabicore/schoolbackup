#include <iostream>
#include <string>
#include <array>
#include <fstream>
#include <iomanip>
#include <limits>
using namespace std;

class Length {
public:
	Length(int);
	~Length();

	float mm() const;
	float cm() const;
	float m() const;
	float km() const;


private:
	float mm_;
};

ostream& format(ostream& o) {
	o << fixed << setprecision(2);
	return o;
}

int main() {
	float input;
	cout << "長さをミリメートルで入力：";
	cin >> input;

	Length distance(input);
	cout << format << distance.mm() << "mmは…" << endl;
	cout << format << distance.cm() << "cm" << endl;
	cout << format << distance.m() << "m" << endl;
	cout << format << distance.km() << "km" << endl;
}



// =====================================================
Length::Length(int mm) : mm_(mm) { }
Length::~Length() {}

float Length::mm() const {
	return mm_;
}

float Length::cm() const {
	return mm_ * 0.1f;
}

float Length::m() const {
	return mm_ * 0.001f;
}

float Length::km() const {
	return mm_ * 0.000001f;
}