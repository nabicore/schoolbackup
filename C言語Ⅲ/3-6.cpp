#include <iostream>
#include <ctime>
#include <string>
#include <vector>
using namespace std;

class Game {

	int attmpt = 0;
	int successCnt = 0;
	int seikai = 0;

public:
	Game(const int& play) : attmpt(play) { 
		std::cout << attmpt << "回チャレンジできます！" << std::endl;
		srand((unsigned int)time(NULL));
		seikai = rand() % 10;
	}
	~Game() { }

	void play() {
		while (attmpt) {
			int guess = 0;
			std::cout << "0-9までの数を当ててください" << std::endl;
			std::cin >> guess;
			if (guess == seikai) {
				std::cout << "正解！" << std::endl;
				successCnt += 1;
			}
			else {
				std::cout << "不正解！正解は" << seikai << "でした" << std::endl;
			}
			attmpt -= 1;
		}
	}

	void result() {
		std::cout << "今回は" << successCnt << "回あてました！" << std::endl;
	}
};


int main() {
	Game game(3);
	game.play();
	game.result();
}