#include <iostream>
#include <string>
#include <vector>
using namespace std;


class Integer {
	int value_;

public:
	Integer(int value) : value_(value) {}
	void output() const {
		cout << value_ << endl;
	}
	Integer& operator+=(int value) {
		value_ += value;
		return *this;
	}
	//friend void operator<<(Integer&, int);

	// Friendを使わずに
	void operator<<(const int& v) {
		value_ = v;
	}
};


// Friendを使って
//void operator<<(Integer& obj, int value) {
//	obj.value_ = value;
//}

int main() {
	Integer obj(0);
	obj << 100;
	obj += 10;
	obj.output();
}