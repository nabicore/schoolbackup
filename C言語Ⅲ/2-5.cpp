#include <iostream>
using namespace std;



class Vec2 {
public:
	Vec2();
	Vec2(float, float);
	~Vec2();

	float x() const;

private:
	float x_;
	float y_;
};

int main() {
	Vec2 pos1(1.f, 2.5f);
	Vec2 pos2;

	cout << pos1.x() << endl;
	cout << pos2.x() << endl;
}


// ====================================
Vec2::Vec2() : x_(0.f), y_(0.f) { }
Vec2::Vec2(float xx, float yy) : x_(xx), y_(yy) { }
Vec2::~Vec2() { }

float Vec2::x() const {
	return x_;
}