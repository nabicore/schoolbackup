#include <iostream>
#include <string>
using namespace std;


class Data {
public:
	Data(int n) : n_(n) {
		d_ = new int[n_];
	}

	~Data() {
		delete[] d_;
	}

	int& operator[] (const int& idx) {
		return d_[idx];
	}

	int size() const {
		return n_;
	}

private:
	int* d_;
	int n_;
};



int main() {
	Data data(10);

	for (int i = 0; i < data.size(); ++i)
		data[i] = i;

	for (int i = 0; i < data.size(); ++i)
		cout << data[i] << " ";
}