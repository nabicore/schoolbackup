//-------------------------------------------------------------------
//プレイヤ
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Player.h"
#include  "Task_Map2D.h"

namespace  Player
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//リソースの初期化
	bool  Resource::Initialize()
	{
		this->imageName = "PlayerImg";
		DG::Image_Create(this->imageName, "./data/image/HitTest.bmp");
		return true;
	}
	//-------------------------------------------------------------------
	//リソースの解放
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName);
		return true;
	}
	//-------------------------------------------------------------------
	//「初期化」タスク生成時に１回だけ行う処理
	bool  Object::Initialize()
	{
		//スーパークラス初期化
		__super::Initialize(defGroupName, defName, true);
		//リソースクラス生成orリソース共有
		this->res = Resource::Create();

		//★データ初期化
		this->render2D_Priority[1] = 0.5f;
		this->hitBase = ML::Box2D(-15, -24, 30, 48);
		this->hitFlag = true;
		this->angle_LR = Right;
		this->controllerName = "P1";
		this->motion = Stand;
		this->maxSpeed = 5.f;
		this->addSpeed = 1.f;
		this->decSpeed = 0.5f;
		this->maxFallSpeed = 10.f;
		this->jumpPow = -10.f;
		this->gravity = ML::Gravity(32) * 5;

		//★タスクの生成

		return  true;
	}
	//-------------------------------------------------------------------
	//「終了」タスク消滅時に１回だけ行う処理
	bool  Object::Finalize()
	{
		//★データ＆タスク解放


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//★引き継ぎタスクの生成
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//「更新」１フレーム毎に行う処理
	void  Object::UpDate()
	{
		this->moveCnt++;
		this->animCnt++;
		this->Think();
		this->Move();
		
		ML::Vec2 est(this->moveVec);
		this->CheckMove(est);
		//auto  in = DI::GPad_GetState(this->controllerName);
		//ML::Vec2  est(0, 0);
		//if (in.LStick.L.on) { est.x -= 3;	this->angle_LR = Left; }
		//if (in.LStick.R.on) { est.x += 3;	this->angle_LR = Right; }
		//if (in.LStick.U.on) { est.y -= 3; }
		//if (in.LStick.D.on) { est.y += 3; }

		//this->CheckMove(est);
		////足元接触判定
		//this->hitFlag = this->CheckFoot();
	}
	//-------------------------------------------------------------------
	//「２Ｄ描画」１フレーム毎に行う処理
	void  Object::Render2D_AF()
	{
		ML::Box2D  draw = this->hitBase.OffsetCopy(this->pos);
		ML::Box2D  src(0, 0, 100, 100);

		//	接触の有無に合わせて画像を変更
		if (true == this->hitFlag) {
			src.Offset(100, 0);
		}
		//スクロール対応
		draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
		DG::Image_Draw(this->res->imageName, draw, src);
	}

	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//以下は基本的に変更不要なメソッド
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//-------------------------------------------------------------------
	//タスク生成窓口
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//ゲームエンジンに登録
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//イニシャライズに失敗したらKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//リソースクラスの生成
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }



	void Object::Think() {
		auto  in = DI::GPad_GetState(this->controllerName);

		BChara::Motion nm = this->motion;
		switch (nm) {
		case Stand:		
			if (in.B1.down) nm = Jump;
			if (!this->CheckFoot()) nm = Fall;		
			break;
		case Walk:	break;
		case Jump:	break;
		case Fall:		
			if (this->moveVec.y >= 0) nm = Fall;		
			break;
		case Attack: break;
		case TakeOff: break;
		case Landing: break;
		}
		this->UpdateMotion(nm);
	}

	void Object::Move() {
		auto  in = DI::GPad_GetState(this->controllerName);


		// 重力加速
		switch (this->motion) {
		default:
			if (this->moveVec.y < 0 ||
				!this->CheckFoot()) {
				this->moveVec.y = min(this->moveVec.y + this->gravity, this->maxFallSpeed);
			}
			else {
				this->moveVec.y = 0.f;
			}
			break;

		case Jump:
			if (this->moveCnt == 0)
				this->moveVec.y = this->jumpPow;
			break;

		case Unnon: break;
		}

		// 移動速度
		switch (this->motion) {
		default: break;
		case Unnon: break;
		}

		//モーション毎に固有の処理
		switch (this->motion) {
		case Stand: break;
		case Walk: break;
		case Fall: break;
		case Jump: break;
		case Attack: break;
		}
	}

	BChara::DrawInfo Object::Anim() {
		ML::Color dc(1.f, 1.f, 1.f, 1.f);
		BChara::DrawInfo imageTable[] = {
			{ ML::Box2D(-16, -40, 32, 80), ML::Box2D(  0,  0, 32, 80), dc },
			{ ML::Box2D( -4, -40, 48, 80), ML::Box2D( 32,  0, 32, 80), dc },
			{ ML::Box2D(-20, -40, 48, 80), ML::Box2D( 64,  0, 48, 80), dc },
			{ ML::Box2D(-20, -40, 48, 80), ML::Box2D(112,  0, 48, 80), dc },
			{ ML::Box2D(-24, -40, 48, 80), ML::Box2D( 48, 80, 48, 80), dc },
			{ ML::Box2D(-24, -40, 48, 80), ML::Box2D( 96, 80, 48, 80), dc },
			{ ML::Box2D(-24, -24, 48, 64), ML::Box2D(  0, 80, 48, 64), dc },
			{ ML::Box2D(-24, -24, 48, 64), ML::Box2D(144, 80, 48, 64), dc },
		};
		BChara::DrawInfo rtv;
		int work;
		
		switch (this->motion) {
		default:	rtv = imageTable[0];	break;
		case Jump:	rtv = imageTable[4];	break;
		case Stand:	rtv = imageTable[0];	break;
		case Walk:
			work = this->animCnt / 8;
			work %= 3;
			rtv = imageTable[work + 1];
			break;
		case Fall:	rtv = imageTable[5];	break;
		}

		if (!this->angle_LR) {
			rtv.draw.x = -rtv.draw.x;
			rtv.draw.w = -rtv.draw.w;
		}
		
		return rtv;
	}
}