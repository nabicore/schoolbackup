//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Ev_Message.h"

namespace  Ev_Message
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		{
			struct BGImageParam {
				string name;
				string path;
			};
			BGImageParam w[10]{
				{ "MBGW", "./data/image/MessageF.png" },
				{ "MBG{θ", "./data/image/MessageF.png" },
				{ "MBG’f", "./data/image/MessageF.png" },
				{ "MBG\θA", "./data/image/MessageF.png" },
				{ "MBG\θB", "./data/image/MessageF.png" },
				{ "MBG\θC", "./data/image/MessageF.png" },
				{ "MBG\θD", "./data/image/MessageF.png" },
				{ "MBG\θE", "./data/image/MessageF.png" },
				{ "MBG\θF", "./data/image/MessageF.png" },
				{ "MBG\θG", "./data/image/MessageF.png" },
			};
			for (int i = 0; i < _countof(this->imageNames); ++i) {
				this->imageNames[i] = w[i].name;
				DG::Image_Create(w[i].name, w[i].path);
			}
		}
		
		{
			struct FontParam {
				string name;
				string fontName;
				int width;
				int height;
			 };
			FontParam w[]{
				{ "MFont¬", "HGΫΊήΌ―ΈM-PRO", 8, 16 },
				{ "MFont", "HGΫΊήΌ―ΈM-PRO", 12, 24 },
				{ "MFontε", "HGΫΊήΌ―ΈM-PRO", 16, 32 },
			};
			for (int i = 0; i < _countof(this->fontNames); ++i) {
				this->fontNames[i] = w[i].name;
				DG::Font_Create(w[i].name, w[i].fontName, w[i].width, w[i].height);
			}
		}
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		for (auto& name : imageNames) {
			DG::Image_Erase(name);
		}
		for (auto& name : fontNames) {
			DG::Image_Erase(name);
		}
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		this->Stop();

		this->render2D_Priority[1] = 0.1f;
		this->pos.x = 0;
		this->pos.y = 270 - 128;
		this->msgText = "";
		this->bgNumber = 0;
		this->fontNumber = 0;

		//^XNΜΆ¬

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		return  true;
	}
	void Object::CreateOrReset(stringstream & ss_)
	{
		string taskName;
		ss_ >> taskName;
		auto p = ge->GetTask_One_GN<Object>("bZ[W\¦g", taskName);

		if (nullptr == p) {
			p = Object::Create(true);
			p->Set(taskName, ss_);
		}
		else {
			p->Set(taskName, ss_);
		}
	}
	void Object::Set(const string& taskname_, stringstream& ss_)
	{
		int timeL;
		string msg;
		ss_ >> timeL >> msg;

		this->name = taskname_;
		if (msg == "off") {
			this->Kill();
			return;
		}
		this->timeCnt = 0;
		this->timeLimit = timeL;
		this->msgText = msg;
		
		string::size_type ast;
		while ((ast = msgText.find("*")) != string::npos) {
			msgText.at(ast) = '\n';
		}

		ge->StopAll_GN("Cxg", "ΐsGW");
		this->Stop(false);
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
		auto in = DI::GPad_GetState("P1");
		if ((this->timeLimit != 0 && this->timeCnt >= this->timeLimit) || in.SE.down) {
			this->Stop();
			ge->StopAll_GN("Cxg", "ΐsGW", false);
		}
		else {
			this->timeCnt++;
		}
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
		ML::Box2D draw(0, 0, 480, 128);
		ML::Box2D src(0, 0, 240, 64);
		draw.Offset(this->pos);
		DG::Image_Draw(
			this->res->imageNames[this->bgNumber],
			draw, src, ML::Color(0.9f, 1.f, 1.f, 1.f)
		);
		
		ML::Box2D drawF(6, 6, 480 - 12, 128 - 12);
		drawF.Offset(this->pos);
		DG::Font_Draw(
			this->res->fontNames[this->fontNumber],
			drawF, this->msgText);
	}

	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}