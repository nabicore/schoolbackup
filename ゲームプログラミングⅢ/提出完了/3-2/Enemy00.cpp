//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Enemy00.h"

namespace  Enemy00
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[Xฬ๚ป
	bool  Resource::Initialize()
	{
		this->imageName = "Enemy00Img";
		DG::Image_Create(this->imageName, "./data/image/Enemy00.png");
		return true;
	}
	//-------------------------------------------------------------------
	//\[Xฬ๐๚
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName);
		return true;
	}
	//-------------------------------------------------------------------
	//u๚ปv^XNถฌษP๑พฏsค
	bool  Object::Initialize()
	{
		//X[p[NX๚ป
		__super::Initialize(defGroupName, defName, true);
		//\[XNXถฌor\[XคL
		this->res = Resource::Create();

		//f[^๚ป
		this->render2D_Priority[1] = 0.6f;
		this->hitBase = ML::Box2D(-28, -22, 56, 45);
		this->angle_LR = Left;
		this->motion = Stand;
		this->maxSpeed = 2.f;
		this->addSpeed = 0.7f;
		this->decSpeed = 0.5f;
		this->maxFallSpeed = 10.f;
		this->jumpPow = -6.f;
		this->gravity = ML::Gravity(32) * 5;
		
		//^XNฬถฌ

		return  true;
	}
	//-------------------------------------------------------------------
	//uIนv^XNมลษP๑พฏsค
	bool  Object::Finalize()
	{
		//f[^^XN๐๚


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//๘ซpฌ^XNฬถฌ
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[ษsค
	void  Object::UpDate()
	{
		this->moveCnt += 1;
		this->animCnt += 1;
		this->Think();
		this->Move();
		auto est = this->moveVec;
		this->CheckMove(est);
	}
	//-------------------------------------------------------------------
	//uQc`ๆvPt[ษsค
	void  Object::Render2D_AF()
	{
		BChara::DrawInfo di = this->Anim();
		di.draw.Offset(this->pos);
		di.draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
		DG::Image_Draw(this->res->imageName, di.draw, di.src);
	}

	//-------------------------------------------------------------------
	void Object::Think() {
		BChara::Motion nm = this->motion;
		switch (nm) {
		case Stand:
			if (this->CheckFoot() == false) { nm = Fall; }
			if (this->CheckFront_LR() == true) { nm = Turn; }
			break;
		case Walk:
			if (this->CheckFoot() == false) { nm = Fall; }
			break;
		case Turn:
			if (this->moveCnt >= 5) { nm = Stand; }
			break;
		case Jump:
			if (this->moveVec.y >= 0) { nm = Fall; }
			break;
		case Fall:
			if (this->CheckFoot() == true) { nm = Stand; }
			break;
		case Attack:
			break;
		case TakeOff:
			if (this->CheckFoot() == true) { nm = Stand; }
			break;
		case Landing:
			if (this->CheckFoot() == false) { nm = Fall; }
			break;
		}
		this->UpdateMotion(nm);
	}

	void Object::Move() {
		// dอมฌ
		switch (this->motion) {
		default:
			if (this->moveVec.y < 0 ||
				!this->CheckFoot()) {
				this->moveVec.y = min(this->moveVec.y + this->gravity,
									  this->maxFallSpeed);
			}
			else {
				this->moveVec.y = 0.f;
			}
			break;

		case Unnon:
			break;
		}

		// ฺฎฌx
		switch (this->motion) {
		default:
			if (this->moveVec.x < 0) this->moveVec.x = min(this->moveVec.x + this->decSpeed, 0);
			else					 this->moveVec.x = max(this->moveVec.x - this->decSpeed, 0);
			break;

		case Unnon:
			break;
		}

		// [VษลLฬ
		switch (this->motion) {
		case Fall:
		case Stand:
			switch (this->angle_LR) {
			case Left:		this->moveVec.x = max(-this->maxSpeed, this->moveVec.x - this->addSpeed);	break;
			case Right:		this->moveVec.x = min(+this->maxSpeed, this->moveVec.x + this->addSpeed);	break;
			}
			break;
		case Turn:
			if (this->moveCnt == 3) 
				switch(this->angle_LR) {
				case Left:		this->angle_LR = Right;		break;
				case Right:		this->angle_LR = Left;		break;
				}
			break;
		case Jump:
			break;
		case Attack:
			break;
		}
	}

	BChara::DrawInfo Object::Anim() {
		ML::Color dc(1, 1, 1, 1);
		BChara::DrawInfo imageTable[] = {
			{ ML::Box2D(-32, -24, 64, 48), ML::Box2D(0, 0, 64, 48), dc },
			{ ML::Box2D(-32, -32, 64, 48), ML::Box2D(128, 48, 64, 48), dc },
		};
		BChara::DrawInfo rtv;

		int work;
		switch (this->motion) {
		default:		rtv = imageTable[0];		break;
		// Wv@=====================================
		case Jump:		rtv = imageTable[0];		break;
		// โ~@@@=====================================
		case Stand:		rtv = imageTable[0];		break;
		// เs@@@=====================================
		case Walk:		rtv = imageTable[0];		break;
		// บ@@@=====================================
		case Fall:		rtv = imageTable[1];		break;
		}

		if (!this->angle_LR) {
			rtv.draw.x = -rtv.draw.x;
			rtv.draw.w = -rtv.draw.w;
		}
		return rtv;
	}

	//
	//ศบอ๎{IษฯXsvศ\bh
	//
	//-------------------------------------------------------------------
	//^XNถฌ๛
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWษo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYษธsตฝ็Kill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXฬถฌ
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}