//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Enemy00.h"

namespace  Enemy00
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//リソースの初期化
	bool  Resource::Initialize()
	{
		this->imageName = "Enemy00Img";
		DG::Image_Create(this->imageName, "./data/image/Enemy00.png");
		return true;
	}
	//-------------------------------------------------------------------
	//リソースの解放
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName);
		return true;
	}
	//-------------------------------------------------------------------
	//「初期化」タスク生成時に１回だけ行う処理
	bool  Object::Initialize()
	{
		//スーパークラス初期化
		__super::Initialize(defGroupName, defName, true);
		//リソースクラス生成orリソース共有
		this->res = Resource::Create();

		//★データ初期化
		this->render2D_Priority[1] = 0.6f;
		this->hitBase = ML::Box2D(-28, -22, 56, 45);
		this->angle_LR = Left;
		this->motion = Stand;
		this->maxSpeed = 2.f;
		this->addSpeed = 0.7f;
		this->decSpeed = 0.5f;
		this->maxFallSpeed = 10.f;
		this->jumpPow = -6.f;
		this->gravity = ML::Gravity(32) * 5;
		this->hp = 20;
		
		//★タスクの生成

		return  true;
	}
	//-------------------------------------------------------------------
	//「終了」タスク消滅時に１回だけ行う処理
	bool  Object::Finalize()
	{
		//★データ＆タスク解放


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//★引き継ぎタスクの生成
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//「更新」１フレーム毎に行う処理
	void  Object::UpDate()
	{
		this->moveCnt += 1;
		this->animCnt += 1;
		this->Think();
		this->Move();
		auto est = this->moveVec;
		this->CheckMove(est);

		{// 当たり判定
			ML::Box2D me = this->hitBase.OffsetCopy(this->pos);
			auto targets = ge->GetTask_Group_G<BChara>("プレイヤ");
			for (auto it = targets->begin(); it != targets->end(); ++it) {
				if ((*it)->CheckHit(me)) {
					BChara::AttackInfo at = { 1, 0, 0 };
					(*it)->Received(this, at);
					break;
				}
			}
		}
	}
	//-------------------------------------------------------------------
	//「２Ｄ描画」１フレーム毎に行う処理
	void  Object::Render2D_AF()
	{
		BChara::DrawInfo di = this->Anim();
		di.draw.Offset(this->pos);
		di.draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
		DG::Image_Draw(this->res->imageName, di.draw, di.src);
	}

	//-------------------------------------------------------------------
	void Object::Think() {
		BChara::Motion nm = this->motion;
		switch (nm) {
		case Stand:
			if (this->CheckFoot() == false) { nm = Fall; }
			if (this->CheckFront_LR() == true) { nm = Turn; }
			break;
		case Walk:
			if (this->CheckFoot() == false) { nm = Fall; }
			break;
		case Turn:
			if (this->moveCnt >= 5) { nm = Stand; }
			break;
		case Jump:
			if (this->moveVec.y >= 0) { nm = Fall; }
			break;
		case Fall:
			if (this->CheckFoot() == true) { nm = Stand; }
			break;
		case Attack:
			break;
		case TakeOff:
			if (this->CheckFoot() == true) { nm = Stand; }
			break;
		case Landing:
			if (this->CheckFoot() == false) { nm = Fall; }
			break;
		}
		this->UpdateMotion(nm);
	}

	void Object::Move() {
		// 重力加速
		switch (this->motion) {
		default:
			if (this->moveVec.y < 0 ||
				!this->CheckFoot()) {
				this->moveVec.y = min(this->moveVec.y + this->gravity,
									  this->maxFallSpeed);
			}
			else {
				this->moveVec.y = 0.f;
			}
			break;

		case Unnon:
			break;
		}

		// 移動速度
		switch (this->motion) {
		default:
			if (this->moveVec.x < 0) this->moveVec.x = min(this->moveVec.x + this->decSpeed, 0);
			else					 this->moveVec.x = max(this->moveVec.x - this->decSpeed, 0);
			break;

		case Unnon:
			break;
		}

		// モーション毎に固有の処理
		switch (this->motion) {
		case Fall:
		case Stand:
			switch (this->angle_LR) {
			case Left:		this->moveVec.x = max(-this->maxSpeed, this->moveVec.x - this->addSpeed);	break;
			case Right:		this->moveVec.x = min(+this->maxSpeed, this->moveVec.x + this->addSpeed);	break;
			}
			break;
		case Turn:
			if (this->moveCnt == 3) 
				switch(this->angle_LR) {
				case Left:		this->angle_LR = Right;		break;
				case Right:		this->angle_LR = Left;		break;
				}
			break;
		case Jump:
			break;
		case Attack:
			break;
		}
	}

	BChara::DrawInfo Object::Anim() {
		ML::Color dc(1, 1, 1, 1);
		BChara::DrawInfo imageTable[] = {
			{ ML::Box2D(-32, -24, 64, 48), ML::Box2D(0, 0, 64, 48), dc },
			{ ML::Box2D(-32, -32, 64, 48), ML::Box2D(128, 48, 64, 48), dc },
		};
		BChara::DrawInfo rtv;

		int work;
		switch (this->motion) {
		default:		rtv = imageTable[0];		break;
		// ジャンプ　=====================================
		case Jump:		rtv = imageTable[0];		break;
		// 停止　　　=====================================
		case Stand:		rtv = imageTable[0];		break;
		// 歩行　　　=====================================
		case Walk:		rtv = imageTable[0];		break;
		// 落下　　　=====================================
		case Fall:		rtv = imageTable[1];		break;
		}

		if (!this->angle_LR) {
			rtv.draw.x = -rtv.draw.x;
			rtv.draw.w = -rtv.draw.w;
		}
		return rtv;
	}

	void Object::Received(BChara* from_, AttackInfo at_) {
		this->hp -= at_.power;
		if (this->hp <= 0) {
			this->Kill();
		}
	}

	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//以下は基本的に変更不要なメソッド
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//-------------------------------------------------------------------
	//タスク生成窓口
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//ゲームエンジンに登録
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//イニシャライズに失敗したらKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//リソースクラスの生成
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}