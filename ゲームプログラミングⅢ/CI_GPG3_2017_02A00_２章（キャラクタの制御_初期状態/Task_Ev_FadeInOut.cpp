//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include "Task_Ev_FadeInOut.h"

namespace  Ev_FadeInOut
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		this->render2D_Priority[1] = 0.005f;
		this->cnt = 0;
		this->imageName = "";
		this->Stop();
		
		//^XNΜΆ¬

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ
		if (this->imageName != "") {
			DG::Image_Erase(this->imageName);
		}

		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
		if (this->mode == In) {
			this->cnt--;
			if (this->cnt < 0) {
				ge->StopAll_GN("Cxg", "ΐsGW", false);
				this->Kill();
			}
		}
		if (this->mode == Out) {
			this->cnt++;
			if (this->cnt > 60) {
				ge->StopAll_GN("Cxg", "ΐsGW", false);
				this->Stop();
			}
		}
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
		ML::Box2D draw(0, 0, ge->screenWidth, ge->screenHeight);
		DG::Image_Draw(this->imageName, draw, this->src,
			ML::Color(this->cnt / 60.f, 1, 1, 1));
	}

	void Ev_FadeInOut::Object::CreateOrFadeOut(stringstream& ss_)
	{
		auto p = ge->GetTask_One_GN<Object>(defGroupName, defName);
		if (!p) {
			p = Object::Create(true);
			p->Set(ss_);
		}
		else {
			p->Set(ss_);
		}
	}

	void Object::Set(stringstream& ss_) {
		string filePath;
		ss_ >> filePath;

		if (filePath == "in") {
			this->mode = In;
			this->cnt = 60;
		}
		else {
			this->mode = Out;
			this->cnt = 0;
			this->imageName = this->groupName + this->name + "Img";
			DG::Image_Create(this->imageName, filePath);
			POINT s = DG::Image_Size(this->imageName);
			this->src = ML::Box2D(0, 0, s.x, s.y);
		}
		ge->StopAll_GN("Cxg", "ΐsGW");
		this->Stop(false);
	}
	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}
