#include "AppearObject.h"
#include "Enemy00.h"
#include "Task_Item.h"
#include "Task_Player.h"
#include "Task_Sprite.h"

BChara::SP AppearObject_BChara(const string& name_) {
	BChara::SP w = nullptr;
		 if ("Player" == name_) w = Player::Object::Create(true);
	else if ("Enemy00" == name_) w = Enemy00::Object::Create(true);
	else if ("Item00" == name_) w = Item00::Object::Create(true);
	else if ("Sprite" == name_) w = Sprite::Object::Create(true);
	return w;
}