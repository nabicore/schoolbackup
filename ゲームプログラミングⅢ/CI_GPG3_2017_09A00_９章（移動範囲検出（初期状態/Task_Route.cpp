//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Route.h"
#include  "Task_Sarch.h"

namespace  Route
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		this->imageName = "RouteImg";
		DG::Image_Create(this->imageName, "./data/image/Route.png");
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName);
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		this->render2D_Priority[1] = 0.4f;
		this->len = 0;
		
		//^XNΜΆ¬

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
		ML::Box2D src(0, 0, 16, 16);
		for (int i = 0; i < this->len; ++i) {
			ML::Box2D draw(0, 0, 16, 16);
			draw.Offset(this->arr[i].x * 16, this->arr[i].y * 16);
			draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
			DG::Image_Draw(this->res->imageName, draw, src);
		}
	}


	bool Object::Check(POINT end_) {
		this->len = 0;
		if (auto sarch = ge->GetTask_One_G<Sarch::Object>("Tυp}bv")) {
			if (end_.x < 0 || end_.x >= sarch->sizeX ||
				end_.y < 0 || end_.y >= sarch->sizeY) {
				return false;
			}
			if (sarch->area[end_.y][end_.x] == -1) {
				return false;
			}

			POINT sPos = end_;
			POINT bkLog[16];
			for (int i = 0; i < 16; ++i) {
				bkLog[i] = sPos;
				enum { cen, left, right, up, down };
				int m[5] = { -1, -1, -1, -1, -1 };
				m[cen] = sarch->area[sPos.y][sPos.x];
				if (sPos.x - 1 >= 0)			m[left] = sarch->area[sPos.y][sPos.x - 1];
				if (sPos.x + 1 < sarch->sizeX)	m[right] = sarch->area[sPos.y][sPos.x + 1];
				if (sPos.y - 1 >= 0)			m[up] = sarch->area[sPos.y - 1][sPos.x];
				if (sPos.y + 1 < sarch->sizeY)	m[down] = sarch->area[sPos.y + 1][sPos.x];

				int maxPos = cen;
				int emax = m[cen];
				for (int j = 1; j < 5; ++j) {
					if (emax < m[j]) { emax = m[j]; maxPos = j; }
				}

				if (maxPos == cen) {
					this->len = i + 1;
					for (int j = 0; j <= i; ++j) {
						this->arr[j] = bkLog[i - j];
					}
					return true;
				}
				else {
					POINT mv[5] = { {0, 0}, {-1, 0}, {+1, 0}, {0, -1}, {0, +1} };
					sPos.x += mv[maxPos].x;
					sPos.y += mv[maxPos].y;
				}
			}
		}
		return false;
	}

	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}