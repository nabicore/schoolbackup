//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Ev_Message.h"
#include  "Task_Ev_Image.h"
#include  "Task_EventEngine.h"
#include  "Task_Ev_FadeInOut.h"
#include  "Task_Map2D.h"
#include  "AppearObject.h"

namespace  EventEngine
{
	Object::WP Object::instance;
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		
		//^XNΜΆ¬
		ge->StopAll_G("vC");
		ge->StopAll_G("G");
		ge->StopAll_G("eivCj");
		ge->StopAll_G("ACe");

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ
		if (this->evFile.is_open()) {
			this->evFile.close();
		}

		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		ge->StopAll_G("vC"	, false);
		ge->StopAll_G("G", false);
		ge->StopAll_G("eivCj", false);
		ge->StopAll_G("ACe", false);

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
		if (!this->evFile) { this->Kill(); }
		string lineText;
		string headerText;
		string dataText;
		while (this->ReadLine(lineText)) {
			string::size_type t = lineText.find(">");
			headerText = lineText.substr(0, t);
			dataText = lineText.substr(t + 1);
			bool next = this->Execute(headerText, dataText);
			if (false == next ||
				BTask::eActive != this->CheckState()) {
				break;
			}
		}
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
	}

	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	Object::SP Object::Create_Mutex()
	{
		if (auto p = instance.lock()) {
			return false;
		}
		else {
			p = Object::Create(true);
			instance = p;
			return p;
		}
	}
	bool Object::Set(const string& fpath_)
	{
		if (this->evFile.is_open()) {
			this->evFile.close();
		}

		this->evFile.open(fpath_);
		if (!this->evFile) {
			return false;
		}

		return true;
	}
	bool Object::ReadLine(string& lineT_)
	{
		bool rtv = false;
		while (getline(this->evFile, lineT_)) {
			if (string::npos == lineT_.find_first_not_of(" @"))		{ continue; }
			if ('/' == lineT_.at(0))								{ continue; }
			if (string::npos == lineT_.find(">"))					{ continue; }
			rtv = true;
			break;
		}
		return rtv;
	}
	bool Object::Execute(string& hs_, string& ds_)
	{
		string::size_type t;
		while ((t = ds_.find_first_of("(,);")) != string::npos) {
			ds_[t] = ' ';
		}

		stringstream ss;
		ss << ds_;
		if (hs_ == "end") {
			this->Kill();
		}

		else if (hs_ == "msg")		{ Ev_Message::Object::CreateOrReset(ss); }
		else if (hs_ == "evimg")	{ Ev_Image::Object::CreateOrReset(ss); }
		else if (hs_ == "if")		{ this->If(ss); }
		else if (hs_ == "flag")		{ this->EventFlag(ss); }
		else if (hs_ == "img")		{ this->Image(ss); }
		else if (hs_ == "fileset")	{ this->FileSet(ss); }
		else if (hs_ == "appear")	{ this->AppearObject(ss); }
		else if (hs_ == "kill")		{ this->KillObject(ss); }
		else if (hs_ == "chara")    { this->ModifyChara(ss); }
		else if (hs_ == "map")		{ this->MapLoad(ss); }
		else if (hs_ == "label") {}
		else if (hs_ == "fade_io")	{ Ev_FadeInOut::Object::CreateOrFadeOut(ss); }
		else {
			return false;
		}
		return true;
	}
	bool Object::EventFlag(stringstream& ss_) {
		string flagName;
		string sign;
		float value;
		ss_ >> flagName >> sign >> value;

		if (sign == "=") { ge->evFlags[flagName] = value; }
		else if (sign == "+") { ge->evFlags[flagName] += value; }
		else if (sign == "-") { ge->evFlags[flagName] -= value; }
		else { return false; }
		return true;
	}
	bool Object::If(stringstream& ss_) {
		string flagKind;
		ss_ >> flagKind;
		bool flag = false;
		// int skip = 0;
		string labelName;
		if (flagKind == "ev_flag") {
			string flagName;
			string sign;
			float value;
			ss_ >> flagName >> sign >> value >> labelName;
			if (sign == "==") { flag = ge->evFlags[flagName] == value; }
			else if (sign == ">") { flag = ge->evFlags[flagName] > value; }
			else if (sign == "<") { flag = ge->evFlags[flagName] < value; }
			else { return false; }
		}
		else if (flagKind == "charaParam") {

		}
		else if (flagKind == "System") {

		}

		if (flag) {
			this->evFile.seekg(0, ios_base::beg);
			string lt;
			while (this->ReadLine(lt)) {
				string::size_type t = lt.find(">");
				string headerStr = lt.substr(0, t);
				string dataStr = lt.substr(t + 1);
				if ("label" == headerStr) {
					stringstream ss;
					ss << dataStr;
					string labelName2;
					ss >> labelName2;
					if (labelName == labelName2)
						break;
				}
			}
		}

		return true;
	}

	bool Object::Image(stringstream& ss_) {
		string OffScreenName;
		string filePath;
		ss_ >> OffScreenName >> filePath;
		DG::Image_Create(OffScreenName, filePath);
		return true;
	}
	bool Object::FileSet(stringstream& ss_) {
		string filePath;
		ss_ >> filePath;
		return this->Set(filePath);
	}
	bool Object::AppearObject(stringstream& ss_) {
		string kind;
		float posX, posY;
		ss_ >> kind >> posX >> posY;
		BChara::SP obj = AppearObject_BChara(kind);
		if (obj) {
			obj->pos.x = posX;
			obj->pos.y = posY;
			obj->Stop();
		}
		while (false == ss_.eof()) {
			string paramName, eq;
			ss_ >> paramName >> eq;
			if (eq != "=") break;
			if (paramName == "name") ss_ >> obj->name;
			if (paramName == "jumpPow") ss_ >> obj->jumpPow;
			if (paramName == "maxFallSpeed") ss_ >> obj->maxFallSpeed;
			if (paramName == "maxSpeed") ss_ >> obj->maxSpeed;
			if (paramName == "eventFileName") ss_ >> obj->eventFileName;
		}
		return nullptr != obj;
	}
	bool Object::MapLoad(stringstream& ss_) {
		auto map = ge->GetTask_One_GN<Map2D::Object>("tB[h", "}bv");
		if (nullptr == map) {
			map = Map2D::Object::Create(true);
		}
		string mapFilePath;
		ss_ >> mapFilePath;
		return map->Load(mapFilePath);
	}
	bool Object::ModifyChara(stringstream& ss_) {
		string gname, name;
		ss_ >> gname >> name;
		if (auto mc = ge->GetTask_One_GN<BChara>(gname, name)) {
			while (false == ss_.eof()) {
				string paramName, eq;
				ss_ >> paramName >> eq;
				if (eq == "=") {
					if (paramName == "posX") { ss_ >> mc->pos.x; }
					if (paramName == "posY") { ss_ >> mc->pos.y; }
					if (paramName == "jumpPow") { ss_ >> mc->jumpPow; }
					if (paramName == "maxFallSpeed") { ss_ >> mc->maxFallSpeed; }
					if (paramName == "maxSpeed") { ss_ >> mc->maxSpeed; }
					if (paramName == "event") { ss_ >> mc->eventFileName; }
				}
			}
			return true;
		}
		return false;
	}
	bool Object::KillObject(stringstream& ss_) {
		string gname;
		ss_ >> gname;
		if (false == ss_.eof()) {
			string name;
			ss_ >> name;
			ge->KillAll_GN(gname, name);
		}
		else {
			ge->KillAll_G(gname);
		}
		return true;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}