//-----------------------------------------------------------------------------
//キャラクタ汎用スーパークラス
//-----------------------------------------------------------------------------
#include "BChara.h"
#include  "MyPG.h"
#include  "Task_Map2D.h"

//-----------------------------------------------------------------------------
//めり込まない移動処理
void BChara::CheckMove(ML::Vec2&  e_)
{
	//マップが存在するか調べてからアクセス
	auto   map = ge->GetTask_One_GN<Map2D::Object>("フィールド", "マップ");
	if (nullptr == map) { return; }//マップが無ければ判定しない(出来ない）

										  //横軸に対する移動
	while (e_.x != 0) {
		float  preX = this->pos.x;
		if (e_.x >= 1) { this->pos.x += 1;		e_.x -= 1; }
		else if (e_.x <= -1) { this->pos.x -= 1;		e_.x += 1; }
		else { this->pos.x += e_.x;		e_.x = 0; }
		ML::Box2D  hit = this->hitBase.OffsetCopy(this->pos);
		if (true == map->CheckHit(hit)) {
			this->pos.x = preX;		//移動をキャンセル
			break;
		}
	}
	//縦軸に対する移動
	while (e_.y != 0) {
		float  preY = this->pos.y;
		if (e_.y >= 1) { this->pos.y += 1;		e_.y -= 1; }
		else if (e_.y <= -1) { this->pos.y -= 1;		e_.y += 1; }
		else { this->pos.y += e_.y;		e_.y = 0; }
		ML::Box2D  hit = this->hitBase.OffsetCopy(this->pos);
		if (true == map->CheckHit(hit)) {
			this->pos.y = preY;		//移動をキャンセル
			break;
		}
	}
}

bool BChara::SearchRotation(const ML::Vec2& tg_) {
	ML::Vec2 toTarget = tg_ - this->pos;
	float dist = toTarget.Length();
	if (dist > this->searchDist) {
		return false;
	}
	ML::Vec2 toTargetN = toTarget.Normalize();
	ML::Vec2 moveVecN = ML::Vec2(cos(this->angle), sin(this->angle));
	float dot = moveVecN.x * toTargetN.x + moveVecN.y * toTargetN.y;
	if (dot < cos(this->searchFOV)) {
		return false;
	}
	float cr = moveVecN.x * toTargetN.y - moveVecN.y * toTargetN.x;
	if (cr >= 0) this->angle += this->rotPow;
	else		 this->angle -= this->rotPow;
	return true;
}