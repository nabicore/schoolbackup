//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Sarch.h"

namespace  Sarch
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		this->imageName = "SarchIconImg";
		DG::Image_Create(this->imageName, "./data/image/MoveCheck.png");
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName);
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		ZeroMemory(this->area, sizeof(this->area));
		ZeroMemory(this->cost, sizeof(this->cost));
		this->sizeX = 0;
		this->sizeY = 0;
		this->hitBase = ML::Box2D(0, 0, 0, 0);
		
		//^XNΜΆ¬

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
		ML::Box2D screen(0, 0, ge->screen2DWidth, ge->screen2DHeight);
		screen.Offset(ge->camera2D.x, ge->camera2D.y);
		if (false == this->hitBase.Hit(screen)) {
			return;
		}

		RECT r = {
			screen.x,
			screen.y,
			screen.x + screen.w,
			screen.y + screen.h
		};
		RECT m = {
			this->hitBase.x,
			this->hitBase.y,
			this->hitBase.x + this->hitBase.w,
			this->hitBase.y + this->hitBase.h
		};
		r.left = max(r.left, m.left);
		r.top = max(r.top, m.top);
		r.right = min(r.right, m.right);
		r.bottom = min(r.bottom, m.bottom);

		int sx, sy, ex, ey;
		sx = r.left / 16;
		sy = r.top / 16;
		ex = (r.right - 1) / 16;
		ey = (r.bottom - 1) / 16;
		for (int y = sy; y <= ey; ++y) {
			for (int x = sx; x <= ex; ++x) {
				ML::Box2D draw(x * 16, y * 16, 16, 16);
				draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
				ML::Box2D src;
				if (this->cost[y][x] == 99) src = ML::Box2D(0, 32, 16, 16);
				else if (this->area[y][x] == -1) src = ML::Box2D(0, 16, 16, 16);
				else {
					int a = min(this->area[y][x], 9);
					src = ML::Box2D(a * 16, 0, 16, 16);
				}
				DG::Image_Draw(this->res->imageName, draw, src, ML::Color(0.3f, 1, 1, 1));
			}
		}
	}

	void Object::Check(Map2D::Object::SP& m_, POINT p_, int ms_) {
		this->sizeX = m_->sizeX;
		this->sizeY = m_->sizeY;
		this->hitBase = ML::Box2D(0, 0, this->sizeX * 16, this->sizeY * 16);
		for (int y = 0; y < this->sizeY; ++y) {
			for (int x = 0; x < this->sizeX; ++x) {
				if (m_->arr[y][x] < 8) { this->cost[y][x] = m_->arr[y][x] + 1; }
				else { this->cost[y][x] = 99; }
				this->area[y][x] = -1;
			}
		}
		if (p_.x >= 0 && p_.y >= 0 && p_.x < this->sizeX && p_.y < this->sizeY) {
			this->area[p_.y][p_.x] = ms_;
			CheckSub(p_.x, p_.y, ms_);
		}
	}
	
	void Object::CheckSub(int px_, int py_, int ms_) {
		int sx = px_, ex = px_;
		int sy = py_, ey = py_;
		int t;

		for (; ms_ > 0; --ms_) {
			for (int y = sy; y <= ey; ++y) {
				for (int x = sx; x <= ex; ++x) {
					if (this->area[y][x] != ms_) continue;
					if (x - 1 >= 0) {
						t = ms_ - this->cost[y][x - 1];
						if (this->area[y][x - 1] < t) this->area[y][x - 1] = t;
					}
					if (y - 1 >= 0) {
						t = ms_ - this->cost[y - 1][x];
						if (this->area[y - 1][x] < t) this->area[y - 1][x] = t;
					}
					if (x + 1 >= 0) {
						t = ms_ - this->cost[y][x + 1];
						if (this->area[y][x + 1] < t) this->area[y][x + 1] = t;
					}
					if (y + 1 >= 0) {
						t = ms_ - this->cost[y + 1][x];
						if (this->area[y + 1][x] < t) this->area[y + 1][x] = t;
					}
				}
			}
			sx = max(0, sx - 1);
			sy = max(0, sy - 1);
			ex = min(this->sizeX - 1, ex + 1);
			ey = min(this->sizeY - 1, ey + 1);
		}
	}
	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}