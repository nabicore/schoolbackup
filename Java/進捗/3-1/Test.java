class P03010s {
	public static void main(String[] args) {
		int[][] students = new int[] {
			{ 80, 75, 82 }, 
			{ 74, 68, 50 },
			{ 98, 58, 84 } 
		};

		for (int i = 0; i < students.length; ++i) {
			int sum = 0;
			for (int j = 0; j < students[i].length; ++j) {
				sum += students[i][j];
			}
			double avg = sum / 3.d;
			System.out.println("学生" + i + "の平均は" + avg + "点です");
		}
	}
}