class P02030{
	public static void main(String[] args){
		boolean bo = 3<4;				// 論理演算的にTrueが出る
		byte by = 127;					
		int in = 0xFFFFFFFF;			// Overflow 発生
		System.out.println(!bo);		// bo == trueの逆だからtrueが出る
		System.out.println(++by);		// Overflow 発生
		System.out.println(in);
		System.out.printf("%X\n",in<<8);	// 
		System.out.printf("%X\n",in>>>8);
		System.out.println(in>>8);
		System.out.println(~in);
	}
}