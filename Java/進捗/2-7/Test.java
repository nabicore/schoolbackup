import java.util.Scanner;
class P02040s{
	public static void main(String[] args){
		
		double bmi;
		do {
			bmi = getBMI();
		} while(!PrintResult(bmi));
	}
	
	static double getBMI() {
		Scanner sc;
		sc = new Scanner(System.in);
		
		float weight = 0.f,
			  height = 0.f;

		try {  
			System.out.print("体重:");
			weight = sc.nextFloat();
			
			System.out.print("身長:");
			height = sc.nextFloat();
		}

		catch (java.util.InputMismatchException e) {
			return -1;
		}

		return (weight / (height * height)) * 10000.f;
	}
	
	static boolean PrintResult(double bmi) {
		if (bmi < 0.f) {
			System.out.println("不正な入力です。");
			return false;
		}
		String status = "";
		double meyasu = 0.f;
		if 		(bmi < 18.5)		status = "低体重";
		else if (bmi < 25)			status = "普通体重";
		else if (bmi < 30)			{ status = "肥満（1度)";    }
		else if (bmi < 35)			{ status = "肥満（2度)";    }
		else if (bmi < 40)			{ status = "肥満（3度)";    }
		else						{ status = "肥満（4度)";    }
		System.out.printf("あなたは現在%s状態です。w", status);
		return true;
	}
}
