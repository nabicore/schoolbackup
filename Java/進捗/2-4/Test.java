import java.util.Scanner;
class P02040s{
	public static void main(String[] args){
		Scanner sc;
		sc = new Scanner(System.in);
		
		float weight = 0.f,
			  height = 0.f;
			  
		System.out.print("体重:");
		weight = sc.nextFloat();
		
		System.out.print("身長:");
		height = sc.nextFloat();
		
		System.out.printf("あなたのBMIは%.2fです。\n", (weight / (height * height)) * 10000.f);
	}
}
