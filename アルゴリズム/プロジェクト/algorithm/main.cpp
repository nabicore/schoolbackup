#include <iostream>
#include <string>
using namespace std;

class Reidai01
{
public:
  Reidai01()
  {

  }
};

class Kadai01
{
	float GetInput(std::string msg) {
		float ret = 0;
		std::cout << msg;
		std::cin >> ret;
		return ret;
	}

public:
	Kadai01()
	{
		const float ApplePrice = 100.f;
		const float OrangePrice = 50.f;
		float appleCnt= 0;
		float orangeCnt = 0;

		appleCnt = GetInput("りんごの数：");
		orangeCnt = GetInput("みかんの数：");

		float price = ((ApplePrice * appleCnt) + (OrangePrice * orangeCnt)) * 1.08f;
		std::cout << "消費税を含めた価格：" << price << std::endl;
	}
};

class Kadai02
{
public:
	Kadai02()
	{
		int input1 = 0;
		int input2 = 0;
		std::cin >> input1 >> input2;
		std::cout << "商：" << input1 / input2 << std::endl;
		std::cout << "余り：" << input1 % input2 << std::endl;
	}
};

class Kadai03
{
public:
	Kadai03()
	{
		float input1 = 0.f;
		float input2 = 0.f;
		int result = 0.f;
		std::cin >> input1 >> input2;
		result = input1 / input2;
		std::cout << "商：" << (int)result << std::endl;
		std::cout << "余り：" << input1 - (result * input2) << std::endl;
	}
};

class Kadai04
{
public:
	Kadai04()
	{
		int input = 0;
		std::cin >> input;
		std::cout << 30 - input << std::endl;
	}
};

class Kadai05
{
public:
	Kadai05()
	{
		int a = 0,
			b = 0,
			c = 0;
		int ab = 32,
			bc = 29,
			ac = 27;

		a = (ab + ac - bc) / 2;
		b = (ab + bc - ac) / 2;
		c = (bc + ac - ab) / 2;

		std::cout << "A: " << a << std::endl;
		std::cout << "B: " << b << std::endl;
		std::cout << "C: " << c << std::endl;
	}
};

class Kadai06
{
public:
	Kadai06(int no)
	{
		switch (no % 6) {
		case 0:
		case 5:
			std::cout << "色は赤" << std::endl;
			break;

		case 1:
		case 2:
			std::cout << "色は白" << std::endl;
			break;

		case 3:
		case 4:
			std::cout << "色は青" << std::endl;
			break;
		}
	}
};

struct Math {
	struct Bunsu
	{
		int bunshi,
			bunbo;

		void input() {
			cout << "分子を入力：";
			cin >> bunshi;
			cout << "分母を入力：";
			cin >> bunbo;
		}

		void output() {
			if (bunbo == 1) {
				cout << bunshi << endl;
			}
			else {
				cout << bunshi << "/" << bunbo << std::endl;
			}
		}

		Bunsu operator+(Bunsu& b1) {
			Bunsu ret;
			
			ret.bunbo = calcLCM(this->bunbo, b1.bunbo);
			ret.bunshi =
				this->bunshi * int(ret.bunbo / this->bunbo) +
				b1.bunshi * int(ret.bunbo / b1.bunbo);
			return ret;
		}

		Bunsu operator-(Bunsu& b1) {
			Bunsu ret;

			ret.bunbo = calcLCM(this->bunbo, b1.bunbo);
			ret.bunshi =
				this->bunshi * int(ret.bunbo / this->bunbo) -
				b1.bunshi * int(ret.bunbo / b1.bunbo);
			return ret;
		}

		Bunsu operator*(Bunsu& b1) {
			Bunsu ret = *this;
			ret.bunbo *= b1.bunbo;
			ret.bunshi *= b1.bunshi;
			return ret;
		}

		Bunsu operator/(Bunsu& b1) {
			Bunsu ret;
			ret.bunbo = (this->bunbo % b1.bunshi == 0) ? this->bunbo / b1.bunshi : this->bunbo * b1.bunshi;
			ret.bunshi = (this->bunshi % b1.bunbo == 0) ? this->bunshi / b1.bunbo : this->bunshi * b1.bunbo;
			ret = reduce(ret);
			return ret;
		}
	};

	static Math::Bunsu reduce(const Math::Bunsu& v)
	{
		Math::Bunsu ret = v;
		int gcd = Math::calcGCD(ret.bunbo, ret.bunshi);
		if (gcd > 1) {
			ret.bunshi /= gcd;
			ret.bunbo /= gcd;
		}
		return ret;
	}

	struct Time {
		int minute;
		int hour;

		void input() {
			cout << "時：";
			cin >> hour;
			cout << "分：";
			cin >> minute;
		}

		Time operator+(Time& t) {
			Time ret = *this;
			ret.minute = ret.minute + t.minute;
			if (ret.minute >= 60) {
				int extraHour = ret.minute / 60;
				ret.hour += extraHour;
				ret.minute = ret.minute - (60 * extraHour);
			}
			ret.hour += t.hour;
			return ret;
		}
	};

	static int calcGCD(int a, int b) {
		int ret = (int)fmin(a, b);
		while (ret > 0) {
			if (a % ret == 0 &&
				b % ret == 0)
				break;
			ret -= 1;
		}
		return ret;
	}

	static int calcLCM(int a, int b) {
		int tmp = -1;
		int ret = 1;
		while (tmp != 1) {
			tmp = calcGCD(a, b);
			a = a / tmp;
			b = b / tmp;
			ret *= tmp;
		}
		ret *= a;
		ret *= b;
		return ret;
	}
};

class Kadai07
{
public:
	Kadai07()
	{
		cout << "最大公約数" << Math::calcGCD(12, 18) << endl;
		cout << "最小公倍数" << Math::calcLCM(12, 18) << endl;
	}
};

class Kadai08 {
	void output(const Math::Bunsu& v1, const Math::Bunsu& v2)
	{
		std::cout <<
			v1.bunshi << "/" << v1.bunbo <<
			" => " <<
			v2.bunshi << "/" << v2.bunbo << std::endl;
	} 

public:   
	Kadai08()   
	{     
		Math::Bunsu v1, v2;
		cout << "分子を入力：";     
		cin >> v1.bunshi;     
		cout << "分母を入力：";     
		cin >> v1.bunbo;     
		v2 = Math::reduce(v1);     
		output(v1, v2);   
	} 
};

class Kadai09 {
public:
	Kadai09() {
		Math::Bunsu v1, v2, kekka;
		v1.input();
		v2.input();
		kekka = v1 + v2;
		kekka = Math::reduce(kekka);
		kekka.output();
	}
};

class Kadai10 {
public:
	Kadai10() {
		Math::Bunsu v1, v2;
		v1.input();
		v2.input();
		cout << "==足し算==" << endl;
		(v1 + v2).output();
		cout << "==引き算==" << endl;
		(v1 - v2).output();
		cout << "==掛け算==" << endl;
		(v1 * v2).output();
		cout << "==割り算==" << endl;
		(v1 / v2).output();
	}
};

class Kadai11 {
public:
	Kadai11() {
		Math::Time t1, t2, ret;
		t1.input();
		cout << "経過分：";
		t2.hour = 0;
		cin >> t2.minute;
		ret = t1 + t2;
		cout << t1.hour << "時" << t1.minute << "分から" << t2.minute << "分経過すると" << ret.hour << "時" << ret.minute << "分" << std::endl;
	}
};

//エントリポイント
int main()
{
	Kadai11();
  system("pause");
}
