#include <vector>
#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	template <typename T> class Coord {
	public:
		T x;
		T y;

		inline bool Equals(const Coord& c_) {
			return c_.x == this->x && c_.y == this->y;
		}

		Coord(int x, int y) {
			this->x = x;
			this->y = y;
		}

		Coord(const Coord& c_) {
			this->x = c_.x;
			this->y = c_.y;
		}

		Coord(ML::Vec2 v_) {
			this->x = (int)v_.x;
			this->y = (int)v_.y;
		}

		bool isOutOfRange(const ML::Box2D b_) {
			return
				this->x >= b_.x &&
				this->x <= b_.x + b_.w &&
				this->y >= b_.y &&
				this->y <= b_.y + b_.h;
		}

		Coord operator+(const Coord& c_) {
			Coord ret = *this;
			ret.x += c_.x;
			ret.y += c_.y;
			return ret;
		}

		bool operator==(const Coord& c_) {
			return Equals(c_);
		}

		ML::Vec2 ToRealPos(float tileSet) {
			return ML::Vec2(this->x * tileSet, this->y * tileSet);
		}
	};
	typedef struct _tagKawa {
		Coord<int>			coord;
		Coord<int>			currentCoord;
		ML::Vec2			dir;
	}Kawa;

	struct MapData {
		int					arr[8][15];
		ML::Box2D			chip[32];
	};
	void Map_Initialize(MapData& md_);
	bool Map_Load(MapData& md_, int n_);
	void Map_UpData(MapData& md_);
	void Map_Render(MapData& md_);

	//ゲーム情報
	DI::VGamePad  in1;

	MapData mapData;
	std::vector<Kawa> kawaCoords;
	int tick = 0;


	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");	//背景
		Map_Initialize(mapData);
		Map_Load(mapData, 0);

		//川を作る
		Coord<int> kawaPos = Coord<int>(6, 0);
		bool isFalling = true;
		while (kawaPos.isOutOfRange(ML::Box2D(0, 0, 8, 15))) {
			mapData.arr[kawaPos.y][kawaPos.x] = (isFalling) ? 16 : 17;
			ML::Vec2 kawaDir = (isFalling) ? ML::Vec2(0, -1) : ML::Vec2(1, 0);
			kawaCoords.push_back({ kawaPos, kawaPos, kawaDir });

			Coord<int> testPos = kawaPos + Coord<int>(0, 1);
			isFalling = true;

			if (mapData.arr[testPos.y][testPos.x] != 0) {
				testPos = kawaPos + Coord<int>(1, 0);
				isFalling = false;
			}
			kawaPos = testPos;
		}
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		tick += 1;
		for (auto& k : kawaCoords) {
			
		}

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		Map_Render(mapData);
	}
	void Map_Initialize(MapData & md_)
	{
		for (int y = 0; y < 8; ++y)
			for (int x = 0; x < 15; ++x) {
				mapData.arr[y][x] = 0;
			}

		for (int c = 0; c < 32; ++c) {
			int x = (c % 8);
			int y = (c / 8);
			mapData.chip[c] = ML::Box2D(x * 32, y * 32, 32, 32);
		}
	}
	bool Map_Load(MapData & md_, int n_)
	{
		int w_map[8][15] = {
			{ 8,8,8,8,8,8,8,8,8,8,8,0,8,8,8 },
			{ 0,0,0,1,2,0,0,0,0,0,1,0,0,0,0 },
			{ 0,0,0,0,3,0,0,0,0,3,0,3,0,0,8 },
			{ 8,0,0,0,1,2,0,0,0,0,0,0,0,0,8 },
			{ 8,0,0,8,8,0,8,8,0,8,8,8,0,0,8 },
			{ 8,0,0,8,0,0,0,0,0,0,8,0,0,0,0 },
			{ 8,0,0,8,0,2,0,0,0,2,8,0,0,0,0 },
			{ 8,8,0,8,8,8,8,8,0,8,8,8,8,0,8 },
		};

		for (int y = 0; y < 8; ++y)
			for (int x = 0; x < 15; ++x) {
				mapData.arr[y][x] = w_map[y][x];
			}

		return true;
	}
	void Map_UpData(MapData & md_)
	{
	}
	void Map_Render(MapData & md_)
	{
		for (int y = 0; y < 8; ++y) {
			for (int x = 0; x < 15; ++x) {
				ML::Box2D  draw(0, 0, 32, 32);
				draw.Offset(x * 32, y * 32);
				DG::Image_Draw("MapChipImg", draw, md_.chip[md_.arr[y][x]]);
			}
		}
	}
}