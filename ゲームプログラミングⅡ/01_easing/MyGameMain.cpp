#include "easing.hpp"
#include "MyGameMain.h"

//イージングオブジェクト
ci_ext::Easing easingX;
ci_ext::Easing easingY;

//-----------------------------------------------------------------------------
//初期化処理
//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Initalize( )
{
	DG::Image_Create("TestImg", "./data/image/TestImage01.bmp");
    //イージングオブジェクト
    //種類、初期値、終了オフセット値、秒の順で指定
    easingX.create(ci_ext::EasingName::LINEAR, 0, 400.f, 1.0f);
    easingY.create(ci_ext::EasingName::BOUNCE_OUT, 0, 200.f, 1.0f);
}
//-----------------------------------------------------------------------------
//解放処理
//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
//-----------------------------------------------------------------------------
void  MyGameMain_Finalize( )
{
	DG::Image_Erase("TestImg");
}
//-----------------------------------------------------------------------------
//更新処理
//機能概要：ゲームの１フレームに当たる処理
//-----------------------------------------------------------------------------
void  MyGameMain_UpDate( )
{
    auto m = DI::Mouse_GetState();
    //Rボタンを押したらリプレイ
    if (m.RB.down)
    {
        easingX.back_head();
        easingY.back_head();
    }
    //Lボタンを押したらマウス位置をターゲットにする
    if (m.LB.down)
    {
       easingX.change_target(m.cursorPos.x - easingX.nowf(), 1.0f);//nowf()で現在値をfloat型で取得する
       easingY.change_target(m.cursorPos.y - easingY.nowf(), 1.0f);
    }
}
//-----------------------------------------------------------------------------
//描画処理
//機能概要：ゲームの１フレームに当たる表示処理 ２Ｄ
//-----------------------------------------------------------------------------
void  MyGameMain_Render2D( )
{
	ML::Box2D draw;
	draw.x = easingX.nowi();//nowi()で現在値をint型で取得する
    draw.y = easingY.nowi();
	draw.w = 64*2;
	draw.h = 48*2;

	ML::Box2D src;
	src.x = 100;
	src.y = 0;
	src.w = 64;
	src.h = 48;

	DG::Image_Draw("TestImg", draw, src);

}
