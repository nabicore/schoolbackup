//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Player.h"
#include  "Task_Map2D.h"
#include  "Task_Effect00.h"
#include  "Task_Shot00.h"

namespace  Player
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		this->imageName = "PlayerImg";
		DG::Image_Create(this->imageName, "./data/image/HitTest.bmp");
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName);
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		this->render2D_Priority[1] = 0.5f;
		this->controllerName = "P1";

		this->pos.x = 0;
		this->pos.y = 0;
		this->hitBase = ML::Box2D(-15, -24, 30, 48);
		
		//^XNΜΆ¬

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
		auto in = DI::GPad_GetState(this->controllerName);

		ML::Vec2 est(0, 0);
		if (in.LStick.L.on)	est.x -= 3;
		if (in.LStick.R.on)	est.x += 3;
		if (in.LStick.U.on)	est.y -= 3;
		if (in.LStick.D.on)	est.y += 3;

		if (in.B3.down) {
			ML::Vec2 shotPos[]{
				ML::Vec2(this->pos.x + this->hitBase.w, this->pos.y + 30),
				ML::Vec2(this->pos.x + this->hitBase.w, this->pos.y),
				ML::Vec2(this->pos.x + this->hitBase.w, this->pos.y - 30)
			};

			for (auto p : shotPos) {
				auto shot = Shot00::Object::Create(true);
				shot->pos = p;
				shot->moveVec = ML::Vec2(8, 0);
			}
		}

		this->CheckMove(est);

		this->hitFlag = this->CheckFoot();
		if (this->hitFlag) {
			auto eff = Effect00::Object::Create(true);
			eff->pos = this->pos;
		}


		{
			int px = ge->camera2D.w / 2,
				py = ge->camera2D.h / 2,
				cpx = int(this->pos.x) - px,
				cpy = int(this->pos.y) - py;
			ge->camera2D.x = cpx;
			ge->camera2D.y = cpy;
		}
		if (auto map = ge->GetTask_One_GN<Map2D::Object>("tB[h", "}bv"))
			map->AjastCameraPos();
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
		ML::Box2D draw = this->hitBase.OffsetCopy(this->pos);
		ML::Box2D src(0, 0, 100, 100);
		draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
		DG::Image_Draw(this->res->imageName, draw, src);
	}

	void Object::CheckMove(ML::Vec2& e_) {
		auto map = ge->GetTask_One_GN<Map2D::Object>("tB[h", "}bv");
		if (!map)
			return;

		while (e_.x != 0) {
			float preX = this->pos.x;
			
			if		(e_.x >= 1)			{ this->pos.x += 1;			e_.x -= 1; }
			else if (e_.x <= -1)		{ this->pos.x -= 1;			e_.x += 1; }
			else						{ this->pos.x += e_.x;		e_.x = 0; }

			ML::Box2D hit = this->hitBase.OffsetCopy(this->pos);
			if (map->CheckHit(hit)) {
				this->pos.x = preX;
				break;
			}
		}

		while (e_.y != 0) {
			float preY = this->pos.y;

			if		(e_.y >= 1)			{ this->pos.y += 1;			e_.y -= 1; }
			else if (e_.y <= -1)		{ this->pos.y -= 1;			e_.y += 1; }
			else						{ this->pos.y += e_.y;		e_.y = 0; }

			ML::Box2D hit = this->hitBase.OffsetCopy(this->pos);
			if (map->CheckHit(hit)) {
				this->pos.y = preY;
				break;
			}
		}
	}

	bool Object::CheckFoot() {
		ML::Box2D foot(
			this->hitBase.x,
			this->hitBase.y + this->hitBase.h,
			this->hitBase.w,
			1
		);
		foot.Offset(this->pos);

		auto map = ge->GetTask_One_GN<Map2D::Object>("tB[h", "}bv");
		if (!map)
			return false;

		return map->CheckHit(foot);
	}

	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}