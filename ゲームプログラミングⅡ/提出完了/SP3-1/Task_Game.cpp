#include "MyPG.h"
#include "MyGameMain.h"





//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad  in1;

	struct  MapData {
		int			arr[17][30];
		ML::Box2D	chip[19 * 6];
	};


	enum class Motion : int {
		Non,
		Normal,
		Dead,
		Happy
	};

	enum class Angle_LR : int {
		Left,
		Right
	};

	struct  Chara {
		float		x;
		float		y;
		bool		hitFlag;
		ML::Box2D	hitBase;
		ML::Box2D	footBase;
		float		fallSpeed;
		float		jumpPow;
		ML::Box2D	drawBase;
		ML::Box2D	src;
		Motion		motion;
		Angle_LR	angle;
		int			moveCnt;
		int			animCnt;
		ML::Box2D	headBase;
	};


	void  Map_Initialize(MapData&  md_);
	bool  Map_Load(MapData&  md_, int n_);
	void  Map_UpData(MapData&  md_);
	void  Map_Render(MapData&  md_);
	bool  Map_CheckHit(MapData&  md_, const ML::Box2D&  h_);
	void  Map_CreateSGE(MapData&  md_);//Start Goal Enemy

	void  Player_Initialize(Chara&  c_, int  x_,  int  y_);
	void  Player_UpDate(Chara&  c_);
	void  Player_Render(Chara&  c_);
	void  Player_Anim(Chara& c_);
	void  Chara_CheckMove(Chara&  c_, ML::Vec2&  e_);
	bool  Chara_CheckFoot(Chara&  c_);
	bool  Chara_CheckHead(Chara&  c_);
	void  Goal_Initialize(Chara&  c_, int x_, int y_);
	void  Goal_UpDate(Chara&  c_);
	void  Goal_Render(Chara&  c_);
	void  Start_Initialize(Chara&  c_, int x_, int y_);
	void  Start_Render(Chara&  c_);
	void  Enemy_Render(Chara&  c_);
	void  Enemy_Initialize(Chara&  c_, int x_, int y_);
	void  Enemy_UpDate(Chara& c_);
	void  Shot_Initialize(Chara& c_, int x_, int y_, Angle_LR d_);
	void  Shot_UpDate(Chara& c_);
	void  Shot_Render(Chara& c_);
	void  Shot_Appear(int x_, int y_, Angle_LR d_);

	TaskFlag Check_State();

	int		 playerStock;
	int		 stageNum;
	MapData  mapData;
	Chara    player;
	Chara	 start;
	Chara	 goal;
	Chara    enemys[30 * 17];
	Chara	 shots[1];

	//-------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapSetting_org.png");//マップチップ画像の読み込み
		DG::Image_Create("BackImg", "./data/image/back.png");			 //背景画像の読み込み
		DG::Image_Create("PlayerImg", "./data/image/all.png");	 //プレイヤ画像の読み込み
		DG::Image_Create("StartImg", "./data/image/Start.png");
		DG::Image_Create("GoalImg", "./data/image/Goal.png");
		DG::Image_Create("ShotImg", "./data/image/shot.png");
		DG::Image_Create("Enemy", "./data/image/Enemy.png");



		for (int i = 0; i < _countof(enemys); ++i)
			enemys[i].motion = Motion::Non;

		for (int i = 0; i < _countof(shots); ++i)
			shots[i].motion = Motion::Non;

		//マップの初期化
		Map_Initialize(mapData);
		//マップの読み込み
		Map_Load(mapData, stageNum);
		//マップからスタート・ゴール・罠敵を生成する
		Map_CreateSGE(mapData);

		//プレイヤの初期化
		Player_Initialize(player, int(start.x), int(start.y));
	}
	//-------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
		DG::Image_Erase("BackImg");
		DG::Image_Erase("PlayerImg");
		DG::Image_Erase("GoalImg");
		DG::Image_Erase("Enemy");
		DG::Image_Erase("StartImg");
	}
	//-------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");
		Map_UpData(mapData);

		//プレイヤの行動
		Player_UpDate(player);
		Goal_UpDate(goal);
		for (auto& e : enemys)
			Enemy_UpDate(e);
		for (auto& b : shots)
			Shot_UpDate(b);

		TaskFlag rtv = Check_State();
		return rtv;
	}
	//-------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-------------------------------------------------------------------------
	void Render()
	{
		Map_Render(mapData);
		//プレイヤの表示
		Player_Render(player);
		Start_Render(start);
		Goal_Render(goal);

		for (auto& e : enemys)
			Enemy_Render(e);
		for (auto& s : shots)
			Shot_Render(s);
	}

	//-------------------------------------------------------------------------
	//ゲームの進行状態を調べる
	TaskFlag Check_State()
	{
		//リセット
		if (in1.ST.down) {
			return  Task_Title;
		}

		static int tick = 0;
		if (player.motion == Motion::Dead) {
			tick += 1;
			if (tick >= 120) {
				tick = 0;
				playerStock -= 1;
				if (playerStock > 0)
					return Task_StageLogo;
				return Task_Title;
			}
		}

		if (player.motion == Motion::Happy) {
			tick += 1;
			if (tick >= 120) {
				tick = 0;
				if (stageNum <= 10) {
					stageNum += 1;
					return Task_StageLogo;
				}
				return Task_Ending;
			}
		}

		//ステージを継続する
		return Task_Game;
	}
	//-------------------------------------------------------------------------
	void  Map_Initialize(MapData&  md_)
	{
		//マップのゼロクリア
		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				md_.arr[y][x] = 0;
			}
		}
		//マップチップ情報の初期化
		for (int c = 0; c < 19 * 6; ++c) {
			md_.chip[c] = ML::Box2D((c % 19) * 32, (c / 18) * 32, 32, 32);
		}
	}
	//-------------------------------------------------------------------------
	bool  Map_Load(MapData&  md_, int n_)
	{
		//ファイルパスを作る
		string  filePath = "./data/map/mapdata" + to_string(n_) + ".csv";
		//ファイルを開く（読み込み）
		ifstream   fin(filePath);
		if (!fin) { return  false; }//読み込み失敗

		//配列にデータを取り込む  //CSV対応
		for (int y = 0; y < 17; ++y) {
			string lineText;
			getline(fin, lineText);
			istringstream  ss_lt(lineText);
			for (int x = 0; x < 30; ++x) {
				string  tc;
				getline(ss_lt, tc, ',');
				stringstream ss;
				ss << tc;
				ss >> md_.arr[y][x];
			}
		}
		//ファイルを閉じる
		fin.close();
		return  true;
	}
	//-----------------------------------------------------------------------------
	//スタート・ゴール・罠敵などを生成する
	void  Map_CreateSGE(MapData&  md_)
	{
		int eneCnt = 0;
		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				if (md_.arr[y][x] == 39) {
					Goal_Initialize(goal, x * 32 + 16, y * 32 + 16);
					md_.arr[y][x] = -1;
				}
				if (md_.arr[y][x] == 38) {
					Start_Initialize(start, x * 32 + 16, y * 32 + 16);
					md_.arr[y][x] = -1;
				}
				if (md_.arr[y][x] == 19) {
					Enemy_Initialize(enemys[eneCnt], x * 32 + 16, y * 32 + 16);
					md_.arr[y][x] = -1;
					eneCnt += 1;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	void  Map_UpData(MapData&  md_)
	{
		//今のところ処理なし（ＳＰ課題で使用）
	}
	//-------------------------------------------------------------------------
	void  Map_Render(MapData&  md_)
	{
		ML::Box2D  draw(0, 0, 960, 540);
		ML::Box2D  src(0, 0, 1280, 720);
		DG::Image_Draw("BackImg", draw, src);

		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				if (md_.arr[y][x] != -1) {
					ML::Box2D  draw(x * 32, y * 32, 32, 32);
					DG::Image_Draw("MapChipImg", draw, md_.chip[md_.arr[y][x]]);
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	bool  Map_CheckHit(MapData&  md_, const ML::Box2D&  h_)
	{
		RECT  r = { h_.x, h_.y, h_.x + h_.w, h_.y + h_.h };
		//矩形がマップ外に出ていたら丸め込みを行う
		RECT  m = { 0, 0, 32 * 30, 32 * 17 };
		if (r.left   < m.left) {	r.left   = m.left; }
		if (r.top    < m.top) {		r.top    = m.top; }
		if (r.right  > m.right) {	r.right  = m.right; }
		if (r.bottom > m.bottom) {	r.bottom = m.bottom; }

		//ループ範囲調整
		int sx, sy, ex, ey;
		sx = r.left / 32;
		sy = r.top  / 32;
		ex = (r.right  - 1) / 32;
		ey = (r.bottom - 1) / 32;

		//範囲内の障害物を探す
		for (int y = sy; y <= ey; ++y) {
			for (int x = sx; x <= ex; ++x) {
				if (8 <= md_.arr[y][x]) {
					return true;
				}
			}
		}
		return false;
	}
	//-------------------------------------------------------------------------
	void  Player_Initialize(Chara&  c_, int  x_, int  y_)
	{
		c_.x = float(x_);
		c_.y = float(y_);
		c_.hitFlag = false;
		c_.hitBase = ML::Box2D(-16, -24, 32, 48);
		c_.footBase = ML::Box2D(c_.hitBase.x, c_.hitBase.h + c_.hitBase.y, c_.hitBase.w, 1);
		c_.fallSpeed = 0.0f;
		c_.jumpPow = -7.2f;
		c_.drawBase = ML::Box2D(0, 0, 0, 0);
		c_.src = ML::Box2D(0, 0, 0, 0);
		c_.motion = Motion::Normal;
		c_.angle = Angle_LR::Right;
		c_.moveCnt = 0;
		c_.animCnt = 0;
		c_.headBase = ML::Box2D(c_.hitBase.x, c_.hitBase.y, c_.hitBase.w, 1);
	}
	//-------------------------------------------------------------------------
	void  Player_UpDate(Chara&  c_)
	{
		if (c_.motion == Motion::Non) return;
		static int tick = 0;

		ML::Vec2  est(0, 0);

		switch (c_.motion) {
		case Motion::Normal:
			if (in1.LStick.L.on) { est.x -= 3.0f; c_.angle = Angle_LR::Left;  }
			if (in1.LStick.R.on) { est.x += 3.0f; c_.angle = Angle_LR::Right; }
			if (in1.B1.down) {
				if (true == c_.hitFlag) {//着地中のみジャンプ開始できる
					c_.fallSpeed = c_.jumpPow;
				}
			}
			if (in1.B2.down) {
				Shot_Appear(int(c_.x), int(c_.y), c_.angle);
			}
			break;

		case Motion::Dead:
			break;

		case Motion::Happy:
			break;
		}

		est.y += c_.fallSpeed;
		Chara_CheckMove(c_, est);

		//足元接触判定
		c_.hitFlag = Chara_CheckFoot(c_);
		if (true == c_.hitFlag) {
			c_.fallSpeed = 0.0f;//落下速度0
		}
		else {
			c_.fallSpeed += ML::Gravity(32) * 3;//重力加速
		}

		//頭上判定
		if (c_.fallSpeed < 0 && Chara_CheckHead(c_))
			c_.fallSpeed = 0;


		c_.moveCnt += 1;
		c_.animCnt += 1;
	}
	//-------------------------------------------------------------------------
	void  Player_Render(Chara&  c_)
	{
		if (c_.motion == Motion::Non) return;
		Player_Anim(c_);
		ML::Box2D  draw = c_.drawBase.OffsetCopy( int(c_.x), int(c_.y));
		DG::Image_Draw("PlayerImg", draw, c_.src);
	}
	//-------------------------------------------------------------------------
	void Player_Anim(Chara & c_)
	{
		switch (c_.motion) {
		case Motion::Normal:
			c_.drawBase = ML::Box2D(-16, -40, 32, 64);
			c_.src = ML::Box2D(32 * ((c_.animCnt / 8) % 6), 0, 32, 64);
			break;

		case Motion::Dead:		//	---------------------------仮
			c_.drawBase = ML::Box2D(-32, -8, 64, 32);
			c_.src = ML::Box2D(192, 96, 64, 32);
			break;

		case Motion::Happy:		//	---------------------------仮
			c_.drawBase = ML::Box2D(-16, -40, 32, 64);
			c_.src = ML::Box2D(32 * ((c_.animCnt / 16) % 2), 128, 32, 64);
			break;
		}

		if (c_.angle == Angle_LR::Right && c_.drawBase.w >= 0) {
			c_.drawBase.x = -c_.drawBase.x;
			c_.drawBase.w = -c_.drawBase.w;
		}
	}
	//-------------------------------------------------------------------------
	void  Chara_CheckMove(Chara&  c_, ML::Vec2&  e_)
	{
		//横軸に対する移動
		while (e_.x != 0) {
			float  preX = c_.x;
			if (     e_.x >=  1) {	c_.x += 1;		e_.x -= 1; }
			else if (e_.x <= -1) {  c_.x -= 1;		e_.x += 1; }
			else {					c_.x += e_.x;	e_.x = 0; }
			ML::Box2D  hit = c_.hitBase.OffsetCopy( int(c_.x), int(c_.y));
			if (true == Map_CheckHit(mapData, hit)) {
				c_.x = preX;		//移動をキャンセル
				break;
			}
		}
		//縦軸に対する移動
		while (e_.y != 0) {
			float  preY = c_.y;
			if (     e_.y >=  1) {	c_.y += 1;		e_.y -= 1; }
			else if (e_.y <= -1) {	c_.y -= 1;		e_.y += 1; }
			else {					c_.y += e_.y;	e_.y = 0; }
			ML::Box2D  hit = c_.hitBase.OffsetCopy(int(c_.x), int(c_.y));
			if (true == Map_CheckHit(mapData, hit)) {
				c_.y = preY;		//移動をキャンセル
				break;
			}
		}
	}
	bool  Chara_CheckFoot(Chara&  c_)
	{
		ML::Box2D  hit = c_.footBase.OffsetCopy( int(c_.x), int(c_.y));
		return  Map_CheckHit(mapData, hit);
	}
	bool Chara_CheckHead(Chara & c_)
	{
		ML::Box2D  hit = c_.headBase.OffsetCopy(int(c_.x), int(c_.y));
		return  Map_CheckHit(mapData, hit);
	}
	void Goal_Initialize(Chara & c_, int x_, int y_)
	{
		c_.x = float(x_);
		c_.y = float(y_);
		c_.hitFlag = false;
		c_.hitBase = ML::Box2D(-1, 14, 2, 2);
		c_.footBase = ML::Box2D(0, 0, 0, 0);
		c_.headBase = ML::Box2D(0, 0, 0, 0);
		c_.fallSpeed = 0.0f;
		c_.jumpPow = 0.0f;
		c_.drawBase = ML::Box2D(-16, -16, 32, 32);
		c_.src = ML::Box2D(0, 0, 32, 32);
		c_.motion = Motion::Normal;
		c_.angle = Angle_LR::Right;
		c_.moveCnt = 0;
		c_.animCnt = 0;
	}
	void Goal_UpDate(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;
		ML::Box2D me = c_.hitBase.OffsetCopy(int(c_.x), int(c_.y));
		if (player.motion == Motion::Normal) {
			ML::Box2D you = player.hitBase.OffsetCopy(int(player.x), int(player.y));
			if (you.Hit(me)) {
				player.motion = Motion::Happy;
				player.moveCnt = 0;
				player.animCnt = 0;
			}
		}
	}
	void Goal_Render(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;
		ML::Box2D draw = c_.drawBase.OffsetCopy(int(c_.x), int(c_.y));
		DG::Image_Draw("GoalImg", draw, c_.src);
	}
	void Start_Initialize(Chara & c_, int x_, int y_)
	{
		c_.x = float(x_);
		c_.y = float(y_);
		c_.hitFlag = false;
		c_.hitBase = ML::Box2D(-1, 14, 2, 2);
		c_.footBase = ML::Box2D(0, 0, 0, 0);
		c_.headBase = ML::Box2D(0, 0, 0, 0);
		c_.fallSpeed = 0.0f;
		c_.jumpPow = 0.0f;
		c_.drawBase = ML::Box2D(-16, -16, 32, 32);
		c_.src = ML::Box2D(0, 0, 32, 32);
		c_.motion = Motion::Normal;
		c_.angle = Angle_LR::Right;
		c_.moveCnt = 0;
		c_.animCnt = 0;
	}
	void Start_Render(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;
		ML::Box2D draw = c_.drawBase.OffsetCopy(int(c_.x), int(c_.y));
		DG::Image_Draw("StartImg", draw, c_.src);
	}
	void Enemy_Render(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;
		if (c_.motion == Motion::Dead) return;
		ML::Box2D draw = c_.drawBase.OffsetCopy(int(c_.x), int(c_.y));
		DG::Image_Draw("Enemy", draw, c_.src);
	}
	void Enemy_Initialize(Chara & c_, int x_, int y_)
	{
		Start_Initialize(c_, x_, y_);
		c_.hitBase = ML::Box2D(-16, -16, 32, 32);
		c_.drawBase = ML::Box2D(-16, -16, 32, 32);
	}
	void Enemy_UpDate(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;
		if (c_.motion == Motion::Normal) {
			if (c_.hitBase.OffsetCopy(ML::Vec2(c_.x, c_.y)).Hit(player.hitBase.OffsetCopy(ML::Vec2(player.x, player.y)))) {
				player.motion = Motion::Dead;
				player.moveCnt = 0;
			}
		}

		if (c_.motion == Motion::Dead) {
			c_.moveCnt += 1;
			if (c_.moveCnt >= 0) {
				c_.motion = Motion::Normal;
				int x = (int(c_.x) - 16) / 32;
				int y = (int(c_.y) - 16) / 32;
				mapData.arr[y][x] = -1;
			}
		}
	}
	void Shot_Initialize(Chara & c_, int x_, int y_, Angle_LR d_)
	{
		Start_Initialize(c_, x_, y_);
		c_.hitBase = ML::Box2D(-16, -16, 32, 32);
		c_.fallSpeed = -3.6f;
		c_.angle = d_;
	}
	void Shot_UpDate(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;

		if (c_.motion == Motion::Normal) {

			// 移動
			ML::Vec2 est(0, 0);
			if (c_.angle == Angle_LR::Left) est.x -= 4.5f;
			else							est.x += 4.5f;
			est.y = c_.fallSpeed;

			c_.x += est.x;
			c_.y += est.y;

			auto me = c_.hitBase.OffsetCopy(int(c_.x), int(c_.y));
			if (Map_CheckHit(mapData, me))
				c_.motion = Motion::Non;

			c_.fallSpeed += ML::Gravity(32) * 3;

			// 敵とのあたり判定
			for (int i = 0; i < _countof(enemys); ++i) {
				if (enemys[i].motion != Motion::Non) {
					auto you = enemys[i].hitBase.OffsetCopy(int(enemys[i].x), int(enemys[i].y));
					if (you.Hit(me)) {
						c_.motion = Motion::Non;
						int x = (int(enemys[i].x) - 16) / 32;
						int y = (int(enemys[i].y) - 16) / 32;
						mapData.arr[y][x] = 20;
						enemys[i].motion = Motion::Dead;
						enemys[i].moveCnt = -120;
					}
				}
			}
		}
	}
	void Shot_Render(Chara & c_)
	{
		if (c_.motion == Motion::Non) return;
		DG::Image_Draw("ShotImg", c_.drawBase.OffsetCopy(int(c_.x), int(c_.y)), c_.src);
	}
	void Shot_Appear(int x_, int y_, Angle_LR d_)
	{
		for (int i = 0; i < _countof(shots); ++i)
			if (shots[i].motion == Motion::Non)
				Shot_Initialize(shots[i], x_, y_, d_);
	}
}