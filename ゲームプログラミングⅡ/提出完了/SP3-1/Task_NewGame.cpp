#include "MyPG.h"
#include "MyGameMain.h"




namespace Game {
	extern int		playerStock;
	extern int		stageNum;
}

namespace NewGame {
	void Initialize() {
		Game::playerStock = 3;
		Game::stageNum = 1;
	}

	void Finalize() {

	}

	TaskFlag UpDate() {
		return Task_StageLogo;
	}

	void Render() {

	}
}