#include "MyPG.h"
#include "MyGameMain.h"

//タイトル画面
namespace Title
{
	//ゲーム情報
	int					logoPosY;

	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("TitleBGImg", "./data/image/titleback.jpg");
		DG::Image_Create("TitleLogoImg", "./data/image/gamelogo.png");
		logoPosY = -100; //タイトル画像の初期位置を画面外に配置する
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("TitleBGImg");
		DG::Image_Erase("TitleLogoImg");
	}
	//-----------------------------------------------------------------------------
	//更新処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		DI::VGamePad  in1 = DI::GPad_GetState("P1");
		logoPosY += 18;
		if (logoPosY  >  200) { logoPosY = 200; }

		TaskFlag rtv = Task_Title;//取りあえず現在のタスクを指定
		if (logoPosY >= 200) {
			if (true == in1.ST.down) {
				rtv = Task_NewGame;	//次のタスクをゲーム本編へ
			}
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		ML::Box2D  draw(0, 0, 960, 540);
		ML::Box2D  src(0, 0, 960, 540);
		DG::Image_Draw("TitleBGImg", draw, src);

		ML::Box2D  logodraw(200, 0, 470, 72);
		logodraw.Offset(0, logoPosY);
		ML::Box2D  logosrc(0, 0, 470, 72);
		DG::Image_Draw("TitleLogoImg", logodraw, logosrc);
	}
}