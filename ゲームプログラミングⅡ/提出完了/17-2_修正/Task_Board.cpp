//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Board.h"

namespace  Board
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//リソースの初期化
	bool  Resource::Initialize()
	{
		this->imageName_Chip = "ChipImg";
		DG::Image_Create(this->imageName_Chip, "./data/image/Chip.bmp");

		this->imageName_Cap = "CapImg";
		DG::Image_Create(this->imageName_Cap, "./data/image/Cap.bmp");

		return true;
	}
	//-------------------------------------------------------------------
	//リソースの解放
	bool  Resource::Finalize()
	{
		DG::Image_Erase(this->imageName_Chip);
		return true;
	}
	//-------------------------------------------------------------------
	//「初期化」タスク生成時に１回だけ行う処理
	bool  Object::Initialize()
	{
		//スーパークラス初期化
		__super::Initialize(defGroupName, defName, true);
		//リソースクラス生成orリソース共有
		this->res = Resource::Create();

		//★データ初期化
		this->render2D_Priority[1] = 0.8f;
		this->sizeX = 0;
		this->sizeY = 0;

		for (int c = 0; c < 10; ++c)
			this->chip[c] = ML::Box2D(c * 16, 0, 16, 16);
		ZeroMemory(this->arr, sizeof(this->arr));

		for (int c = 0; c < 4; ++c)
			this->capChip[c] = ML::Box2D(c * 16, 0, 16, 16);
		ZeroMemory(this->cap, sizeof(this->cap));
		
		//★タスクの生成

		return  true;
	}
	//-------------------------------------------------------------------
	//「終了」タスク消滅時に１回だけ行う処理
	bool  Object::Finalize()
	{
		//★データ＆タスク解放


		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//★引き継ぎタスクの生成
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//「更新」１フレーム毎に行う処理
	void  Object::UpDate()
	{
		auto mouse = DI::Mouse_GetState();
		if (mouse.RB.down) {
			POINT pos = mouse.cursorPos;
			if (pos.x >= 0 && pos.x < 16 * this->sizeX &&
				pos.y >= 0 && pos.y < 16 * this->sizeY) {
				POINT masu = { pos.x / 16, pos.y / 16 };
				this->cap[masu.y][masu.x] += 1;
				if (this->cap[masu.y][masu.x] > 3)
					this->cap[masu.y][masu.x] = 1;
			}
		}
	}
	//-------------------------------------------------------------------
	//「２Ｄ描画」１フレーム毎に行う処理
	void  Object::Render2D_AF()
	{
		for (int y = 0; y < this->sizeY; ++y) 
			for (int x = 0; x < this->sizeX; ++x) {
				ML::Box2D draw(x * 16, y * 16, 16, 16);
				DG::Image_Draw(this->res->imageName_Chip, draw, this->chip[this->arr[y][x]]);
			}

		for (int y = 0; y < this->sizeY; ++y)
			for (int x = 0; x < this->sizeX; ++x) 
				if (this->cap[y][x] != 0) {
					ML::Box2D draw(x * 16, y * 16, 16, 16);
					DG::Image_Draw(this->res->imageName_Cap, draw, this->capChip[this->cap[y][x]]);
				}
	}



	void Object::Set(int sx_, int sy_, int mine_) {
		this->sizeX = sx_;
		this->sizeY = sy_;

		for (int y = 0; y < this->sizeY; ++y)
			for (int x = 0; x < this->sizeX; ++x)
				this->arr[y][x] = 0;

		// キャップ配列を初期化
		for (int y = 0; y < this->sizeY; ++y)
			for (int x = 0; x < this->sizeX; ++x)
				this->cap[y][x] = 1;

		// 仮
		int tmpY = 0, tmpX = 0, cnt = 0;
		while (cnt <= mine_) {
			this->arr[tmpY][tmpX] = 9;
			if (tmpX >= this->sizeX) {
				tmpX = 0;
				tmpY += 1;
			}
			else
				tmpX += 1;
			cnt += 1;
		}

		// シャッフルして地雷をばらけさせる
		for (int y = 0; y < this->sizeY; ++y)
			for (int x = 0; x < this->sizeX; ++x) {
				int xr = rand() % this->sizeX;
				int yr = rand() % this->sizeY;

				int tmp = this->arr[y][x];
				this->arr[y][x] = this->arr[yr][xr];
				this->arr[yr][xr] = tmp;
			}

		for (int y = 0; y < this->sizeY; ++y)
			for (int x = 0; x < this->sizeX; ++x)
				if (this->arr[y][x] == 0)
					this->arr[y][x] = this->CntMine(x, y);
	}

	int Object::CntMine(int x_, int y_) {
		POINT m[8]{
			{ -1, -1 },	{ +0, +1 },	{ +1, -1 },
			{ -1, +0 },				{ +1, +0 },
			{ -1, +1 },	{ +0, +1 },	{ +1, +1 }
		};
		for (auto& n : m) {
			n.x += x_;
			n.y += y_;
		}

		int cnt = 0;
		for (auto& n : m) {
			if (n.x < 0 && n.y < 0 &&
				n.x >= this->sizeX &&
				n.y >= this->sizeY)
				continue;

			if (this->arr[n.y][n.x] == 9)
				cnt += 1;
		}

		return cnt;
	}
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//以下は基本的に変更不要なメソッド
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//-------------------------------------------------------------------
	//タスク生成窓口
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//ゲームエンジンに登録
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//イニシャライズに失敗したらKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//リソースクラスの生成
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}