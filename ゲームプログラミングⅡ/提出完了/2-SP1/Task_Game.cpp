#include <vector>
#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad  in1;

	struct MapData {
		int					arr[8][15];
		ML::Box2D			chip[32];
	};
	std::vector<ML::Vec2> yukaCoord;
	MapData mapData;
	int tick = 0;


	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");	//背景

		int w_map[8][15] = {
			{ 8,8,8,8,8,8,16,8,8,8,8,0,8,8,8 },
			{ 0,0,0,1,2,0,16,0,0,0,1,0,0,0,0 },
			{ 0,0,0,0,3,0,16,0,0,3,0,3,0,0,8 },
			{ 8,0,0,0,1,2,17,17,0,0,0,0,0,0,8 },
			{ 8,0,0,8,8,0,8,16,0,8,8,8,0,0,8 },
			{ 8,0,0,8,0,0,0,0,0,0,8,0,0,0,0 },
			{ 8,0,0,8,0,2,0,16,0,2,8,0,0,0,0 },
			{ 8,8,0,8,8,8,8,16,8,8,8,8,8,0,8 },
		};

		for (int y = 0; y < 8; ++y) 
			for (int x = 0; x < 15; ++x) {
				mapData.arr[y][x] = w_map[y][x];
				if (w_map[y][x] == 1)
					yukaCoord.push_back(ML::Vec2(x, y));
			}

		for (int c = 0; c < 32; ++c) {
			int x = (c % 8);
			int y = (c / 8);
			mapData.chip[c] = ML::Box2D(x * 32, y * 32, 32, 32);
		}
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		tick += 1;
		if (tick > 30) {
			for (auto& yuka : yukaCoord) {
				auto& y = mapData.arr[(int)yuka.y][(int)yuka.x];
				switch (y) {
				case 0: y = 1; break;
				case 1: y = 0; break;
				}
			}
			tick = 0;
		}

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		ML::Box2D  src(0, 32, 32, 32);
		for (int y = 0; y < 8; ++y) {
			for (int x = 0; x < 15; ++x) {
				ML::Box2D  draw(0, 0, 32, 32);
				draw.Offset(x * 32, y * 32);

				int num = mapData.arr[y][x];
				ML::Box2D src = mapData.chip[num];
				DG::Image_Draw("MapChipImg", draw, src);
			}
		}
	}
}