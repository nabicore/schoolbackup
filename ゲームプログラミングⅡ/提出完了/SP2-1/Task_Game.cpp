#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad	in1;
	DI::Mouse	  mouse;

	struct  MapData {
		int			arr[3][3];
		ML::Box2D	chip[3];
	};
	void  Map_Initialize(MapData&  md_);
	bool  Map_ChangeChip(MapData&  md_, POINT&  p_, int stone_);
	void  Map_Render(MapData&  md_);

	enum class GameState : int {
		Normal,
		MaruWin, 
		BatuWin,
		Draw
	};
	void  GameState_Render();
	GameState Map_CheckGameState(MapData& md_);

	GameState gameState;
	MapData  mapData;
	POINT	 offset;
	int		 turn;

	//-------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MaruBatuImg", "./data/image/MaruBatu.bmp");
		DG::Font_Create("FontA", "HG丸ｺﾞｼｯｸM-PRO", 8, 16);

		//マップの初期化
		offset.x = (ge->screenWidth - 64 * 3) / 2;
		offset.y = (ge->screenHeight- 64 * 3) / 2;
		Map_Initialize(mapData);

		turn = 1;
		gameState = GameState::Normal;
	}
	//-------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MaruBatuImg");
	}
	//-------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");
		mouse = DI::Mouse_GetState();

		if (mouse.LB.down && gameState == GameState::Normal) {
			POINT mp = mouse.cursorPos;
			RECT sb = { offset.x, offset.y, offset.x + (64 * 3), offset.y + (64 * 3) };
			if (mp.x >= sb.left && mp.x < sb.right &&
				mp.y >= sb.top  && mp.y < sb.bottom) {
				POINT mp2 = { mp.x - sb.left, mp.y - sb.top };
				POINT mp3 = { mp2.x / 64, mp2.y / 64 };
				Map_ChangeChip(mapData, mp3, turn);
				turn = ((turn + 2) % 2) + 1;
				gameState = Map_CheckGameState(mapData);
			}
		}


		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-------------------------------------------------------------------------
	void Render()
	{
		Map_Render(mapData);
		GameState_Render();
	}

	//-------------------------------------------------------------------------
	void  Map_Initialize(MapData&  md_)
	{
		//マップのゼロクリア
		for (int y = 0; y < 3; ++y) {
			for (int x = 0; x < 3; ++x) {
				md_.arr[y][x] = 0;
			}
		}
		//マップチップ情報の初期化
		for (int c = 0; c < 3; ++c) {
			md_.chip[c] = ML::Box2D(c * 64, 0, 64, 64);
		}
	}
	bool Map_ChangeChip(MapData & md_, POINT & p_, int stone_)
	{
		if (p_.x < 0 || p_.y < 0 ||
			p_.x >= 3 || p_.y >= 3 ||
			md_.arr[p_.y][p_.x] != 0)
			return false;
		md_.arr[p_.y][p_.x] = stone_;
		return true;
	}
	void  Map_Render(MapData&  md_)
	{
		for (int y = 0; y < 3; ++y) {
			for (int x = 0; x < 3; ++x) {
				ML::Box2D  draw(0, 0, 64, 64);
				draw.Offset(x * 64, y * 64);	//表示位置を調整
				draw.Offset(offset.x, offset.y);
				DG::Image_Draw("MaruBatuImg", draw, md_.chip[ md_.arr[y][x] ]);
			}
		}
	}
	GameState Map_CheckGameState(MapData & md_)
	{
		int(*p)[3] = md_.arr;


		for (int player = 1; player <= 2; ++player) {
			int vertical = 0;
			int horizontal = 0;
			int leftCount = 0;
			for (int j = 0; j < 3; ++j) {
				for (int i = 0; i < 3; ++i) {
					if (p[i][j] == player)
						horizontal += 1;
					if (p[j][i] == player)
						vertical += 1;
					if (p[i][j] == 0)
						leftCount += 1;
				}

				if (vertical >= 3 || horizontal >= 3) {
					if (player == 1) return GameState::MaruWin;
					else			 return GameState::BatuWin;
				}
				OutputDebugString(("Horizontal = " + std::to_string(horizontal) + "\n").c_str());
				OutputDebugString(("Vertical = " + std::to_string(vertical) + "\n").c_str());

				vertical = 0;
				horizontal = 0;
			}

			int digonal = 0, digonal_r = 0;
			for (int player = 1; player <= 2; ++player) {
				for (int i = 0; i < 3; ++i) {
					if (p[i][i] == player)
						digonal += 1;
					if (p[2 - i][i] == player)
						digonal_r += 1;
				}

				if (digonal >= 3 || digonal_r >= 3) {
					if (player == 1) return GameState::MaruWin;
					else			 return GameState::BatuWin;
				}

				OutputDebugString(("Digonal = " + std::to_string(digonal) + "\n").c_str());
				OutputDebugString(("Digonal_R = " + std::to_string(digonal_r) + "\n").c_str());

				digonal = 0;
				digonal_r = 0;
			}

			OutputDebugString("\n\n");

			if (leftCount <= 0)
				return GameState::Draw;
		}
		
		return GameState::Normal;
	}
	//-------------------------------------------------------------------------

	void GameState_Render() {
		ML::Box2D textBox(0, 0, 480, 32);
		string text = "";

		switch (gameState) {
		case GameState::Normal:
			if (turn == 1) text = "oの番です";
			else	  text = "xの番です";
			break;

		case GameState::MaruWin:  text = "○のかち";		break;
		case GameState::BatuWin:  text = "×のかち";		break;
		case GameState::Draw:	  text = "引き分け";		break;
		}
		DG::Font_Draw("FontA", textBox, text, ML::Color(1, 1, 0, 1));
	}
}