#include <vector>
#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	class Coord {
	public:
		int x;
		int y;

		inline bool Equals(const Coord& c_) {
			return c_.x == this->x && c_.y == this->y;
		}

		Coord(int x, int y) {
			this->x = x;
			this->y = y;
		}

		Coord(const Coord& c_) {
			this->x = c_.x;
			this->y = c_.y;
		}

		Coord(ML::Vec2 v_) {
			this->x = (int)v_.x;
			this->y = (int)v_.y;
		}

		bool isOutOfRange(const ML::Box2D b_) {
			return
				this->x > b_.x &&
				this->x < b_.x + b_.w &&
				this->y > b_.y &&
				this->y < b_.y + b_.h;
		}

		Coord& operator+(const Coord& c_) {
			this->x += c_.x;
			this->y += c_.y;
			return *this;
		}

		bool operator==(const Coord& c_) {
			return Equals(c_);
		}
	};

	//ゲーム情報
	DI::VGamePad  in1;

	struct MapData {
		int					arr[8][15];
		ML::Box2D			chip[32];
	};

	struct Chara {
		int					x;
		int					y;
	};

	MapData mapData;
	Chara player;


	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");	//背景
		DG::Image_Create("PlayerImg", "./data/image/Chara01.png");	//背景

		int w_map[8][15] = {
			{ 8,8,8,8,8,8,8,8,8,8,8,0,8,8,8 },
			{ 0,0,0,1,2,0,8,0,0,0,1,0,0,0,0 },
			{ 0,0,0,0,3,0,8,0,0,3,0,3,0,0,8 },
			{ 8,0,0,0,1,2,8,0,0,0,0,0,0,0,8 },
			{ 8,0,0,8,8,0,8,8,0,8,8,8,0,0,8 },
			{ 8,0,0,8,0,0,0,0,0,0,8,0,0,0,0 },
			{ 8,0,0,8,0,2,0,0,0,2,8,0,0,0,0 },
			{ 8,8,0,8,8,8,8,8,8,8,8,8,8,0,8 },
		};

		for (int y = 0; y < 8; ++y) 
			for (int x = 0; x < 15; ++x) {
				mapData.arr[y][x] = w_map[y][x];
			}

		for (int c = 0; c < 32; ++c) {
			int x = (c % 8);
			int y = (c / 8);
			mapData.chip[c] = ML::Box2D(x * 32, y * 32, 32, 32);
		}


		player.x = 5;
		player.y = 3;
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
		DG::Image_Erase("PlayerImg");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		if (in1.LStick.L.down)	player.x -= 1;
		if (in1.LStick.R.down)	player.x += 1;
		if (in1.LStick.U.down)	player.y -= 1;
		if (in1.LStick.D.down)	player.y += 1;

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		ML::Box2D  src(0, 32, 32, 32);
		for (int y = 0; y < 8; ++y) {
			for (int x = 0; x < 15; ++x) {
				ML::Box2D  draw(0, 0, 32, 32);
				draw.Offset(x * 32, y * 32);

				int num = mapData.arr[y][x];
				ML::Box2D src = mapData.chip[num];
				DG::Image_Draw("MapChipImg", draw, src);
			}
		}

		ML::Box2D draw(0, 0, 32, 32);
		draw.Offset(player.x * 32, player.y * 32 - 12);
		src = ML::Box2D(0, 0, 32, 32);
		DG::Image_Draw("PlayerImg", draw, src);
	}
}