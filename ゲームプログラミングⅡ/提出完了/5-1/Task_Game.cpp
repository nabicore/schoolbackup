#include <vector>
#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	class Coord {
	public:
		int x;
		int y;

		inline bool Equals(const Coord& c_) {
			return c_.x == this->x && c_.y == this->y;
		}

		Coord(int x, int y) {
			this->x = x;
			this->y = y;
		}

		Coord(POINT p) {
			Coord(p.x, p.y);
		}

		Coord(const Coord& c_) {
			Coord(c_.x, c_.y);
		}

		Coord(ML::Vec2 v_) {
			Coord((int)v_.x, (int)v_.y);
		}

		bool isOutOfRange(const ML::Box2D b_) {
			return
				this->x >= b_.x &&
				this->x < b_.x + b_.w &&
				this->y >= b_.y &&
				this->y < b_.y + b_.h;
		}

		inline POINT ToPoint() {
			POINT ret;
			ret.x = this->x;
			ret.y = this->y;
			return ret;
		}

		Coord& operator+(const Coord& c_) {
			this->x += c_.x;
			this->y += c_.y;
			return *this;
		}

		bool operator==(const Coord& c_) {
			return Equals(c_);
		}
	};

	//ゲーム情報
	DI::VGamePad  in1;

	struct MapData {
		int					arr[8][15];
		ML::Box2D			chip[32];
	};
	void Map_Initialize(MapData& md_);
	bool Map_Load(MapData& md_, int n_);
	void Map_Update(MapData& md_);
	void Map_Render(MapData& md_);
	bool Map_CheckHit(MapData& md_, Coord p_);

	struct Chara {
		int					x;
		int					y;
		int					angle;
	};
	void Player_Initialize(Chara& c_, int x_, int y_, int a_);
	void Player_Update(Chara& c_);
	void Player_Render(Chara& c_);
	ML::Box2D playerImage[4]{
		ML::Box2D(0, 0, 32, 32),
		ML::Box2D(32, 0, 32, 32),
		ML::Box2D(64, 0, 32, 32),
		ML::Box2D(96, 0, 32, 32)
	};

	MapData mapData;
	Chara player;


	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");	//背景
		DG::Image_Create("PlayerImg", "./data/image/Chara01.png");	//背景

		
		Map_Initialize(mapData);
		Map_Load(mapData, 0);
		Player_Initialize(player, 5, 3, 0);
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
		DG::Image_Erase("PlayerImg");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");


		Map_Update(mapData);
		Player_Update(player);


		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		Map_Render(mapData);
		Player_Render(player);
	}




	// ====> MapData
	// ================================================================
	void Map_Initialize(MapData & md_)
	{
		for (int y = 0; y < 8; ++y)
			for (int x = 0; x < 15; ++x) {
				md_.arr[y][x] = 0;
			}

		for (int c = 0; c < 32; ++c) {
			int x = (c % 8);
			int y = (c / 8);
			md_.chip[c] = ML::Box2D(x * 32, y * 32, 32, 32);
		}
	}

	bool Map_Load(MapData & md_, int n_)
	{
		int w_map[8][15] = {
			{ 8,8,8,8,8,8,8,8,8,8,8,0,8,8,8 },
			{ 0,0,0,1,2,0,8,0,0,0,1,0,0,0,0 },
			{ 0,0,0,0,3,0,8,0,0,3,0,3,0,0,8 },
			{ 8,0,0,0,1,2,8,0,0,0,0,0,0,0,8 },
			{ 8,0,0,8,8,0,8,8,0,8,8,8,0,0,8 },
			{ 8,0,0,8,0,0,0,0,0,0,8,0,0,0,0 },
			{ 8,0,0,8,0,2,0,0,0,2,8,0,0,0,0 },
			{ 8,8,0,8,8,8,8,8,8,8,8,8,8,0,8 },
		};

		for (int y = 0; y < 8; ++y)
			for (int x = 0; x < 15; ++x) {
				md_.arr[y][x] = w_map[y][x];
			}
		return true;
	}

	void Map_Update(MapData & md_)
	{
		
	}

	void Map_Render(MapData & md_)
	{
		ML::Box2D  src(0, 32, 32, 32);
		for (int y = 0; y < 8; ++y) {
			for (int x = 0; x < 15; ++x) {
				ML::Box2D  draw(0, 0, 32, 32);
				draw.Offset(x * 32, y * 32);

				int num = md_.arr[y][x];
				ML::Box2D src = md_.chip[num];
				DG::Image_Draw("MapChipImg", draw, src);
			}
		}
	}
	bool Map_CheckHit(MapData & md_, Coord p_)
	{
		if (md_.arr[p_.y][p_.x] < 8 &&
			p_.isOutOfRange(ML::Box2D(0, 0, 15, 8)))
			return false;
		return true;
	}
	// ================================================================

	// ====> Player
	// ================================================================
	void Player_Initialize(Chara & c_, int x_, int y_, int a_)
	{
		c_.x = x_;
		c_.y = y_;
		c_.angle = a_;
	}
	void Player_Update(Chara & c_)
	{
		Coord pre = Coord(c_.x, c_.y);
		if (in1.LStick.L.down)	{ c_.x -= 1; c_.angle = 1; }
		if (in1.LStick.R.down)	{ c_.x += 1; c_.angle = 3; }
		if (in1.LStick.U.down)	{ c_.y -= 1; c_.angle = 2; }
		if (in1.LStick.D.down)	{ c_.y += 1; c_.angle = 0; }
		Coord now = Coord(c_.x, c_.y);

		if (Map_CheckHit(mapData, now)) {
			c_.x = pre.x;
			c_.y = pre.y;
		}
	}
	void Player_Render(Chara & c_)
	{
		ML::Box2D draw(0, 0, 32, 32);
		draw.Offset(c_.x * 32, c_.y * 32 - 12);
		auto src = playerImage[c_.angle];
		DG::Image_Draw("PlayerImg", draw, src);
	}
	// ================================================================
}