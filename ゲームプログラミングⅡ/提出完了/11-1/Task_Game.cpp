#include <vector>
#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	class Coord {
	public:
		int x;
		int y;

		inline bool Equals(const Coord& c_) {
			return c_.x == this->x && c_.y == this->y;
		}

		Coord(int x, int y) {
			this->x = x;
			this->y = y;
		}

		Coord(POINT p) {
			Coord(p.x, p.y);
		}

		Coord(const Coord& c_) {
			this->x = c_.x;
			this->y = c_.y;
		}

		Coord(ML::Vec2 v_) {
			Coord((int)v_.x, (int)v_.y);
		}

		bool isOnRange(const ML::Box2D b_) {
			return
				this->x >= b_.x &&
				this->x < b_.x + b_.w &&
				this->y >= b_.y &&
				this->y < b_.y + b_.h;
		}

		inline POINT ToPoint() {
			POINT ret;
			ret.x = this->x;
			ret.y = this->y;
			return ret;
		}

		Coord& operator+(const Coord& c_) {
			this->x += c_.x;
			this->y += c_.y;
			return *this;
		}

		bool operator==(const Coord& c_) {
			return Equals(c_);
		}

		bool operator!=(const Coord& c_) {
			return !Equals(c_);
		}
	};

	//ゲーム情報
	DI::VGamePad  in1;
	DI::Mouse	  mouse;

	struct MapData {
		int					arr[8][15];
		ML::Box2D			chip[32];
	};
	void Map_Initialize(MapData& md_);
	bool Map_Load(MapData& md_, int n_);
	void Map_Update(MapData& md_);
	void Map_Render(MapData& md_);
	bool Map_CheckHit(MapData& md_, const ML::Box2D& h_);
	void Map_ChangeChip(MapData& md_, Coord& p_);

	struct Chara {
		float				x;
		float				y;
		bool				hitFlag;
		ML::Box2D			hitBase;
		ML::Box2D			footBase;
		int					fallSpeed;
		int					jumpPow;
	};
	void Player_Initialize(Chara& c_, int x_, int y_);
	void Player_Update(Chara& c_);
	void Player_Render(Chara& c_);
	void Chara_CheckMove(Chara& c_, ML::Vec2& e_);
	bool Chara_CheckFoot(Chara& c_);




	ML::Box2D playerImage[4]{
		ML::Box2D(0, 0, 32, 32),
		ML::Box2D(32, 0, 32, 32),
		ML::Box2D(64, 0, 32, 32),
		ML::Box2D(96, 0, 32, 32)
	};

	MapData mapData;
	Chara player;


	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapChip01.bmp");	//背景
		DG::Image_Create("PlayerImg", "./data/image/HitTest.bmp");	//背景
		DG::Font_Create("FontA", "MS ゴシック", 16, 16);

		Map_Initialize(mapData);
		Map_Load(mapData, 1);
		Player_Initialize(player, 100, 180);
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
		DG::Image_Erase("PlayerImg");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");
		mouse = DI::Mouse_GetState();

		Map_Update(mapData);
		Player_Update(player);


		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		Map_Render(mapData);
		Player_Render(player);
	}




	// ====> MapData
	// ================================================================
	void Map_Initialize(MapData & md_)
	{
		for (int y = 0; y < 8; ++y)
			for (int x = 0; x < 15; ++x) {
				md_.arr[y][x] = 0;
			}

		for (int c = 0; c < 32; ++c) {
			int x = (c % 8);
			int y = (c / 8);
			md_.chip[c] = ML::Box2D(x * 32, y * 32, 32, 32);
		}
	}

	bool Map_Load(MapData & md_, int n_)
	{
		string filePath = "./data/map/Map" + to_string(n_) + ".txt";
		ifstream fin(filePath);
		if (!fin)
			return false;

		for (int y = 0; y < 8; ++y)
			for (int x = 0; x < 15; ++x) {
				fin >> md_.arr[y][x];
			}

		fin.close();
		return true;
	}

	void Map_Update(MapData & md_)
	{
		
	}

	void Map_Render(MapData & md_)
	{
		ML::Box2D  src(0, 32, 32, 32);
		for (int y = 0; y < 8; ++y) {
			for (int x = 0; x < 15; ++x) {
				ML::Box2D  draw(0, 0, 32, 32);
				draw.Offset(x * 32, y * 32);

				int num = md_.arr[y][x];
				ML::Box2D src = md_.chip[num];
				DG::Image_Draw("MapChipImg", draw, src);
			}
		}
	}
	bool Map_CheckHit(MapData & md_, const ML::Box2D& h_)
	{
		RECT r = { h_.x, h_.y, h_.x + h_.w, h_.y + h_.h };

		RECT m = { 0, 0, 32 * 15, 32 * 8 };
		if (r.left < m.left)	 r.left = m.left;
		if (r.right > m.right)	 r.right = m.right;
		if (r.top < m.top)		 r.top = m.top;
		if (r.bottom > m.bottom) r.bottom = m.bottom;

		int sx = r.left / 32,
			sy = r.top / 32,
			ex = (r.right - 1) / 32,
			ey = (r.bottom - 1) / 32;

		for (int y = sy; y <= ey; ++y)
			for (int x = sx; x <= ex; ++x)
				if (8 <= md_.arr[y][x])
					return true;

		return false;
	}
	void Map_ChangeChip(MapData & md_, Coord& p_)
	{
		if (p_.isOnRange(ML::Box2D(0, 0, 15, 8)))
			md_.arr[p_.y][p_.x] = (md_.arr[p_.y][p_.x] + 1) % 4;
	}
	// ================================================================

	// ====> Player
	// ================================================================
	void Player_Initialize(Chara & c_, int x_, int y_)
	{
		c_.x = x_;
		c_.y = y_;
		c_.hitFlag = false;
		c_.hitBase = ML::Box2D(-15, -24, 30, 48);
		c_.footBase = ML::Box2D(-15, 24, 30, 1);
		c_.fallSpeed = 0;
		c_.jumpPow = -15;
	}
	void Player_Update(Chara & c_)
	{
		ML::Vec2 est = { 0, 0 };
		if		(in1.LStick.L.on)	{ est.x -= 1; }
		if		(in1.LStick.R.on)	{ est.x += 1; }
	
		if (in1.B1.down && c_.hitFlag)
			c_.fallSpeed = c_.jumpPow;
		est.y += c_.fallSpeed;
		Chara_CheckMove(c_, est);

		c_.hitFlag = Chara_CheckFoot(c_);
		if (c_.hitFlag)
			c_.fallSpeed = 0;
		else
			c_.fallSpeed += 1;
	}
	void Player_Render(Chara & c_)
	{
		ML::Box2D draw = c_.hitBase.OffsetCopy(c_.x, c_.y);
		ML::Box2D src(0, 0, 100, 100);
		if (c_.hitFlag)
			src.Offset(100, 0);
		DG::Image_Draw("PlayerImg", draw, src, ML::Color(0.5f, 1.f, 1.f, 1.f));

		ML::Box2D textBox(0, 0, 480, 48);
		int dL = draw.x,
			dT = draw.y,
			dR = draw.x + draw.w - 1,
			dB = draw.y + draw.h - 1;

		int mL = dL / 32,
			mT = dT / 32,
			mR = dR / 32,
			mB = dB / 32;

		string text = 
			"X軸：" + to_string(dL) + "~" + to_string(dR) +
			"(" + to_string(mL) + "~" + to_string(mR) + ")" + "\n" +
			"Y軸：" + to_string(dT) + "~" + to_string(dB) +
			"(" + to_string(mT) + "~" + to_string(mB) + ")";
		DG::Font_Draw("FontA", textBox, text, ML::Color(1.f, 0.f, 0.f, 0.f));

	}
	void Chara_CheckMove(Chara & c_, ML::Vec2 & e_)
	{
		while (e_.x != 0) {
			float preX = c_.x;
			if (e_.x >= 1) {
				c_.x += 1;
				e_.x -= 1;
			}
			else if (e_.x <= -1) {
				c_.x -= 1;
				e_.x += 1;
			}
			else {
				c_.x += e_.x;
				e_.x -= e_.x;
			}

			ML::Box2D hit = c_.hitBase.OffsetCopy(c_.x, c_.y);
			if (Map_CheckHit(mapData, hit)) {
				c_.x = preX;
				break;
			}
		}

		while (e_.y != 0) {
			float preY = c_.y;
			if (e_.y >= 1) {
				c_.y += 1;
				e_.y -= 1;
			}
			else if (e_.y <= -1) {
				c_.y -= 1;
				e_.y += 1;
			}
			else {
				c_.y += e_.y;
				e_.y -= e_.y;
			}

			ML::Box2D hit = c_.hitBase.OffsetCopy(c_.x, c_.y);
			if (Map_CheckHit(mapData, hit)) {
				c_.y = preY;
				break;
			}
		}
	}
	bool Chara_CheckFoot(Chara & c_)
	{
		ML::Box2D hit = c_.footBase;
		hit.Offset(c_.x, c_.y);
		return Map_CheckHit(mapData, hit);
	}
	// ================================================================
}