#include "BChara.h"
#include "MyPG.h"
#include "Task_Map2D.h"




void BChara::CheckMove(ML::Vec2& e_) {
	auto map = ge->GetTask_One_GN<Map2D::Object>("フィールド", "マップ");
	if (!map)
		return;

	while (e_.x != 0) {
		float preX = this->pos.x;

		if (e_.x >= 1) { this->pos.x += 1;			e_.x -= 1; }
		else if (e_.x <= -1) { this->pos.x -= 1;			e_.x += 1; }
		else { this->pos.x += e_.x;		e_.x = 0; }

		ML::Box2D hit = this->hitBase.OffsetCopy(this->pos);
		if (map->CheckHit(hit)) {
			this->pos.x = preX;
			break;
		}
	}

	while (e_.y != 0) {
		float preY = this->pos.y;

		if (e_.y >= 1) { this->pos.y += 1;			e_.y -= 1; }
		else if (e_.y <= -1) { this->pos.y -= 1;			e_.y += 1; }
		else { this->pos.y += e_.y;		e_.y = 0; }

		ML::Box2D hit = this->hitBase.OffsetCopy(this->pos);
		if (map->CheckHit(hit)) {
			this->pos.y = preY;
			break;
		}
	}
}

bool BChara::CheckFoot() {
	ML::Box2D foot(
		this->hitBase.x,
		this->hitBase.y + this->hitBase.h,
		this->hitBase.w,
		1
	);
	foot.Offset(this->pos);

	auto map = ge->GetTask_One_GN<Map2D::Object>("フィールド", "マップ");
	if (!map)
		return false;

	return map->CheckHit(foot);
}