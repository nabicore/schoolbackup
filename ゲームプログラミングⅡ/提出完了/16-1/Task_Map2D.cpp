//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#include  "MyPG.h"
#include  "Task_Map2D.h"

namespace  Map2D
{
	Resource::WP  Resource::instance;
	//-------------------------------------------------------------------
	//\[XΜϊ»
	bool  Resource::Initialize()
	{
		return true;
	}
	//-------------------------------------------------------------------
	//\[XΜπϊ
	bool  Resource::Finalize()
	{
		return true;
	}
	//-------------------------------------------------------------------
	//uϊ»v^XNΆ¬ΙPρΎ―s€
	bool  Object::Initialize()
	{
		//X[p[NXϊ»
		__super::Initialize(defGroupName, defName, true);
		//\[XNXΆ¬or\[X€L
		this->res = Resource::Create();

		//f[^ϊ»
		this->render2D_Priority[1] = 0.9f;
		this->imageName = "MapChipImg";

		for (int y = 0; y < 100; ++y)
			for (int x = 0; x < 100; ++x)
				this->arr[y][x] = 0;

		this->sizeX = 0;
		this->sizeY = 0;
		this->hitBase = ML::Box2D(0, 0, 0, 0);

		for (int c = 0; c < 16; ++c)
			this->chip[c] = ML::Box2D(
				(c % 8) * 32,
				(c / 8) * 32,
				32, 32
			);

		//^XNΜΆ¬

		return  true;
	}
	//-------------------------------------------------------------------
	//uIΉv^XNΑΕΙPρΎ―s€
	bool  Object::Finalize()
	{
		//f[^^XNπϊ
		DG::Image_Erase(this->imageName);

		if (!ge->QuitFlag() && this->nextTaskCreate) {
			//ψ«p¬^XNΜΆ¬
		}

		return  true;
	}
	//-------------------------------------------------------------------
	//uXVvPt[Ιs€
	void  Object::UpDate()
	{
	}
	//-------------------------------------------------------------------
	//uQc`ζvPt[Ιs€
	void  Object::Render2D_AF()
	{
		if (this->hitBase.Hit(ge->camera2D)) {
			RECT c = {
				ge->camera2D.x,
				ge->camera2D.y,
				ge->camera2D.x + ge->camera2D.w,
				ge->camera2D.y + ge->camera2D.h
			};
			RECT m = {
				this->hitBase.x,
				this->hitBase.y,
				this->hitBase.x + this->hitBase.w,
				this->hitBase.y + this->hitBase.h
			};
			RECT isr = {
				max(c.left, m.left),
				max(c.top,  m.top),
				max(c.right, m.right),
				max(c.bottom, m.bottom)
			};

			int sx = isr.left / 32,
				sy = isr.top / 32,
				ex = (isr.right - 1) / 32,
				ey = (isr.bottom - 1) / 32;
			
			for (int y = sy; y <= ey; ++y)
				for (int x = sx; x <= ex; ++x) {
					ML::Box2D draw(x * 32, y * 32, 32, 32);
					draw.Offset(-ge->camera2D.x, -ge->camera2D.y);
					DG::Image_Draw(this->imageName, draw, this->chip[this->arr[y][x]]);
				}
		}
	}

	bool Object::Load(const string& fpath_) {
		ifstream fin(fpath_);
		if (!fin)
			return false;

		string chipFileName, chipFilePath;
		fin >> chipFileName;
		chipFilePath = "./data/image/" + chipFileName;
		DG::Image_Create(this->imageName, chipFilePath);

		fin >> this->sizeX >> this->sizeY;
		this->hitBase = ML::Box2D(0, 0, this->sizeX * 32, this->sizeY * 32);

		for (int y = 0; y < this->sizeY; ++y)
			for (int x = 0; x < this->sizeX; ++x)
				fin >> this->arr[y][x];

		fin.close();
		return true;
	}

	bool Object::Load() {
		int w_map[8][15]{
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
			{ 8,8,8,8,8,8,8,8,8,8,8,8,8,8,8 },
		};
		this->sizeX = 15;
		this->sizeY = 8;
		this->hitBase = ML::Box2D(0, 0, this->sizeX * 32, this->sizeY * 32);

		memcpy(this->arr, w_map, sizeof(w_map));
		DG::Image_Create(this->imageName, "./data/image/MapChip01.bmp");
		return true;
	}

	bool Object::CheckHit(const ML::Box2D& h_) {
		RECT r = { h_.x, h_.y, h_.x + h_.w, h_.y + h_.h };
		RECT m = { this->hitBase.x,
				   this->hitBase.y,
				   this->hitBase.x + this->hitBase.w,
				   this->hitBase.y + this->hitBase.h };

		if (r.left < m.left) r.left = m.left;
		if (r.top < m.top)	 r.top = m.top;
		if (r.right > m.right) r.right = m.right;
		if (r.bottom > m.bottom) r.bottom = m.bottom;

		int sx = r.left / 32,
			sy = r.top / 32,
			ex = (r.right - 1) / 32,
			ey = (r.bottom - 1) / 32;

		for (int y = sy; y <= ey; ++y)
			for (int x = sx; x <= ex; ++x)
				if (8 <= this->arr[y][x])
					return true;


		return false;
	}

	void Object::AjastCameraPos() {
		RECT c = {
			ge->camera2D.x,
			ge->camera2D.y,
			ge->camera2D.x + ge->camera2D.w,
			ge->camera2D.y + ge->camera2D.h
		};
		RECT m = {
			this->hitBase.x,
			this->hitBase.y,
			this->hitBase.x + this->hitBase.w,
			this->hitBase.y + this->hitBase.h
		};

		if (c.right > m.right)		ge->camera2D.x = m.right - ge->camera2D.w;
		if (c.bottom > m.bottom)	ge->camera2D.y = m.bottom - ge->camera2D.h;
		if (c.left < m.left)		ge->camera2D.x = m.left;
		if (c.top < m.top)			ge->camera2D.y = m.top;
	}

	//
	//ΘΊΝξ{IΙΟXsvΘ\bh
	//
	//-------------------------------------------------------------------
	//^XNΆ¬ϋ
	Object::SP  Object::Create(bool  flagGameEnginePushBack_)
	{
		Object::SP  ob = Object::SP(new  Object());
		if (ob) {
			ob->me = ob;
			if (flagGameEnginePushBack_) {
				ge->PushBack(ob);//Q[GWΙo^
			}
			if (!ob->B_Initialize()) {
				ob->Kill();//CjVCYΙΈs΅½ηKill
			}
			return  ob;
		}
		return nullptr;
	}
	//-------------------------------------------------------------------
	bool  Object::B_Initialize()
	{
		return  this->Initialize();
	}
	//-------------------------------------------------------------------
	Object::~Object() { this->B_Finalize(); }
	bool  Object::B_Finalize()
	{
		auto  rtv = this->Finalize();
		return  rtv;
	}
	//-------------------------------------------------------------------
	Object::Object() {	}
	//-------------------------------------------------------------------
	//\[XNXΜΆ¬
	Resource::SP  Resource::Create()
	{
		if (auto sp = instance.lock()) {
			return sp;
		}
		else {
			sp = Resource::SP(new  Resource());
			if (sp) {
				sp->Initialize();
				instance = sp;
			}
			return sp;
		}
	}
	//-------------------------------------------------------------------
	Resource::Resource() {}
	//-------------------------------------------------------------------
	Resource::~Resource() { this->Finalize(); }
}