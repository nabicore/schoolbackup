#include "MyPG.h"
#include "MyGameMain.h"

//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad  in1;

	struct  MapData {
		int			arr[17][30];
		ML::Box2D	chip[24];
	};
	struct  Chara {
		int			x;
		int			y;
		int			angle;
		int			animCnt;
	};


	void  Map_Initialize(MapData&  md_);
	bool  Map_Load(MapData&  md_, int n_);
	void  Map_ConnectWall(MapData&  md_);
	void  Map_UpData(MapData&  md_);
	void  Map_Render(MapData&  md_);
	bool  Map_CheckHit(MapData&  md_, POINT  p_);

	void  Player_Initialize(Chara&  c_, int  x_,  int  y_,  int a_);
	void  Player_UpDate(Chara&  c_);
	void  Player_Render(Chara&  c_);

	int   Angle4();

	MapData  mapData;
	Chara    player;
	ML::Box2D  playerImage[4][4];
	bool	clearFlag;
	bool	failFlag;

	int currentTime;
	const int timeLimit = 5 * 60;

	//-------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/MapChip02.png");
		DG::Image_Create("PlayerImg", "./data/image/chara02.png");
		DG::Image_Create("ClearImg", "./data/image/Clear.bmp");
		DG::Image_Create("FailImg", "./data/image/Fail.bmp");
		DG::Font_Create("fontA", "ＭＳ ゴシック", 8, 16);
		//マップの初期化
		Map_Initialize(mapData);
		//マップの読み込み
		Map_Load(mapData, 1);

		//プレイヤの初期化
		Player_Initialize(player, 1, 1, 0);
		//
		int  d[4] = {0,1,3,2};//画像の並び順入れ替え
		int  anim[4] = { 0, 1, 0, 2 };
		for (int i = 0; i < 4; ++i) {
			int y = d[i];
			for (int j = 0; j < 4; ++j) {
				int x = anim[j];
				playerImage[i][j] = ML::Box2D(x * 32, y * 32, 32, 32);
			}
		}
		clearFlag = false;
		failFlag = false;
		currentTime = 0;
	}
	//-------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-------------------------------------------------------------------------
	void Finalize()
	{
		DG::Image_Erase("MapChipImg");
		DG::Image_Erase("PlayerImg");
		DG::Image_Erase("ClearImg");
		DG::Image_Erase("FailImg");
		DG::Font_Erase("fontA");
	}
	//-------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");
		Map_UpData(mapData);

		//プレイヤの行動
		if (!clearFlag && !failFlag) {
			Player_UpDate(player);
			currentTime += 1;
			if (currentTime >= timeLimit)
				failFlag = true;
		}

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-------------------------------------------------------------------------
	void Render()
	{
		Map_Render(mapData);
		//プレイヤの表示
		Player_Render(player);
		
		DG::Font_Draw("fontA", ML::Box2D(0, 0, 100, 100), "残り時間：" + std::to_string((timeLimit - currentTime) / 60 + 1));

		if (clearFlag)
			DG::Image_Draw("ClearImg",
				ML::Box2D(ge->screenWidth / 2 - 256, 80, 512, 160),
				ML::Box2D(0, 0, 256, 80));
		if (failFlag)
			DG::Image_Draw("FailImg",
				ML::Box2D(ge->screenWidth / 2 - 256, 80, 512, 160),
				ML::Box2D(0, 0, 256, 80));
	}

	//-------------------------------------------------------------------------
	void  Map_Initialize(MapData&  md_)
	{
		//マップのゼロクリア
		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				md_.arr[y][x] = 0;
			}
		}
		//マップチップ情報の初期化
		for (int c = 0; c < 24; ++c) {
			int  x = (c % 4);
			int  y = (c / 4);
			md_.chip[c] = ML::Box2D(x * 32, y * 32, 32, 32);
		}
	}
	//-------------------------------------------------------------------------
	bool  Map_Load(MapData&  md_, int n_)
	{
		//ファイルパスを作る
		string  filePath = "./data/map/Map" + to_string(n_) + ".txt";
		//ファイルを開く（読み込み）
		ifstream   fin(filePath);
		if (!fin) { return  false; }//読み込み失敗

		//配列にデータを取り込む
		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				fin >> md_.arr[y][x];
			}
		}

		//ファイルを閉じる
		fin.close();

		//くっつきマップにコンバート
		Map_ConnectWall(md_);
		return  true;
	}
	//-----------------------------------------------------------------------------
	//くっつきマップへの変換
	void  Map_ConnectWall(MapData&  md_)
	{
		int w_map[17][30];
		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				int no = 8;
				if (md_.arr[y][x] == 0) {
					if (y - 1 >= 0		&& md_.arr[y - 1][x] == 0) no += 1;
					if (x - 1 <= 30 - 1 && md_.arr[y][x + 1] == 0) no += 2;
					if (y - 1 <= 17 - 1 && md_.arr[y + 1][x] == 0) no += 4;
					if (x - 1 >= 0		&& md_.arr[y][x - 1] == 0) no += 8;
				}
				else
					no = md_.arr[y][x];
				w_map[y][x] = no;
			}
		}

		memcpy(md_.arr, w_map, sizeof(md_.arr));
	}
	//-------------------------------------------------------------------------
	void  Map_UpData(MapData&  md_)
	{

	}
	//-------------------------------------------------------------------------
	void  Map_Render(MapData&  md_)
	{
		for (int y = 0; y < 17; ++y) {
			for (int x = 0; x < 30; ++x) {
				ML::Box2D  draw(x * 32, y * 32, 32, 32);
				int  num = md_.arr[y][x];
				DG::Image_Draw("MapChipImg", draw, md_.chip[ num ]);
			}
		}
	}
	//-------------------------------------------------------------------------
	bool  Map_CheckHit(MapData&  md_, POINT  p_)
	{
		if (p_.x < 0) { return true; }
		if (p_.y < 0) { return true; }
		if (p_.x >= 30) { return true; }
		if (p_.y >= 17) { return true; }

		//マスを調べる（侵入可能マス）
		if (md_.arr[p_.y][p_.x] == 1 ||
			md_.arr[p_.y][p_.x] == 2 || 
			md_.arr[p_.y][p_.x] == 4 || 
			md_.arr[p_.y][p_.x] == 5 ||
			md_.arr[p_.y][p_.x] == 6) {
			return false;
		}
		return true;//障害物に接触
	}

	//-------------------------------------------------------------------------
	void  Player_Initialize(Chara&  c_, int  x_, int  y_, int a_)
	{
		c_.x = x_;
		c_.y = y_;
		c_.angle = a_;
		c_.animCnt = 0;
	}
	//-------------------------------------------------------------------------
	void  Player_UpDate(Chara&  c_)
	{
		POINT  pre = { c_.x, c_.y };//移動前の座標を保持
		bool move = true;
		switch (Angle4()) {
		case 1:		c_.x -= 1;		c_.angle = 1;	break;
		case 3:     c_.x += 1;		c_.angle = 3;	break;
		case 2:		c_.y -= 1;		c_.angle = 2;	break;
		case 0:		c_.y += 1;		c_.angle = 0;	break;
		default:	move = false;					break;
		}
		if (move == true) {
			POINT  now = { c_.x, c_.y };//移動後の座標で接触判定情報を用意
			if (true == Map_CheckHit(mapData, now)) {
				//移動をキャンセル
				c_.x = pre.x;
				c_.y = pre.y;
			}
			else {
				int chip = mapData.arr[c_.y][c_.x];
				switch (chip) {
				case 2:
					clearFlag = true;
					break;
				case 4:
					for (int y = 0; y < 17; ++y)
						for (int x = 0; x < 30; ++x)
							if (mapData.arr[y][x] == 5) {
								c_.x = x;
								c_.y = y;
							}
					break;
				case 5:
					for (int y = 0; y < 17; ++y)
						for (int x = 0; x < 30; ++x)
							if (mapData.arr[y][x] == 4) {
								c_.x = x;
								c_.y = y;
							}
					break;
				case 6:
					mapData.arr[c_.y][c_.x] = 1;
					for (int y = 0; y < 17; ++y)
						for (int x = 0; x < 30; ++x)
							if (mapData.arr[y][x] == 7)
								mapData.arr[y][x] = 1;
				}
			}
		}
		c_.animCnt += 1;
	}
	//-------------------------------------------------------------------------
	void  Player_Render(Chara&  c_)
	{
		ML::Box2D  draw(0, -12, 32, 32);
		draw.Offset(c_.x * 32, c_.y * 32);
		ML::Box2D  src = playerImage[c_.angle][(c_.animCnt / 10) % 4];
		DG::Image_Draw("PlayerImg", draw, src);
	}
	//-------------------------------------------------------------------------
	int   Angle4( )
	{
		int x = 1, y = 1;
		if (in1.LStick.L.down) {	x -= 1; }
		if (in1.LStick.R.down) {	x += 1; }
		if (in1.LStick.U.down) {	y -= 1; }
		if (in1.LStick.D.down) {	y += 1; }
		int  mat[3][3] = {
			{ 1, 2, 2},
			{ 1,-1, 3},
			{ 0, 0, 3},
		};
		return mat[y][x];//４方向orニュートラルのどれかを返す
	}


}