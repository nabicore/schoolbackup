#pragma once



#include "GameEngine_Ver3_7.h"


class BEnemy : public BTask {

public:
	typedef shared_ptr<BEnemy>			SP;
	typedef weak_ptr<BEnemy>			WP;


public:
	enum Angle_LR { Left, Right };

	ML::Vec2			pos;
	ML::Box2D			hitBase;

	BEnemy()
		: pos(0, 0)
		, hitBase(0, 0, 0, 0)
	{}
	virtual ~BEnemy() {}

	
};