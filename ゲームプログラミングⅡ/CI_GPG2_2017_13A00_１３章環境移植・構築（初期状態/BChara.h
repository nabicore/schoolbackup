#pragma once



#include "GameEngine_Ver3_7.h"


class BChara : public BTask {

public:
	typedef shared_ptr<BChara>			SP;
	typedef weak_ptr<BChara>			WP;


public:
	enum Angle_LR { Left, Right };

	ML::Vec2							pos;
	ML::Box2D							hitBase;
	ML::Vec2							moveVec;
	int									moveCnt;
	Angle_LR							angle_LR;

	BChara()
		: pos(0, 0)
		, hitBase(0, 0, 0, 0)
		, moveVec(0, 0)
		, moveCnt(0)
		, angle_LR(Right)
	{}
	virtual ~BChara() {}


	virtual void CheckMove(ML::Vec2& est_);
	virtual bool CheckFoot();
	
};