#pragma once



#include "GameEngine_Ver3_7.h"


class BShot : public BTask {

public:
	typedef shared_ptr<BShot>			SP;
	typedef weak_ptr<BShot>				WP;


public:
	ML::Vec2					pos;
	ML::Box2D					hitBase;
	ML::Vec2					moveVec;
	int							moveCnt;

	BShot()
		: pos(0, 0)
		, hitBase(0, 0, 0, 0)
		, moveVec(0, 0)
		, moveCnt(0)
	{}
	virtual ~BShot() {}

	
};