#include "MyPG.h"
#include "MyGameMain.h"




namespace Game {
	extern int		playerStock;
	extern int		stageNum;
}

namespace StageLogo
{

	int timeCnt;
	void ImageFont_Draw(int x_, int y_, const char* msg_);

	void Initialize() {
		DG::Image_Create("TitleBGImg", "./data/image/titleback.jpg");
		DG::Image_Create("FontTextImg", "./data/image/font_text.png");
		timeCnt = 0;
	}

	void Finalize() {
		DG::Image_Erase("TitleBGImg");
		DG::Image_Erase("FontTextImg");
	}

	TaskFlag UpDate() {
		TaskFlag rtv = Task_StageLogo;
		if (timeCnt > 30)
			rtv = Task_Game;
		timeCnt += 1;
		return rtv;
	}

	void Render() {
		ML::Box2D draw(0, 0, 960, 540);
		ML::Box2D src(0, 0, 960, 540);
		DG::Image_Draw("TitleBGImg", draw, src);

		string buf;
		buf = "STAGE:" + to_string(Game::stageNum);
		ImageFont_Draw(300, 200, buf.c_str());
		buf = "ZANKI:" + to_string(Game::playerStock);
		ImageFont_Draw(300, 250, buf.c_str());
	}

	void ImageFont_Draw(int x_, int y_, const char* msg_) {
		int dx = x_;
		int dy = y_;
		for (int i = 0; i < (int)strlen(msg_); ++i) {
			int code = ((unsigned char)msg_[i]);
			int fx = code % 16 * 32;
			int fy = code / 16 * 32;
			ML::Box2D draw(dx, dy, 32, 32);
			ML::Box2D src(fx, fy, 32, 23);
			DG::Image_Draw("FontTextImg", draw, src);
			dx += 32;
		}
	}

}