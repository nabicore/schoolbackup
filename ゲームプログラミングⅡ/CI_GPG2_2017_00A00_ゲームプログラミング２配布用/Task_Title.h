#pragma warning(disable:4996)
#pragma once

// ------------------------------------------------
//タイトル画面
// ------------------------------------------------
#include "GameEngine_Ver3_7.h"


namespace Title {
	const string defGroupName("タイトル");
	const string defName("NoName");
	// ------------------------------------------------
	class Resource {
		bool Initialize();
		bool Finalize();
		Resource();

	public:
		~Resource();
		typedef shared_ptr<Resource> SP;
		typedef weak_ptr<Resource> WP;
		static WP instance;
		static Resource::SP Create();
		string imageName;
	};
	// ------------------------------------------------
	class Object : public BTask {
	public:
		virtual ~Object();
		typedef shared_ptr<Object> SP;
		typedef weak_ptr<Object> WP;
		static Object::SP Create(bool flagGameEnginePushBack_);
		Resource::SP res;

	private:
		Object();
		bool B_Initialize();
		bool B_Finalize();
		bool Initialize();
		void UpDate();
		void Render2D_AF();
		bool Finalize();

	public:
		int logoPosY;
	};
}