#pragma once


class Animation {
	
public:
	typedef struct _tagParam {
		ML::Box2D*				frames;
		int						maxFrameCnt = 0;
		string					spriteSheet;
		ML::Box2D				spriteSheetSize;
		int						switchTick;
	}Param;

private:
	int						tick = 0;
	int						curFrameCnt = 0;
	bool					isActive = true;
	ML::Box2D				curFrame;
	ML::Vec2				position;
	Param					params;

public:
	explicit Animation(const Param& param_);
	virtual ~Animation();

	void setPosition(const ML::Vec2& pos_);
	ML::Vec2 getPosition() const;

	void Update();
	void Draw();
	void Destroy();
};





class AnimationStateMachine {


private:
	std::map<string, Animation>			states;

public:
	explicit AnimationStateMachine();
	virtual ~AnimationStateMachine();

	
};