#include "MyPG.h"



Animation::Animation(const Param & param_) : params(param_) {
	curFrame = params.frames[0];
	position = ML::Vec2(0, 0);
}

Animation::~Animation() {
}

void Animation::setPosition(const ML::Vec2 & pos_) {
	this->position = pos_;
}

ML::Vec2 Animation::getPosition() const {
	return this->position;
}

void Animation::Update() {

	if (isActive) {
		tick += 1;
		if (tick >= params.switchTick) {
			if (curFrameCnt >= (params.maxFrameCnt - 1))
				curFrameCnt = 0;
			else
				curFrameCnt += 1;
			curFrame = params.frames[curFrameCnt];
			tick = 0;
		}
	}
}

void Animation::Draw() {
	DG::Image_Draw(
		params.spriteSheet, 
		ML::Box2D(position.x, position.y, curFrame.w, curFrame.h),
		curFrame);
}

void Animation::Destroy() {
	delete params.frames;
}