#pragma once



class Enemy {

public:
	enum class State : int {
		Idle = 0,
		Attack,
		Hit,
		Dead
	};

	typedef struct _tagParam {
		int			hp;
		int			speed;
		ML::Box2D	collideBox;
	}Param;


private:
	State			curState;
	ML::Box2D		collider;
	Param			params;

public:
	explicit Enemy(const Param& p_);
	virtual ~Enemy();

	State getState() const;
	ML::Box2D getCollider() const;

	void Update();
};