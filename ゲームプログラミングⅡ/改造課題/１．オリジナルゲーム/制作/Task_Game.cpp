#include "MyPG.h"
#include "MyGameMain.h"

#define ADD_ANIMATION(name, params) \
	animations.insert(std::pair<string, Animation>(name, Animation(params)))

//ゲーム本編
namespace Game
{
	//ゲーム情報
	DI::VGamePad  in1;
	map<string, Animation> animations;

	//-----------------------------------------------------------------------------
	//初期化処理
	//機能概要：プログラム起動時に１回実行される（素材などの準備を行う）
	//-----------------------------------------------------------------------------
	void Initialize()
	{
		DG::Image_Create("MapChipImg", "./data/image/TestImage01.bmp");	//背景

		ML::Box2D* player = new ML::Box2D[4]{
			ML::Box2D(0, 0, 64, 48), 
			ML::Box2D(64, 0, 64, 48), 
			ML::Box2D(0, 48, 64, 48), 
			ML::Box2D(64, 48, 64, 48)
		};
		animations.insert(std::pair<string, Animation>("Player", Animation(Animation::Param{ player, 4, "MapChipImg", ML::Box2D(0, 0, 512, 512), 60 })));
	}
	//-----------------------------------------------------------------------------
	//解放処理
	//機能概要：プログラム終了時に１回実行される（素材などの解放を行う）
	//-----------------------------------------------------------------------------
	void Finalize()
	{
		// Animations
		for (auto& anim : animations)
			anim.second.Destroy();

		DG::Image_Erase("MapChipImg");
	}
	//-----------------------------------------------------------------------------
	//実行処理
	//機能概要：ゲームの１フレームに当たる処理
	//-----------------------------------------------------------------------------
	TaskFlag UpDate()
	{
		in1 = DI::GPad_GetState("P1");

		// Animations
		for (auto& anim : animations)
			anim.second.Update();

		TaskFlag rtv = Task_Game;//取りあえず現在のタスクを指定
		if (true == in1.ST.down) {
			rtv = Task_Title;	//次のタスクをタイトルへ
		}
		return rtv;
	}
	//-----------------------------------------------------------------------------
	//描画処理
	//機能概要：ゲームの１フレームに当たる表示処理
	//-----------------------------------------------------------------------------
	void Render()
	{
		// Animations
		for (auto& anim : animations) {
			anim.second.Draw();
		}
	}
}