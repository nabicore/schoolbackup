﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai14
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string result;
            float weight = float.Parse(textBox2.Text);
            float height = float.Parse(textBox1.Text);
            float bmi = weight / ((height * 0.01f) * (height * 0.01f));

            if (bmi < 18.5f)
                result = "低体重";
            else if (bmi < 25f)
                result = "普通体重";
            else if (bmi < 30f)
                result = "肥満1度";
            else if (bmi < 35f)
                result = "肥満2度";
            else if (bmi < 40f)
                result = "肥満3度";
            else
                result = "肥満4度";

            label3.Text = "BMI: " + bmi;
            label4.Text = "判定：" + result;
            label5.Text = "標準体重：" + ((height > 150) ? height - 100f : (height - 100) * 0.9f).ToString();
        }
    }
}
