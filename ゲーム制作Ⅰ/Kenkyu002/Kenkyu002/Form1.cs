﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kenkyu002
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float weight = float.Parse(textBox2.Text);
            float height = float.Parse(textBox1.Text);
            float normalWeight = (height < 150) ? height - 100 : (height - 100) * 0.9f;

            label3.Text = "理想体重：" + normalWeight;
            label4.Text = (weight - normalWeight) + "Kgやせなさい！";
        }
    }
}
