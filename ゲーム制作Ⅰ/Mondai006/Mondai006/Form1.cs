﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai006
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Font = new Font("MS UI Gothic", 16, FontStyle.Italic);
            label1.Text = "Hello";
            label1.ForeColor = Color.Red;

            label2.Font = new Font("MS UI Gothic", 24, FontStyle.Regular);
            label2.Text = "17CI0112 キムヒウォン";
            label2.ForeColor = Color.Blue;

            label3.Font = new Font("MS Pゴシック", 24, FontStyle.Bold);
            label3.ForeColor = Color.Green;
            label3.Text = "日本　太郎";
        }
    }
}
