﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai029
{
    public partial class Form1 : Form
    {
        string text_sex = "";
        string text_bloodtype = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label5.Text = "";
            label5.Text += textBox1.Text + "さんは、";                          //名前
            label5.Text += comboBox1.SelectedItem.ToString() + "の";           //年齢
            label5.Text += text_sex + "性です。\n";                               //性別
            label5.Text += "血液方は、" + text_bloodtype + "です。";            //血液方
        }


        void Select_Sex (Object s, EventArgs e)
        {
            RadioButton sex = (RadioButton)s;
            text_sex = sex.Text;
        }
        void Select_Bloodtype(Object s, EventArgs e)
        {
            RadioButton bt = (RadioButton)s;
            text_bloodtype = bt.Text;
        }
    }
}
