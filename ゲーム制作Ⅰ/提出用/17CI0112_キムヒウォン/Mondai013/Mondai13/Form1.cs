﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai13
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float height = float.Parse(textBox1.Text);
            string output;
            if (height > 150f)
                output = "標準体重：" + ((height - 100) * 0.9f).ToString();
            else
                output = "標準体重：" + (height - 100).ToString();
            label2.Text = output;
        }
    }
}
