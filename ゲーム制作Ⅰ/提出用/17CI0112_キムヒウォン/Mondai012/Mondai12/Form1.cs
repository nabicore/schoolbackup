﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float hankei = float.Parse(textBox1.Text);
            label2.Text = "円　　周：" + (hankei * Math.PI * 2.0f);
            label3.Text = "面　　積：" + (hankei * hankei * Math.PI);
        }
    }
}
