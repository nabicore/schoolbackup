﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai002
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //　ウィンドウフォーム中のラベルのテキストを変更
            label1.Text = "Hello";
            label2.Text = "17CI0112";
            label3.Text = "キムヒウォン";
            label4.Text = label2.Text + label3.Text;

            //　ラベルの書式を変更する
            label1.BorderStyle = BorderStyle.None;
            label2.BorderStyle = BorderStyle.FixedSingle;
            label3.BorderStyle = BorderStyle.Fixed3D;
            label4.BackColor = Color.Red;
        }
    }
}
