﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_46
{
    public partial class Form1 : Form
    {
        private class Rect
        {
            public int x;
            public int y;
            public int w;
            public int h;
        }

        PictureBox imgBox;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            imgBox = new PictureBox();
            imgBox.SetBounds(50, 20, 84, 66);
            this.Controls.Add(imgBox);

            string[] flagFile = {
                "flag_japan.gif",
                "flag_korea.gif",
                "flag_usa.gif",
                "flag_french.gif",
                "flag_china.gif"
            };
            string[] flagName = {
                "日本",
                "韓国",
                "アメリカ",
                "フランス",
                "中国"
            };
            int[] buttonPos = {
                0, 1,
                10, 11,
                20
            };

            Rect firstPos = new Rect { x = 40, y = 110, w = 100, h = 24 };
            for (int i = 0; i < 5; ++i)
                CreateNewMenu(new Rect { x = firstPos.x + (buttonPos[i] % 10) * 130,
                                         y = firstPos.y + (buttonPos[i] / 10) * 30,
                                         w = 100, h = 24 },
                                         flagName[i], flagFile[i]);
        }

        private void CreateNewMenu(Rect pos, string name, string path)
        {
            Button button = new Button();
            button.SetBounds(pos.x, pos.y, pos.w, pos.h);
            button.Text = name + "の国旗";
            button.Click += new EventHandler((o, e) => imgBox.Image = Image.FromFile(path));
            this.Controls.Add(button);
        }
    }
}
