﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_40
{
    public partial class Form1 : Form
    {
        string[] leftAnime = { "pipiL1.gif", "pipiL2.gif" };
        string[] rightAnime = { "pipiR1.gif", "pipiR2.gif" };
        int currentCut = 0;
        int maxCut = 2;
        int animeTick = 0;

        int muki = -1;
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // 右側へ
            if (pictureBox1.Location.X < 0)
            {
                pictureBox1.Image = Image.FromFile(rightAnime[currentCut]);
                pictureBox1.Location = new Point(0, pictureBox1.Location.Y);
                muki = 1;
            }
            if (pictureBox1.Location.X > (this.Size.Width - pictureBox1.Size.Width))
            {
                muki = -1;
                pictureBox1.Image = Image.FromFile(leftAnime[currentCut]);
                pictureBox1.Location = new Point(this.Size.Width - pictureBox1.Size.Width,
                                                 pictureBox1.Location.Y);
            }

            pictureBox1.Location = new Point(pictureBox1.Location.X + muki * 2, 
                                             pictureBox1.Location.Y);


            // アニメーション処理
            animeTick++;
            if (muki < 0 && animeTick > 5)
            {
                pictureBox1.Image = Image.FromFile(leftAnime[currentCut]);
                ResetAnimation();
            }
            if (muki > 0 && animeTick > 5)
            {
                pictureBox1.Image = Image.FromFile(rightAnime[currentCut]);
                ResetAnimation();
            }
        }

        private void ResetAnimation()
        {
            animeTick = 0;
            currentCut++;
            if (currentCut > maxCut - 1)
                currentCut = 0;
        }
    }
}
