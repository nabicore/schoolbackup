﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_10_2
{
    public partial class Form1 : Form
    {
        int posx;
        int idox = 4;
        int posy;
        int idoy = 4;
        int ten = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            posx = pictureBox1.Left;
            posy = pictureBox1.Top;
            timer1.Interval = 20;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int wx, wy;
            posx += idox;
            posy += idoy;
            pictureBox1.Left = posx;
            pictureBox1.Top = posy;

            if (button1.Left <= pictureBox1.Right
                && pictureBox1.Left <= button1.Right)
            {
                if (button1.Top <= pictureBox1.Bottom
                    && pictureBox1.Top <= button1.Bottom) 
                {
                    posy -= idoy;
                    posx += idox;
                    idoy = -idoy;
                    ten++;

                    if (idox < -4 || 4 < idox)
                        idox = (idox >= 0) ? idox - 3 : idox + 3;
                    if (idoy < -4 || 4 < idoy)
                        idoy = (idoy >= 0) ? idoy - 3 : idoy + 3;
                }
                label1.Text = "得点：" + ten.ToString("D4");
            }

            wx = posx + idox;
            if (wx <= 0)
            {
                idox = -idox;
                idox = (idox >= 0) ? idox + 1 : idox - 1;
            }
            else if (this.ClientRectangle.Right <= wx + pictureBox1.Width)
            {
                idox = -idox;
                idox = (idox >= 0) ? idox + 1 : idox - 1;
            }

            wy = posy + idoy;
            if (wy <= 0)
            {
                idoy = -idoy;
                idox = (idox >= 0) ? idox + 1 : idox - 1;
            }
            else if (this.ClientRectangle.Bottom <= wy + pictureBox1.Height)
            {
                idoy = -idoy;
                timer1.Stop();
            }
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.Left = button1.Left + e.X - button1.Width / 2;
        }
    }
}
