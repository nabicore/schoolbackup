﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_35
{
    public partial class Form1 : Form
    {
        DateTime start;
        bool isStart = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            TimeSpan kankaku;

            kankaku = DateTime.Now - start;
            label1.Text = kankaku.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            isStart = !isStart;
            if (isStart)
            {
                button1.Text = "ストップ";
                timer1.Interval = 1;
                timer1.Start();
                if (start.Ticks == 0)
                    start = DateTime.Now;
            }
            else
            {
                button1.Text = "スタート";
                timer1.Stop();
            }
        }
    }
}
