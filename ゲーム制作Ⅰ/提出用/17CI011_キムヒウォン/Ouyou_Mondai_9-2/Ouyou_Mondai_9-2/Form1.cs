﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 

namespace Ouyou_Mondai_9_2
{
    public partial class Form1 : Form
    {
        Animation anim;
        int speed = 1;
        int dir = 1;

        public Form1()
        {
            InitializeComponent();
            anim = new Animation(50, 2, "invaderB", ".gif");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // アニメーション
            anim.Update(pictureBox1);

            // キャラクター真ん中固定
            pictureBox2.Location = new Point(this.Size.Width / 2 - pictureBox2.Size.Width, 
                                             this.Size.Height - pictureBox2.Size.Height * 2);

            // 動き処理
            var playerPos = pictureBox1.Location;
            playerPos.X += (dir * speed);
            if (pictureBox1.Location.X < 0)
            {
                dir = 1;
                playerPos.X = 0;
                playerPos.Y += 30;
            }
            if (pictureBox1.Location.X > this.Size.Width - pictureBox1.Size.Width) {
                dir = -1;
                playerPos.X = this.Size.Width - pictureBox1.Size.Width;
                playerPos.Y += 30;
            }
            pictureBox1.Location = playerPos;
        }
    }
}
