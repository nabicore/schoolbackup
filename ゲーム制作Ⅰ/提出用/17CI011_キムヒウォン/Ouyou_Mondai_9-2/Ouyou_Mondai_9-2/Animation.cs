﻿using System.Windows.Forms;
using System.Drawing;


public class Animation
{
    string fileName;
    string extension;
    int tick = 0;
    int delay = 0;
    int curImg = 0;
    int maxImg = 0;
    
    public Animation(int delay, int maxImg, string fileName, string ext)
    {
        this.delay = delay;
        this.fileName = fileName;
        this.extension = ext;
        this.maxImg = maxImg;
    }

    public void Update(PictureBox target)
    {
        tick++;
        if (tick >= delay)
        {
            tick = 0;
            if (curImg < maxImg)
                curImg++;
            else
                curImg = 1;
            target.Image = Image.FromFile(fileName + curImg.ToString() + extension);
        }
    }
}