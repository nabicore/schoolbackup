﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_48
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] number = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
            int total = 0;
            label1.Text = "";


            foreach (var n in number)
            {
                total += n;
                label1.Text += n.ToString() + "\n";
            }
            label1.Text += "合計：" + total.ToString();
        }
    }
}
