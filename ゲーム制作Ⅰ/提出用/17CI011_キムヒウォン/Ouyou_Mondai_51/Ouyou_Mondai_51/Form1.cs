﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_51
{
    public partial class Form1 : Form
    {
        string[] data;
        Random ran = new Random();
        int seiseki = 0;
        int erCount = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //　文字データの初期化　（すみません）
            data = new string[('Z' + 1 - 'A') + ('9' + 1 - '0')];
            int initCnt = 0;
            while (initCnt < data.Length)
            {
                for (char c = 'A'; c <= 'Z'; ++c)
                {
                    data[initCnt] = c.ToString();
                    initCnt++;
                }
                for (char c = '0'; c <= '9'; ++c)
                {
                    data[initCnt] = c.ToString();
                    initCnt++;
                }
            }

            //　ボードの初期化
            label1.BorderStyle = BorderStyle.Fixed3D;
            label1.Text = "";
            label2.BorderStyle = BorderStyle.Fixed3D;
            label2.Text = "";
            Scoreboard();

            //　タイマの初期化
            timer1.Interval = 1000;
            timer1.Tick += new EventHandler((o, eh) => label1.Text = data[ran.Next(0, data.Length)].ToString());
            timer1.Start();
        }

        //　入力当たり判定
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            char input = (char)e.KeyCode;
            label2.Text = input.ToString();

            // 　正解
            if (input.ToString() == label1.Text)
            {
                seiseki++;
                if (seiseki % 10 == 0)
                    timer1.Interval -= 100;
            }
            else
            {
                erCount++;
            }

            label2.Text = "";
            label1.Text = data[ran.Next(0, data.Length)].ToString();
            Scoreboard();
        }

        //　点数表示更新
        void Scoreboard()
        {
            label3.Text = "得点：" + seiseki.ToString() + "　　　　　" + "エラー回数：" + erCount.ToString();
            if (seiseki >= 100)
                label3.Text += "\nゲームクリア";
        }
    }
}
