﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ouyou_Mondai_52
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int a = int.Parse(textBox1.Text);
                int b = int.Parse(textBox2.Text);
                label3.Text = (a / b).ToString();
            }
           

            catch(System.DivideByZeroException ex)
            {
                label3.Text = "０で除算しようとしました。";
            }
            
            catch(System.FormatException ex)
            {
                label3.Text = "入力文字列の形式が正しくありません。";
            }
        }
    }
}
