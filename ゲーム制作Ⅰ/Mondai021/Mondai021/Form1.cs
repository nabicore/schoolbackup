﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai021
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            int addNum = 0;
            if (radioButton1.Checked)
                addNum = 8;
            if (radioButton2.Checked)
                addNum = 4;
            if (radioButton3.Checked)
                addNum = 2;
            if (radioButton4.Checked)
                addNum = 1;

            label4.Text = (int.Parse(label4.Text) + addNum).ToString();

        }
    }
}
