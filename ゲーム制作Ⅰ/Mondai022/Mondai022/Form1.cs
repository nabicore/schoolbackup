﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai022
{
    public partial class Form1 : Form
    {
        Random rand = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string output = "発生した乱数：";
            int curRan = rand.Next(1, 32);
            output += curRan.ToString();
            if ((curRan % 2 != 0 && radioButton1.Checked) || (curRan % 2 == 0 && radioButton2.Checked))
                output += "あたり";
            else
                output += "はずれ";

            label1.Text = output;
        }
    }
}
