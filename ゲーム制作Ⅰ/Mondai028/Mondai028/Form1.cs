﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai028
{
    public partial class Form1 : Form
    {
        readonly int[] japan_to_west = { 1868, 1912, 1926, 1989 };

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result = japan_to_west[comboBox1.SelectedIndex] + int.Parse(textBox1.Text);
            label3.Text = comboBox1.SelectedItem.ToString() + textBox1.Text + "年は、西暦" + result.ToString() + "年です";
        }
    }
}
