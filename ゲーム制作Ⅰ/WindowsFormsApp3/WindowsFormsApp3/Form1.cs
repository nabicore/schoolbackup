﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        private class Rect
        {
            public int x;
            public int y;
            public int w;
            public int h;

            public bool isCollide(Rect t)
            {
                return this.x < t.x + t.w && this.x + this.w < t.x &&
                       this.y < t.x + t.h && this.y + this.h < t.y;
            }

            public bool isCollide(Point p)
            {
                return this.x < p.X && p.X < this.x + this.w &&
                       this.y < p.Y && p.Y < this.y + this.h;
            }
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Rect ballRect = new Rect
            {
                x = pictureBox1.Location.X,
                y = pictureBox1.Location.Y,
                w = pictureBox1.Size.Width,
                h = pictureBox1.Size.Height
            };

            Random r = new Random();
            float angle = r.Next(0, 180);
            Point direction = new Point { X = 10, Y = 0 };

            if (ballRect.x < 0)                         direction = new Point { X = (int)Math.Cos(angle)  * 10, Y = (int)Math.Sin(angle)  * 10 };
            if (ballRect.x > this.Size.Width)           direction = new Point { X = -(int)Math.Cos(angle) * 10, Y = (int)Math.Sin(angle)  * 10 };
            if (ballRect.y < 0)                         direction = new Point { X = (int)Math.Cos(angle)  * 10, Y = (int)Math.Sin(angle)  * 10 };
            if (ballRect.y < this.Size.Height)          direction = new Point { X = (int)Math.Cos(angle)  * 10, Y = -(int)Math.Sin(angle) * 10  };
            pictureBox1.Location = new Point(pictureBox1.Location.X + direction.X, pictureBox1.Location.Y + direction.Y);
        }
    }
}
