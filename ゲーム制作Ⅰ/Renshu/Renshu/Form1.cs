﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Renshu
{
    public class Bullet
    {
        PictureBox bulletImg = new PictureBox();
        int bulletSpeed = 10;

        public Bullet(Point playerPos)
        {
            bulletImg.Image = Image.FromFile("beam01.gif");
            bulletImg.Location = playerPos;


            System.Diagnostics.Debug.WriteLine("Generated :: Bullet");

        }

        ~Bullet()
        {
            
        }

        public void Fire()
        {
            bulletImg.Location += new Size(0, -bulletSpeed);
        }
    }


    public partial class Form1 : Form
    {
        List<Bullet> bullets = new List<Bullet>();

        public Form1()
        {
            InitializeComponent();
            Timer timer = new Timer();
            timer.Tick += new EventHandler(Update);
            timer.Start();
        }

        private void Update(object sender, EventArgs e)
        {
            foreach (var bullet in bullets)
                bullet.Fire();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            keyPress(e.KeyCode);
        }

        void keyPress(Keys e)
        {
            Point playerLoc = Player.Location;
            switch (e)
            {
                case Keys.Left:     //左移動
                    Player.Location = playerLoc + new Size(-1, 0);
                    break;
                case Keys.Right:    //右移動
                    Player.Location = playerLoc + new Size(1, 0);
                    break;
                case Keys.Up:       //上移動
                    Player.Location = playerLoc + new Size(0, -1);
                    break;
                case Keys.Down:     //下移動
                    Player.Location = playerLoc + new Size(0, 1);
                    break;

                case Keys.Space:    //攻撃
                    Bullet bullet = new Bullet(Player.Location);
                    bullets.Add(bullet);
                    break;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }
    }
}
