﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai024
{
    public partial class Form1 : Form
    {
        CheckBox[] pokemon;
        int totalCount = 0;

        public Form1()
        {
            InitializeComponent();

            pokemon = new CheckBox[]
            {
                checkBox1,
                checkBox2,
                checkBox3,
                checkBox4,
                checkBox5,
                checkBox6,
                checkBox7
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = "";
            foreach (var p in pokemon)
            {
                if(p.Checked)
                    label2.Text += p.Text + "\n";
            }
        }

        private void pokemonChecked(object sender, EventArgs e)
        {
            var checker = (CheckBox)sender;

            if (checker.Checked)
            {
                if (totalCount >= 3)
                    checker.Checked = false;
                totalCount += 1;
            }
            else
                totalCount -= 1;
        }
    }
}
