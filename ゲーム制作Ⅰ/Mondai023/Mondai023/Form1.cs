﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mondai023
{
    public partial class Form1 : Form
    {
        class Goods
        {
            CheckBox checkBox;
            int price;

            public Goods(CheckBox c, int p)
            {
                checkBox = c;
                price = p;
            }

            public bool Checked { get { return checkBox.Checked; } }
            public int Price { get { return price; } }
        }
        Goods[] goods;

        public Form1()
        {
            InitializeComponent();
            goods = new Goods[6]
            {
                new Goods(checkBox1, 880),
                new Goods(checkBox2, 450),
                new Goods(checkBox3, 1240),
                new Goods(checkBox4, 5370),
                new Goods(checkBox5, 5220),
                new Goods(checkBox6, 620)
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int total = 25000;
            foreach (var g in goods)
                if (g.Checked)
                    total += g.Price;
            label4.Text = total.ToString("c");
        }
    }
}
