﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_5_6_OR_NEWER
public class MMD4MecanimPreProcessBuild : UnityEditor.Build.IPreprocessBuild
{
    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        if( target == BuildTarget.Android ) {
            MMD4MecanimEditorCommon.CheckAndroidDevices( true );
        }
    }
}
#endif