﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Murtaorune : MonoBehaviour {

    public float power = 500f;
    public GameObject prefab;
    public Vector3 direction;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var obj = GameObject.Instantiate(prefab);
            obj.transform.position = this.transform.position;
            obj.transform.rotation = this.transform.rotation;
            obj.GetComponent<Rigidbody>().AddForce(direction * power);
        }
    }
}
