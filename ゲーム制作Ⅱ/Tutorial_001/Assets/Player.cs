﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Rigidbody rigid;
    public float jumpPower = 8f;

    bool isJumping = false;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(1) && !isJumping)
        {
            rigid.AddForce(Vector3.up * jumpPower);
            isJumping = true;
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        isJumping = false;
    }
}
