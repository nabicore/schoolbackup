﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallInChecker : MonoBehaviour {

    public Hole[] holes;

    
    void OnGUI() {
        string label = " ";

        bool isFallIn = false;
        foreach (var h in holes)
            isFallIn = h.IsFallIn();

        if (isFallIn)
            label = "Fall In Hole!";
        GUI.Label(new Rect(0, 0, 100, 30), label);
    }
}
