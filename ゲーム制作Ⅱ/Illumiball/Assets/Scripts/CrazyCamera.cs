﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazyCamera : MonoBehaviour {

    public Camera target;

	void Update () {
        target.transform.position = new Vector3(
            Random.Range(-2f, 2f),
            target.transform.position.y,
            Random.Range(-2f, 2f)); 
	}
}
