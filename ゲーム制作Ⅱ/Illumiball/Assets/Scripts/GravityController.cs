﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityController : MonoBehaviour {

    const float Gravity = 9.81f;
    public float gravityScale = 1f;
	
	void Update () {
        Vector3 vector = new Vector3(
            Input.GetAxis("Horizontal"),
            0, 
            Input.GetAxis("Vertical"));

        if (Application.isEditor)
        {
            if (Input.GetKey("z"))
                vector.y = 1f;
            else
                vector.y = -1f;
        }

        else
            vector = Input.acceleration;

        Physics.gravity = Gravity * vector.normalized * gravityScale;
	}
}
