﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallInChecker : MonoBehaviour {

    public Hole[] holes;
    public Texture win;
    public CrazyCamera crazyCam;
    public GameObject backPanel;
    public Material clearBack;
    public AudioSource panpare;

    
    void OnGUI() {
        string label = " ";

        int isFallIn = 0;
        foreach (var h in holes)
            if (h.IsFallIn())
                isFallIn += 1;

        if (isFallIn >= holes.Length) {
            crazyCam.enabled = true;
            panpare.Play();
            backPanel.GetComponent<Renderer>().material.mainTexture = win;
            label = "Fall In Hole!";
        }
        GUI.Label(new Rect(250, 100, 300, 100), label);
    }
}
