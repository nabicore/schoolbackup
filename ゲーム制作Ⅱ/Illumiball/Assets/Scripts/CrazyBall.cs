﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazyBall : MonoBehaviour {

    public Material ballMat;
	void Update () {
		ballMat.color =
            new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }
}
