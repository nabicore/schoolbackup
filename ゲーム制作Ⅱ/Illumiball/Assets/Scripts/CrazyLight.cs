﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazyLight : MonoBehaviour {

    public Light myLight;

	void Update () {
        myLight.color =
            new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }
}
