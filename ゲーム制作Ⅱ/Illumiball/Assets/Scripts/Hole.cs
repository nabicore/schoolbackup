﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour {

    bool fallIn;
    public string activeTag;

    public bool IsFallIn() {
        return fallIn;
    }



    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag.Equals(activeTag))
            fallIn = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals(activeTag))
            fallIn = false;
    }

    void OnTriggerStay(Collider other) {

        Rigidbody r = other.gameObject.GetComponent<Rigidbody>();

        Vector3 direction = this.transform.position - other.transform.position;
        direction.Normalize();

        if (other.gameObject.tag.Equals(activeTag))
        {
            r.velocity *= 0.9f;
            r.AddForce(direction * r.mass * 20f);
        }
        else
            r.AddForce(-direction * r.mass * 80f);
    }

}
