﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public NejikoController nejiko;
    public Text scoreLabel;
    public LifePanel lifePanel;


    private void Update()
    {
        int score = CalcScore();
        scoreLabel.text = "Score : " + score + "m";
        lifePanel.UpdateLife(nejiko.Life());

        if (nejiko.Life() <= 0) {
            enabled = false;
            Invoke("ReturnToTitle", 2f);
        }
    }

    int CalcScore()
    {
        return (int)nejiko.transform.position.z;
    }

    void ReturnToTitle()
    {
        Application.LoadLevel("Title");
    }
}
