using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; 

public class Key_SceneLoad : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey ("escape")) {
			Application.Quit();
		}
		if(Input.GetButton("Jump")) {
			SceneManager.LoadScene("Scene_Game");
		}
	}
}
