﻿using UnityEngine;
using System.Collections;

public class Player_KeyMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 vel = new Vector3();
		//方向キー操作
		vel.x = Input.GetAxis("Horizontal");
		vel.z = Input.GetAxis("Vertical");

        Rigidbody rig = this.GetComponent<Rigidbody>();

        rig.velocity = vel * 5.0f;
        this.transform.LookAt(this.transform.position + rig.velocity);
    }
}
