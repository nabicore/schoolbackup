﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player_HitCheck : MonoBehaviour {
	public GameObject	GoalEffect;
	public Image 		GameOverImage;
	public Image		GameClearImage;
	public bool			is_Goal = false;		//ゴールしたかのフラグ
	public bool			is_GameOver = false;	//ゲームオーバーになったかのフラグ
	public AudioSource	BGM;
	public GameObject	SE_Goal;
	public GameObject	SE_GameOver;

	// Use this for initialization
	void Start () {
		GameOverImage.enabled = false; 
		GameClearImage.enabled = false;
		is_Goal = false;
		is_GameOver = false;
	}
	
	//何かと接触した時に呼び出される
	void OnCollisionEnter(Collision collision)
	{
		//ゴールとプレイヤーが触れた（ゲームオーバー後でない）
		if (!is_GameOver && collision.gameObject.tag == "Goal") {
			if(!is_Goal){   //ゴールした直後に一度だけ発動
                if (GoalEffect != null)     Instantiate(GoalEffect, this.transform.position , Quaternion.Euler(new Vector3(-90 , 0 , 0)));
                if (SE_Goal != null)        Instantiate(SE_Goal);
				if (BGM != null)            BGM.Stop();
			}
			is_Goal = true;
			GameClearImage.enabled = true;
		}
		//ハンターとプレイヤーが触れた（ゴール後でない）
		if (!is_Goal && collision.gameObject.tag == "Hunter") {
			if(!is_GameOver){   //ゲームオーバー直後に一度だけ発動
                if (SE_GameOver != null)    Instantiate(SE_GameOver);
                if (BGM != null)            BGM.Stop();
			}
			is_GameOver = true;
			GameOverImage.enabled = true; 
		}
	}

	//毎フレーム描画される
	void OnGUI(){
		if (is_Goal) {
			//ゴールした時に文字を表示
			GUI.Label(new Rect(Screen.width/2 , Screen.height/2 , 100 , 20) , "GOAL!!");
		}
		else if (is_GameOver) {
			//ゲームオーバーした時に文字を表示
			GUI.Label(new Rect(Screen.width/2 , Screen.height/2 , 100 , 20) , "GameOver");
		}
	}
}
