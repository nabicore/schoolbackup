﻿using UnityEngine;
using System.Collections;

public class Hunter : MonoBehaviour {
	GameObject player;

	// Use this for initialization
	void Start () {
		//Playerタグのついているオブジェクトを探し、それを追うようにする
		player = GameObject.FindWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		UnityEngine.AI.NavMeshAgent agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		agent.destination = player.transform.position;
	}
}
