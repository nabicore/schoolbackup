using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Text;

public class Launcher_ver1 : MonoBehaviour {
    
	public GameObject	WallPrefab;
	public GameObject	HunterPrefab;

    //デフォルトマップ
    private int mapWidth = 15;
	private int mapLength = 15;
	private char[,] mapData = {
		{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
		{'1','h','0','0','0','0','0','0','1','0','0','0','0','0','1'},
		{'1','0','1','1','1','1','1','0','1','0','1','1','1','0','1'},
		{'1','0','1','0','0','0','1','0','0','0','1','0','0','0','1'},
		{'1','0','1','0','1','0','1','0','1','1','1','0','1','0','1'},
		{'1','0','1','0','1','0','0','0','0','0','0','0','1','0','1'},
		{'1','0','0','0','0','0','1','0','1','1','1','0','1','0','1'},
		{'1','0','1','1','1','0','1','0','1','0','0','0','1','0','1'},
		{'1','0','1','0','0','0','0','0','1','0','1','0','1','0','1'},
		{'1','0','1','0','1','0','1','1','1','1','1','0','0','0','1'},
		{'1','0','1','1','1','0','1','0','0','0','0','0','1','0','1'},
		{'1','0','1','s','0','0','1','0','0','0','1','g','1','0','1'},
		{'1','0','1','0','1','1','1','1','1','1','1','0','1','0','1'},
		{'1','0','0','0','0','0','0','0','0','0','0','0','0','h','1'},
		{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
	};

    //壁を設置
    private void SetWalls()
    {
        Vector3 vec = Vector3.zero;
        for (int z = 0; z < mapLength; z++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                //該当マスの位置を先に計算しておく
                vec.Set(x * 1.0f - mapWidth * 0.5f
                    , 0.5f
                    , -(z * 1.0f - mapLength * 0.5f));

                switch (mapData[z, x])
                {
                    case '0':   //通路（何もしない）
                        break;
                    case '1':   //壁ブロックの生成
                        Instantiate(this.WallPrefab, vec, Quaternion.identity);
                        break;
                    case 'h':   //ハンターの出現
                        Instantiate(this.HunterPrefab, vec, Quaternion.identity);
                        break;
                    case 's':   //プレイヤーの初期位置
                        GameObject player = GameObject.FindWithTag("Player");
                        player.transform.position = vec;
                        break;
                    case 'g':   //ゴール位置
                        GameObject goal = GameObject.FindWithTag("Goal");
                        goal.transform.position = vec;
                        break;
                }
            }
        }
    }

    //生成された初回だけ呼び出される
    void Start () {
		SetWalls();
	}
}
