﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageGenerator : MonoBehaviour {
    const int StageTipSize = 30;

    int currentTipIndex;

    public Transform character;
    public GameObject[] stageTips;
    public int startTipIndex;
    public int preInstantiate;
    public List<GameObject> generatedStageList;

    private void Start()
    {
        generatedStageList = new List<GameObject>();
        currentTipIndex = startTipIndex - 1;
    }

    private void Update()
    {
        int charaPositionIndex = (int)(character.position.z / StageTipSize);
        if (charaPositionIndex + preInstantiate > currentTipIndex)
            UpdateStage(charaPositionIndex + preInstantiate);
    }


    void UpdateStage(int toTipIndex)
    {
        if (toTipIndex <= currentTipIndex)
            return;

        for (int i = currentTipIndex + 1; i <= toTipIndex; ++i) {
            GameObject stageObject = GenerateStage(i);
            generatedStageList.Add(stageObject);
        }

        while (generatedStageList.Count > preInstantiate + 2)
            DestroyOldestStage();
        currentTipIndex = toTipIndex;
    }

    GameObject GenerateStage(int tipIdx)
    {
        int nextStageTip = Random.Range(0, stageTips.Length);
        GameObject stageObject = (GameObject)Instantiate(
            stageTips[nextStageTip],
            new Vector3(0f, 0f, tipIdx * StageTipSize),
            Quaternion.identity
            );

        return stageObject;
    }

    void DestroyOldestStage()
    {
        GameObject oldStage = generatedStageList[0];
        generatedStageList.RemoveAt(0);
        Destroy(oldStage);
    }
}
