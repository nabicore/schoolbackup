﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFollow : MonoBehaviour {

    Vector3 diff;

    public GameObject target;
    public float followSpeed;

    private void Start()
    {
        diff = target.transform.position - this.transform.position;
    }

    private void LateUpdate()
    {
        this.transform.position = Vector3.Lerp(
            this.transform.position,
            target.transform.position - diff,
            Time.deltaTime * followSpeed);
    }
}
