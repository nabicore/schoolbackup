﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NejikoController : MonoBehaviour {

    const int MinLane = -2;
    const int MaxLane = 2;
    const float LaneWidth = 1f;

    delegate void InputTrigger();
    CharacterController controller;
    Animator animator;
    Vector3 moveDirection = Vector3.zero;
    Dictionary<string, InputTrigger> inputs;
    int targetLane;

    public float gravity;
    public float speedX;
    public float speedZ;
    public float speedJump;
    public float accelerationZ;


    private void Awake()
    {
        controller = this.GetComponent<CharacterController>();
        animator = this.GetComponent<Animator>();

        inputs = new Dictionary<string, InputTrigger>();
        inputs.Add("left", () => { if (controller.isGrounded && targetLane > MinLane) targetLane -= 1; });
        inputs.Add("right", () => { if (controller.isGrounded && targetLane < MaxLane) targetLane += 1; });
        inputs.Add("space", () => {
            if (controller.isGrounded) {
                moveDirection.y = speedJump;
                animator.SetTrigger("jump");
            }
        });
    }

    private void Update()
    {
        foreach (var i in inputs)
            if (Input.GetKeyDown(i.Key))
                i.Value();

        float acceleratedZ = moveDirection.z + (accelerationZ * Time.deltaTime);
        moveDirection.z = Mathf.Clamp(acceleratedZ, 0, speedZ);

        float ratioX = (targetLane * LaneWidth - transform.position.x) / LaneWidth;
        moveDirection.x = ratioX * speedX;
        
        moveDirection.y -= gravity * Time.deltaTime;

        Vector3 globalDirection = transform.TransformDirection(moveDirection);
        controller.Move(globalDirection * Time.deltaTime);

        if (controller.isGrounded)
            moveDirection.y = 0f;
        animator.SetBool("run", moveDirection.z > 0f);
    }
}
