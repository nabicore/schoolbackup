﻿
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FallInChecker : MonoBehaviour 
{
	public Hole red;
	public Hole blue;
	public Hole green;
    public GameObject clearEffect;
    public AudioSource clearSE;
    

	void OnGUI()
	{
		string label = " ";

		// すべてのボールが入ったらラベルを表示
		if (red.IsFallIn() && blue.IsFallIn() && green.IsFallIn() && !clearEffect.activeSelf)
		{
            label = "Fall in hole!";
            StartCoroutine("Clear");
        }

        GUI.Label (new Rect(0,0,100,30), label);
	}

    IEnumerator Clear()
    {
        clearEffect.SetActive(true);
        clearSE.PlayOneShot(clearSE.clip);
        yield return new WaitForSeconds(5f);
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);
    }
}
