﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour {

    ParticleSystem particle;

	void Awake () {
        particle = this.GetComponent<ParticleSystem>();
	}
	

	void Update () {
        if (!particle.isPlaying)
            Destroy(this.gameObject);
	}
}
