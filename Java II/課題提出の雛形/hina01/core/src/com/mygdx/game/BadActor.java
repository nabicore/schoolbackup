package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BadActor extends Actor {
    Texture texture;
    Sprite sp;
    float x;

    BadActor() {
        texture = new Texture("badlogic.jpg");
        sp = new Sprite(texture);
    }

    @Override
    public void act(float dt) {
        x += 100 * dt; //1秒間に100px右に移動

    }

    @Override
    public void draw(Batch batch, float alpha) {
        sp.setPosition(x, 100);
        sp.draw(batch);
    }
}
