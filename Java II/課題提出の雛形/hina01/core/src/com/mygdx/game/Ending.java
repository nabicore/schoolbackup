package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Ending extends ScreenAdapter{
    SpriteBatch batch;
    Texture img;
    Game game;
    float time;

    Ending(Game game) {
        this.game = game;
        batch = new SpriteBatch();
    }

    @Override
    public void show() {
        img = new Texture("ending.png");
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(img, 0, 0);
        batch.end();
        time += Gdx.graphics.getDeltaTime();
        if (Gdx.input.isTouched() && time > 1) {
            game.setScreen(new Opening(game));
        }
    }

}
