package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Main extends ScreenAdapter{
    SpriteBatch batch;
    Game game;
    float time, dt;
    private Stage stage;

    Main(Game game) {
        this.game = game;
        batch = new SpriteBatch();
        stage = new Stage();
        stage.addActor(new BackActor());
        stage.addActor(new BadActor());
        stage.scrolled(10);
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        dt=Gdx.graphics.getDeltaTime();
        stage.act(dt);
        stage.draw();
        batch.end();
        time += dt;
        if (Gdx.input.isTouched() && time > 1) {
            stage.clear();
            game.setScreen(new Ending(game));
        }
    }
}
