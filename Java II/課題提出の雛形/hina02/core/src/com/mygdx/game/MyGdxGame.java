package com.mygdx.game;

import com.badlogic.gdx.Game;

public class MyGdxGame extends Game {
    @Override
    public void create() {
        setScreen(new Opening(this));
    }

    @Override
    public void render() {
        super.render();
    }
}
