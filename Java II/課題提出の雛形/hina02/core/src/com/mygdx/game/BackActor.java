package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Norihisa Ishida on 2016/02/07.
 */
public class BackActor extends Actor {
    Texture texture;
    Sprite sp;

    BackActor() {
        texture = new Texture("main.png");
        sp = new Sprite(texture);
    }

    @Override
    public void act(float dt){
    }

    @Override
    public void draw(Batch batch, float alpha) {
        sp.draw(batch);
    }
}
