package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Opening extends ScreenAdapter {
    private final BitmapFont font;
    SpriteBatch batch;
    Sprite sp;
    Texture img;
    Game game;
    float time;

    Opening(Game game) {
        this.game = game;
        batch = new SpriteBatch();
        font = new BitmapFont();
        img = new Texture("opening.png");
        sp = new Sprite(img);
        sp.setBounds(0f,0f,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        sp.draw(batch);
        font.draw(batch,"Hello GDX!",300,300);
        batch.end();
        time += Gdx.graphics.getDeltaTime();
        if (Gdx.input.isTouched() && time > 1) {
            game.setScreen(new Main(game));
        }
    }
}
