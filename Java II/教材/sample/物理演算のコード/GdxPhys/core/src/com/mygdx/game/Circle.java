package com.mygdx.game;

import com.badlogic.gdx.physics.box2d.CircleShape;

public class Circle extends PhysBody {
    private final float ratio, scale;
    public Circle(String imageFile,float scale,float ratio) {
        super(imageFile,scale,ratio);
        this.ratio = ratio;
        this.scale=scale;
    }
    @Override
    protected void setShape(){
        shape = new CircleShape();
        ((CircleShape)shape).setRadius(sp.getWidth()/2*ratio*scale);
    }
}


