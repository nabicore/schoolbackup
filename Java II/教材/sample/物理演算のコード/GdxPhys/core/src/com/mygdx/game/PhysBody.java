package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

abstract public class PhysBody {
    protected final float unit;
    protected Texture image;
    protected Sprite sp = null;
    private float x,y;
    protected float density, restitution, friction;
    private Body body;
    private Fixture fixture;
    Shape shape;

    public PhysBody(String imageFile, float scale, float ratio) {
        this.unit= 1.0f/ratio;
        image = new Texture(imageFile);
        sp =new Sprite(image);
        sp.setScale(scale);
    }
    public PhysBody(float ratio){
        this.unit = 1.0f/ratio;
    }
    void setPosition(float x, float y){
        this.x = x;
        this.y = y;
    }
    void setAttribution(float density, float restitution, float friction){
        this.density = density;
        this.restitution = restitution;
        this.friction = friction;
    }
    abstract protected void setShape();
    protected void setBody(World world, BodyDef.BodyType type){
        BodyDef bd = new BodyDef();
        bd.type = type;
        bd.position.set(x,y);
        body = world.createBody(bd);
    }
    protected void setFixture(){
        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.density = density;
        fd.restitution = restitution;
        fd.friction = friction;
        fixture = body.createFixture(fd);
    }
    Body createBody(World world, BodyDef.BodyType type){
        setShape();
        setBody(world,type);
        setFixture();
   //     shape.dispose();
        return body;
    }
    Body getBody(){
        return body;
    }
    Fixture getFixture(){
        return fixture;
    }
    public void update(){
        sp.setPosition((body.getPosition().x * unit)-sp.getWidth() /2,
                (body.getPosition().y * unit)-sp.getHeight()/2);
        sp.setRotation(MathUtils.radiansToDegrees*body.getAngle());
    }
    public void draw(Batch batch){
        sp.draw(batch);
    }
    public void dispose(){
        image.dispose();
    }
}
