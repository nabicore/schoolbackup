package com.mygdx.game;

import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Box extends PhysBody{
    private final float ratio, scale;
    private float w,h;
    public Box(String imageFile,float scale,float ratio) {
        super(imageFile,scale,ratio);
        this.ratio = ratio;
        this.scale=scale;
    }
    public Box(float w, float h, float scale, float ratio){
        super(ratio);
        this.ratio = ratio;
        this.scale=scale;
        this.w = w;
        this.h = h;
    }
    @Override
    protected void setShape(){
        shape = new PolygonShape();
        if(sp!=null)
            ((PolygonShape)shape).setAsBox(sp.getWidth()/2*ratio*scale , sp.getHeight()/2 *ratio*scale);
        else ((PolygonShape)shape).setAsBox(w/2*scale , h/2*scale);
    }
}
