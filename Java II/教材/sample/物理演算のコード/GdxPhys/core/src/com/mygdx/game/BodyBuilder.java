package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class BodyBuilder {
    World world;
    float ratio;
    public BodyBuilder(World world, float ratio) {
        this.world = world;
        this.ratio = ratio;
    }
    //Box
    Box createDynamicBox(String imageFile, float scale,float x, float y){
        Box box = new Box(imageFile,scale,ratio);
        box.setPosition(x,y);
        box.setAttribution(1.0f, 0.5f, 0.5f);
        box.createBody(world, BodyDef.BodyType.DynamicBody);
        return box;
    }
    Box createDynamicBox(String imageFile, float scale,float x, float y,float restitution, float friction){
        Box box = new Box(imageFile,scale,ratio);
        box.setPosition(x,y);
        box.setAttribution(1.0f, 0.5f, 0.5f);
        box.createBody(world, BodyDef.BodyType.DynamicBody);
        return box;
    }
    Box createStaticBox(String imageFile, float scale,float x, float y){
        Box box = new Box(imageFile,scale,ratio);
        box.setPosition(x,y);
        box.setAttribution(1.0f, 0.5f, 0.5f);
        box.createBody(world, BodyDef.BodyType.StaticBody);
        return box;
    }
    Box createStaticBox(float scale,float ratio, float x, float y, float w, float h){
        Box box = new Box(w,h,scale,ratio);
        box.setPosition(x,y);
        box.setAttribution(1.0f, 0.5f, 0.5f);
        box.createBody(world, BodyDef.BodyType.StaticBody);
        return box;
    }
    //Circle
    Circle createDynamicCircle(String imageFile, float scale,float x, float y){
        Circle circle = new Circle(imageFile,scale,ratio);
        circle.setPosition(x,y);
        circle.setAttribution(1.0f, 0.5f, 0.5f);
        circle.createBody(world, BodyDef.BodyType.DynamicBody);
        return circle;
    }
    //Chain
/*    Chain createStaticChain(Vector2[] pos, Color color){
        Chain chain = new Chain(ratio,pos,color);
        chain.setPosition(pos[0].x,pos[0].y);
        chain.setAttribution(1.0f,0.5f,0.5f);
        chain.createBody(world, BodyDef.BodyType.StaticBody);
        return chain;
    }*/

    //Edge
    Edge createStaticEdge(float x0, float y0, float x1, float y1, Color color){
        Edge edge = new Edge(ratio,x0,y0,x1,y1,color);
        edge.setPosition((x0+x1)/2,(y0+y1)/2);
        edge.setAttribution(1.0f,0.5f,0.5f);
        edge.createBody(world, BodyDef.BodyType.StaticBody);
        return edge;
    }
    Edge createStaticEdge(String imageFile, float scale, float x, float y, Color color){
        Edge edge = new Edge(imageFile,scale,ratio);
        edge.setPosition(x,y);
        edge.setAttribution(1.0f,0.0f,0.5f);
        edge.createBody(world, BodyDef.BodyType.StaticBody);
        return edge;
    }
    //JPoly
    JPoly createDynamicPoly(String jsonFile, String key, float scale, float x, float y){
        JPoly jp = new JPoly(jsonFile,key,scale,ratio);
        jp.setPosition(x,y);
        jp.setAttribution(1.0f,0.5f,0.5f);
        jp.createBody(world, BodyDef.BodyType.DynamicBody);
        return jp;
    }
}
