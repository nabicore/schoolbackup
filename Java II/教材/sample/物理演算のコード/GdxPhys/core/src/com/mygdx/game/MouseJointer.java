package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;

import java.util.ArrayList;


public class MouseJointer {
    private final World world;
    MouseJoint mj=null;
    MouseJointDef mjd;
    Box anchor;
    float ratio;
    MouseJointer(World world, float ratio){
        this.world = world;
        this.ratio = ratio;
        BodyBuilder bb= new BodyBuilder(world,0.01f);
        anchor = bb.createStaticBox(1.0f,ratio,0,0,1,1);
        mjd = new MouseJointDef();
    }
    void mouseJoint(Body target){
        if (target==null) return;
        if (Gdx.input.isTouched()){
            if (mj == null){
                mjd.bodyA = anchor.getBody();
                mjd.bodyB = target;
                mjd.maxForce = 10000.0f*target.getMass();
                mjd.target.set(Gdx.input.getX()*ratio, (Gdx.graphics.getHeight()-Gdx.input.getY())*ratio);
                mj = (MouseJoint)world.createJoint(mjd);
                target.setAwake(true);
            }
            mj.setTarget(new Vector2(Gdx.input.getX()*ratio, (Gdx.graphics.getHeight()-Gdx.input.getY())*ratio));
        }
        else{
            if (mj != null){
                world.destroyJoint(mj);
                mj = null;
            }
        }
    }
    void mouseJoint(){
        mouseJoint(getBodyOnMouse());
    }
    Body getBodyOnMouse(){
        float mx=Gdx.input.getX()*ratio;
        float my=(Gdx.graphics.getHeight()-Gdx.input.getY())*ratio;
        float xa = mx - ratio/100;
        float ya = my - ratio/100;
        float xb = mx + ratio/100;
        float yb = my + ratio/100;
        FindBody findbody = new FindBody();
        world.QueryAABB(findbody,Math.min(xa,xb),Math.min(ya,yb),Math.max(xa,xb),Math.max(ya,yb));
        return findbody.getBody();
    }
    private class FindBody implements QueryCallback{
        Body body;
        @Override
        public boolean reportFixture(Fixture fixture) {
            body = fixture.getBody();
            return false;
        }
        Body getBody(){
            return body;
        }
    }
    MouseJoint getJoint() {
        return mj;
    }
}
