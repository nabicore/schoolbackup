package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	World world;
	Box box;
	Circle c;
	Edge edge;
	JPoly ok;
	MouseJointer mj;
	final float ratio = 0.01f;//スクリーンと物理座標の比

	@Override
	public void create () {
		batch = new SpriteBatch();
		world = new World(new Vector2(0,-9.8f),true);

		BodyBuilder bb = new BodyBuilder(world,ratio);

		box = bb.createDynamicBox("badlogic.jpg",1f,4,5);
		c = bb.createDynamicCircle("BeachBall.png",1f,7,5);
		ok = bb.createDynamicPoly("obstacle1.json","OK",0.5f,2,5);
		edge = bb.createStaticEdge(0,0.5f, 10,0.5f,Color.BLUE);

		mj = new MouseJointer(world, ratio);

		DistanceJointer dj = new DistanceJointer(world, ratio);
		dj.joint(ok.getBody(),box.getBody(),3,true);
	}

	@Override
	public void render () {
		world.step(Gdx.graphics.getDeltaTime(), 6, 2);
		box.update();
		c.update();
		edge.update();
		ok.update();
		mj.mouseJoint();
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		box.draw(batch);
		c.draw(batch);
		edge.draw(batch);
		ok.draw(batch);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
