package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class JPoly extends PhysBody {
    BodyEditorLoader loader;
    private final float ratio, scale;
    private String key;
    public JPoly(String jsonFile, String key, float scale, float ratio) {
        super(ratio);
        this.ratio = ratio;
        this.scale=scale;
        this.key = key;
        loader = new BodyEditorLoader(Gdx.files.internal(jsonFile));
        String imageFile=loader.getImagePath(key);
        image = new Texture(imageFile);
        sp =new Sprite(image);
        sp.setScale(scale);
    }

    @Override
    protected void setFixture(){
        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.density = density;
        fd.restitution = restitution;
        fd.friction = friction;
        loader.attachFixture(getBody(),key,fd,sp.getWidth()*ratio*scale);
    }
    @Override
    protected void setShape() {
        //shape = getFixture().getShape();
    }
}
