package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;

public class DistanceJointer {
    private final World world;
    DistanceJoint dj=null;
    DistanceJointDef djd;
    float ratio;
    public DistanceJointer(World world, float ratio) {
        this.world = world;
        this.ratio = ratio;
        djd = new DistanceJointDef();
    }
    void joint(Body targetA, Body targetB, float length, boolean connect){
        if(connect){
            if (dj == null){
                djd.bodyA = targetA;
                djd.bodyB = targetB;
                djd.length = length;
                dj = (DistanceJoint)world.createJoint(djd);
            }
        }
        else{
            if (dj != null){
                world.destroyJoint(dj);
                dj = null;
            }
        }
    }
}
