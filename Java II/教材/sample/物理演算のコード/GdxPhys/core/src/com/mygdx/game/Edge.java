package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.EdgeShape;

public class Edge extends PhysBody {
    private final float ratio, scale;
    private float x0, y0, x1, y1;

    public Edge(String imageFile, float scale, float ratio) {
        super(imageFile, scale, ratio);
        this.ratio = ratio;
        this.scale = scale;
    }

    public Edge(float ratio, float x0, float y0, float x1, float y1,Color color) {
        super(ratio);
        this.ratio = ratio;
        this.scale = 1;
        setPosition(x0,y0,x1,y1);
        Pixmap pixmap = new Pixmap((int)(Math.abs(x1-x0)/ratio)+1, (int)(Math.abs(y1-y0)/ratio)+1, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.drawLine(x1-x0>0?0:(int)((x0-x1)/ratio), y1-y0<0?0:(int)((y1-y0)/ratio),
                x1-x0>0?(int)((x1-x0)/ratio):0, y1-y0<0?(int)((y0-y1)/ratio):0);
        image = new Texture(pixmap);
        sp = new Sprite(image);
    }

    void setPosition(float x, float y) {
        super.setPosition(x - sp.getWidth() / 2 * ratio * scale, y - sp.getHeight() / 2 * ratio * scale);
        if(x0==0&&y0==0&&x1==0&&y1==0)//画像(横棒のみ)の時
            setPosition(x,y + sp.getHeight()/2  * ratio * scale,x+sp.getWidth() * ratio * scale,y + sp.getHeight()/2  * ratio * scale);
    }
    void setPosition(float x0, float y0, float x1, float y1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }
    @Override
    protected void setShape() {
        shape = new EdgeShape();
        ((EdgeShape) shape).set(//0, 0,
                //sp.getWidth() * ratio * scale, sp.getHeight() * ratio * scale);
                x1-x0>0?0:((x0-x1)), y1-y0>0?0:((y0-y1)),
                x1-x0>0?((x1-x0)):0, y1-y0>0?((y1-y0)):0);
    }

    public void update() {
        sp.setPosition(getBody().getPosition().x / ratio,
                getBody().getPosition().y / ratio);
        sp.setRotation(MathUtils.radiansToDegrees * getBody().getAngle());
    }
}
