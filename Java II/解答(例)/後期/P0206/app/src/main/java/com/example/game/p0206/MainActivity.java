package com.example.game.p0206;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {
    Button[][] btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = new Button[3][3];
        btn[0][0] = (Button)findViewById(R.id.button5);
        btn[0][1] = (Button)findViewById(R.id.button4);
        btn[0][2] = (Button)findViewById(R.id.button8);
        btn[1][0] = (Button)findViewById(R.id.button6);
        btn[1][1] = (Button)findViewById(R.id.button3);
        btn[1][2] = (Button)findViewById(R.id.button9);
        btn[2][0] = (Button)findViewById(R.id.button7);
        btn[2][1] = (Button)findViewById(R.id.button2);
        btn[2][2] = (Button)findViewById(R.id.button);

        class Click implements View.OnClickListener {

            @Override
            public void onClick(View v) {
                for(int i=0;i<3;i++){
                    for(int j=0;j<3;j++) {
                        if((Button)v==btn[i][j]){
                            if(btn[i][j].isShown()) btn[i][j].setVisibility(View.INVISIBLE);
                            else btn[i][j].setVisibility(View.VISIBLE);
                            try {
                                if(btn[i-1][j].isShown()) btn[i-1][j].setVisibility(View.INVISIBLE);
                                else btn[i-1][j].setVisibility(View.VISIBLE);
                            }catch(ArrayIndexOutOfBoundsException ae){
                            }
                            try {
                                if(btn[i+1][j].isShown()) btn[i+1][j].setVisibility(View.INVISIBLE);
                                else btn[i+1][j].setVisibility(View.VISIBLE);
                            }catch(ArrayIndexOutOfBoundsException ae){
                            }
                            try {
                                if(btn[i][j-1].isShown()) btn[i][j-1].setVisibility(View.INVISIBLE);
                                else btn[i][j-1].setVisibility(View.VISIBLE);
                            }catch(ArrayIndexOutOfBoundsException ae){
                            }
                            try {
                                if(btn[i][j+1].isShown()) btn[i][j+1].setVisibility(View.INVISIBLE);
                                else btn[i][j+1].setVisibility(View.VISIBLE);
                            }catch(ArrayIndexOutOfBoundsException ae){
                            }
                        }
                    }
                    }
            }
        }

        for(int i=0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                btn[i][j].setOnClickListener(new Click());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
