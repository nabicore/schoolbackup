package com.example.game.p301;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by Norihisa Ishida on 2015/10/17.
 */
public class P0301 extends View {
    int[] color={0xFFFF0000,0xFFFFFF00,0xFF0000FF};
    public P0301(Context context) {
        super(context);
    }
    public void onDraw(Canvas c){
        Paint p = new Paint();
        int i=0;
        for(int col:color) {
            p.setColor(col);
            c.drawCircle(200*(++i),200,80,p);
        }
    }
}
