package com.example.game.p0303;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by Norihisa Ishida on 2015/10/17.
 */
public class P0303 extends View {
    float x,y,t;
    P0303(Context context){
        super(context);
    }
    public void onDraw(Canvas c){
        Paint p= new Paint();
        c.drawCircle(x,y,50,p);
        t += 0.05f;
        x = (float)(200*Math.cos(t)+300);
        y = (float)(200*Math.sin(t)+300);
        invalidate();
    }
}

