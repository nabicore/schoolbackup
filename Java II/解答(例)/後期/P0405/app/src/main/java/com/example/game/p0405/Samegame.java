package com.example.game.p0405;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Norihisa Ishida on 2015/10/28.
 */
public class Samegame extends View {
    int n;
    int cw;
    int color[][];
    int col[]={0xFF000000,0xFF0000FF,0xFF00FF00,
            0xFFFF0000,0xFF00FFFF,
            0xFFFF00FF,0xFFFFFF00};
    TextView tv;
    //enum STATE{GAMESTART, GAMECLEAR, GAMEOVER};
    //String[] msg = {"Samegame", "Game Clear !", "Game Over !"};
    //STATE state=STATE.GAMESTART;
    public Samegame(Context context, AttributeSet attrs) {
        super(context,attrs);
        // TODO Auto-generated constructor stub
        n=2;

        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        display.getRotation();
        Point p = new Point();
        display.getSize(p);
        cw = (int)(0.95*p.x/(3*n));
        init();
    }
    private void init() {
        color=new int[3*n][4*n];
        Random r=new Random();
        for(int i=0;i<3*n;i++)
            for(int j=0;j<4*n;j++)
                color[i][j]=r.nextInt(4)+1;
        setBackgroundColor(0xFF000000);
    }
    void setTextView(TextView tv){
        this.tv = tv;
    }
    public void replay(){
        init();
        tv.setText("SAMEGAME");
        invalidate();
    }
    void remove(int x, int y, int col){
        if(color[x][y]==0) return;
        color[x][y]=0;
        if(x+1<3*n&&color[x+1][y]==col) remove(x+1,y,col);
        if(x>0*n  &&color[x-1][y]==col) remove(x-1,y,col);
        if(y+1<4*n&&color[x][y+1]==col) remove(x,y+1,col);
        if(y>0*n  &&color[x][y-1]==col) remove(x,y-1,col);
    }
    void fall(){
        for(int i=0;i<3*n;i++){
            boolean flag;
            do{
                flag = false;
                for(int j=4*n-1;j>0;j--){
                    if(color[i][j]==0&&color[i][j-1]!=0){
                        color[i][j] = color[i][j-1];
                        color[i][j-1] = 0;
                        flag = true;
                    }
                }
            }while(flag);
        }
    }
    void slide(){
        boolean flag;
        do{
            flag=false;
            for(int i=0;i<3*n-1;i++){
                if(color[i][4*n-1]==0){
                    for(int j=4*n-1;j>=0;j--){
                        if(color[i+1][j]==0) break;
                        color[i][j] = color[i+1][j];
                        color[i+1][j] = 0;
                        flag=true;
                    }
                }
            }
        }while(flag);
    }
    boolean isClear(){
        if(color[0][4*n-1]==0) return true;
        else return false;
    }
    boolean isOver(){
        boolean flag = true;
        for(int i=0;i<3*n;i++)
            for(int j=0;j<4*n;j++)
                if(isRemovable(i,j)&&color[i][j]!=0) flag = false;
        return flag;
    }
    boolean isRemovable(int px, int py){
        return (px+1<3*n&&color[px][py]==color[px+1][py])||
                (px>0&&color[px][py]==color[px-1][py])||
                (py+1<4*n&&color[px][py]==color[px][py+1])||
                (py>0&&color[px][py]==color[px][py-1]);
    }
    public boolean onTouchEvent(MotionEvent me){
        int ae=me.getAction();
        int x = (int)me.getX();
        int y = (int)me.getY();
        int px = x/cw, py = y/cw;
        if(px<0||py<0||px>=3*n||py>=4*n) return false;
        switch(ae){
            case MotionEvent.ACTION_DOWN:
                if(isRemovable(px, py)){
                    remove(px,py,color[px][py]);
                    fall();
                    slide();
                }
                if(isClear()) tv.setText("Game Clear !");
                if(isOver()) tv.setText("Game Over !");
                invalidate();
                break;
        }
        return true;
    }
    public void onDraw(Canvas c){
        Paint p= new Paint();
        p.setColor(Color.YELLOW);
        p.setTextSize(48.0f);
        for (int i = 0; i < 3 * n; i++)
            for (int j = 0; j < 4 * n; j++) {
                p.setColor(col[color[i][j]]);
                c.drawCircle(i * cw + cw / 2, j * cw + cw / 2, cw / 2, p);
            }
    }
}
