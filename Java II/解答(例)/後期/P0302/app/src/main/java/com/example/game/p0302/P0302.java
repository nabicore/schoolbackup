package com.example.game.p0302;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Norihisa Ishida on 2015/10/17.
 */
public class P0302 extends View {
    int[] color;
    int[] col = {Color.BLACK,Color.BLACK,Color.BLACK};
    float radius = 80;
    PointF[] pt= new PointF[]{new PointF(200, 200), new PointF(400,200), new PointF(600,200)};
    public P0302(Context context) {
        super(context);
        color=new int[]{Color.RED, Color.YELLOW, Color.BLUE};
    }
    public boolean onTouchEvent(MotionEvent me){
        int ae=me.getAction();
        float x = me.getX();
        float y = me.getY();
        switch(ae) {
            case MotionEvent.ACTION_DOWN:
                int pos;
                for(pos = 0;pos<pt.length;pos++) {
                    if ((x - pt[pos].x) * (x - pt[pos].x) + (y - pt[pos].y) * (y - pt[pos].y) < radius * radius) {
                        col[pos] = color[pos];
                        break;
                    }
                }
                invalidate();
                break;
        }
        return true;
    }

    public void onDraw(Canvas c){
        Paint p= new Paint();
        for(int i=0;i<pt.length;i++) {
            p.setColor(col[i]);;
            c.drawCircle(pt[i].x, pt[i].y, radius, p);
        }
    }
}
