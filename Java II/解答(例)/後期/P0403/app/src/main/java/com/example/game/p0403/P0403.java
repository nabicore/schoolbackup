package com.example.game.p0403;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TableLayout;

/**
 * Created by Norihisa Ishida on 2015/10/17.
 */
public class P0403 extends View {
    int color[][];
    float x,y,w,h;
    int n=3, m=3;
    Point p;
    boolean Fstart;
    public P0403(Context context,AttributeSet as) {
        super(context, as);
        Display disp = ((Activity)context).getWindowManager().getDefaultDisplay();
        p = new Point();
        disp.getSize(p);
        Fstart = false;
    }
    void start(){
        color = new int[m][n];
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                color[i][j]= Color.RED;
            }
        }
        w = 0.95f*p.x/m;
        h = 0.95f*p.x/n;
        Fstart = true;
        invalidate();
    }
    public void setSize(int m, int n){
        this.m = m;
        this.n = n;
    }
    public boolean onTouchEvent(MotionEvent me){
        if(!Fstart) return true;
        int ae=me.getAction();
        x = me.getX();
        y = me.getY();
        switch(ae) {
            case MotionEvent.ACTION_DOWN:
                int i = (int)(x/w);
                int j = (int)(y/h);
                if(i<0 || i>=m || j<0 || j>=n) return false;
                color[i][j]=(color[i][j]==Color.RED)?Color.BLACK:Color.RED;
                try {
                    color[i-1][j] = (color[i-1][j] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                try {
                    color[i+1][j] = (color[i+1][j] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                try {
                    color[i][j-1] = (color[i][j-1] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                try {
                    color[i][j+1] = (color[i][j+1] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                invalidate();
                break;
        }
        return true;
    }
    public void onDraw(Canvas c){
        if(!Fstart) return;
        Paint p= new Paint();

        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                p.setColor(color[i][j]);
                c.drawRect(i*w, j*h, (i+1)*w-1,(j+1)*h-1,p);
            }
        }

    }
}
