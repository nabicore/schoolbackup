package com.example.game.p0403;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText et1 = (EditText)findViewById(R.id.editText);
        final EditText et2 = (EditText)findViewById(R.id.editText2);
        final TextView tv = (TextView)findViewById(R.id.textView);
        Button b = (Button)findViewById(R.id.button);
        final P0403 game= (P0403)findViewById(R.id.view);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int m =Integer.parseInt(et1.getText().toString());
                int n =Integer.parseInt(et2.getText().toString());
                tv.setText(m+"×"+n);
                game.setSize(m,n);
                game.start();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
