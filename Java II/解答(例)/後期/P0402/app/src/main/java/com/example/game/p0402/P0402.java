package com.example.game.p0402;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Norihisa Ishida on 2015/10/18.
 */
public class P0402 extends View {
    int color=Color.BLACK;
    public P0402(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    void buttonR(){
        color = Color.RED;
        invalidate();
    }
    void buttonG(){
        color = Color.GREEN;
        invalidate();
    }
    void buttonB(){
        color = Color.BLUE;
        invalidate();
    }
    public void onDraw(Canvas c){
        Paint p = new Paint();
        p.setColor(color);
        c.drawCircle(100, 100, 50, p);
    }

}
