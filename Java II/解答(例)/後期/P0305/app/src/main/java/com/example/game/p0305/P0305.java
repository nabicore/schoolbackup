package com.example.game.p0305;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Norihisa Ishida on 2015/10/17.
 */
public class P0305 extends View {
    int color[][];
    float x,y,w=200,h=200;
    final int n, m;
    public P0305(Context context,int m, int n) {
        super(context);
        this.m = m;
        this.n = n;
        color = new int[m][n];
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                color[i][j]= Color.RED;
            }
        }
    }
    public boolean onTouchEvent(MotionEvent me){
        int ae=me.getAction();
        x = me.getX();
        y = me.getY();
        switch(ae) {
            case MotionEvent.ACTION_DOWN:
                int i = (int)(x/w);
                int j = (int)(y/h);
                if(i<0 || i>=m || j<0 || j>=n) return false;
                color[i][j]=(color[i][j]==Color.RED)?Color.BLACK:Color.RED;
                try {
                    color[i-1][j] = (color[i-1][j] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                try {
                    color[i+1][j] = (color[i+1][j] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                try {
                    color[i][j-1] = (color[i][j-1] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                try {
                    color[i][j+1] = (color[i][j+1] == Color.RED) ? Color.BLACK : Color.RED;
                }catch(ArrayIndexOutOfBoundsException aioobe){}
                invalidate();
                break;
        }
        return true;
    }
    public void onDraw(Canvas c){
        Paint p= new Paint();

        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                p.setColor(color[i][j]);
                c.drawRect(i*w, j*h, (i+1)*w-1,(j+1)*h-1,p);
            }
        }

    }
}
