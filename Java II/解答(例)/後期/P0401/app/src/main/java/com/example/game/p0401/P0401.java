package com.example.game.p0401;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Norihisa Ishida on 2015/10/18.
 */
public class P0401 extends View {
    public P0401(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public void onDraw(Canvas c){
        Paint p = new Paint();
        p.setColor(Color.RED);
        c.drawColor(Color.YELLOW);
        c.drawCircle(200,200,100,p);
        p.setColor(Color.BLUE);
        c.drawRect(400,100,700,300,p);
    }
}
